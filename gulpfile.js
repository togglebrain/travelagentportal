var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var cleanCSS = require('gulp-clean-css');
var app ={};

var config = {
    assetsDir: 'web',
    sassPattern: 'sass/**/*.scss',
    bowerDir: 'vendor/bower_components'
}
app.addStyle = function(paths, outputFilename) {
    gulp.src(paths)
        .pipe(plugins.plumber(function(error) {
            console.log(error.toString());
            this.emit('end');
        }))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass())
        .pipe(plugins.concat(outputFilename))
        .pipe(cleanCSS())
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest('web/css'));
}

app.addScript = function(paths, outputFilename) {
    gulp.src(paths)
        .pipe(plugins.plumber(function(error) {
            console.log(error.toString());
            this.emit('end');
        }))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.concat(outputFilename))
        .pipe(plugins.uglify())
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest('web/js'));
}

app.copy = function(srcFiles, outputDir) {
    gulp.src(srcFiles)
        .pipe(gulp.dest(outputDir));
}


gulp.task('styles', function() {
    app.addStyle([
        config.bowerDir+'/bootstrap/dist/css/bootstrap.css',
        config.bowerDir+'/font-awesome/css/font-awesome.css',
        config.assetsDir+'/sass/theme.scss'
    ], 'main.css');
});

gulp.task('scripts', function() {
    app.addScript([
        config.bowerDir+'/bootstrap/dist/js/bootstrap.js',
        config.assetsDir+'/js/main.js'
    ], 'site.js');
});

gulp.task('fonts', function() {
    app.copy(
        config.bowerDir+'/font-awesome/fonts/*',
        'web/fonts'
    );
});


gulp.task('watch', function() {
    gulp.watch(config.assetsDir+'/'+config.sassPattern, ['styles']);
    gulp.watch(config.assetsDir+'/js/**/*.js', ['scripts']);
});

gulp.task('default', ['styles','scripts','fonts','watch']);