<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class PropertyDiscount
 * @ORM\Table(name="property_discount")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PropertyDiscountRepository")
 */
class PropertyDiscount
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Property", inversedBy="discount")
     * @Assert\NotBlank()
     */
    protected $property;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime")
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(name="discount_rate", type="integer")
     */
    private $rate;

    /**
     * Get Id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set location-page
     *
     * @param \AppBundle\Entity\Property $property
     *
     * @return PropertyDiscount
     */
    public function setProperty(\AppBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get location-page
     *
     * @return \AppBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set Start Date
     *
     * @param \DateTime $startDate
     *
     * @return PropertyDiscount
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get Start Date
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set End Date
     *
     * @param \DateTime $endDate
     *
     * @return PropertyDiscount
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get End Date
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set Rate
     *
     * @param integer $rate
     *
     * @return PropertyDiscount
     *
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get Rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }
}