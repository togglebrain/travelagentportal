<?php
/**
 * Created by PhpStorm.
 * User: Demi
 * Date: 26-Feb-18
 * Time: 12:44 PM
 */
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields={"phone"}, message="Mobile number entered is already registered")
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */

class User  extends BaseUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @ORM\Column(name="first_name", type="string", length=30, options={"comment":"Users First Name"}, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     * @ORM\Column(name="last_name", type="string", length=30, options={"comment":"Users Last Name"}, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(name="phone", type="string", length=10, options={"comment":"Users phone number"}, unique=true, nullable=true)
     * @Assert\Type(type="digit",message="Mobile Number has to be Numeric!")
     * @Assert\Regex(pattern="/^[0-9]{10}$/", message="Mobile Number has to be 10 digits")
     */
    private $phone;


    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Reservation", mappedBy="user",cascade={"persist","remove"})
     */
    private $reservation;

    /**
     * @var int
     *
     * @ORM\Column(name="category", type="integer", nullable=true)
     */
    private $category;


    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\UserDetails", mappedBy="user",cascade={"persist","remove"})
     */

    private $userDetails;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="OurguestBundle\Entity\Payment", mappedBy="user")
     */
    private $payment;


    protected $roles=array();

    public function __construct()
    {
        parent::__construct();
        $this->roles=array('ROLE_AGENT');
    }


    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }


    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }


    /**
     * Add reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     *
     * @return User
     */
    public function addReservation(\AppBundle\Entity\Reservation $reservation)
    {
        $this->reservation[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\AppBundle\Entity\Reservation $reservation)
    {
        $this->reservation->removeElement($reservation);
    }

    /**
     * Get reservation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * Set userDetails
     *
     * @param \AppBundle\Entity\UserDetails $userDetails
     *
     * @return User
     */
    public function setUserDetails(\AppBundle\Entity\UserDetails $userDetails = null)
    {
        $this->userDetails = $userDetails;

        return $this;
    }

    /**
     * Get userDetails
     *
     * @return \AppBundle\Entity\UserDetails
     */
    public function getUserDetails()
    {
        return $this->userDetails;
    }

    /**
     * Add review
     *
     * @param \OurguestBundle\Entity\Reviews $review
     *
     * @return User
     */
    public function addReview(\OurguestBundle\Entity\Reviews $review)
    {
        $this->review[] = $review;

        return $this;
    }

    /**
     * Add payment
     *
     * @param \OurguestBundle\Entity\Payment $payment
     *
     * @return User
     */
    public function addPayment(\OurguestBundle\Entity\Payment $payment)
    {
        $this->payment[] = $payment;

        return $this;
    }

    /**
     * Remove payment
     *
     * @param \OurguestBundle\Entity\Payment $payment
     */
    public function removePayment(\OurguestBundle\Entity\Payment $payment)
    {
        $this->payment->removeElement($payment);
    }

    /**
     * Get payment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayment()
    {
        return $this->payment;
    }
}
