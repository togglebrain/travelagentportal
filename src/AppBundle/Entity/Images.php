<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Images
 *
 * @ORM\Table(name="images")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImagesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Images
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Property", inversedBy="image")
     *
     */
    private $property;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Rooms", inversedBy="image")
     *
     */
    private $rooms;


    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\Tours", inversedBy="image")
     *
     */
    private $tour;


    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\Experiences", inversedBy="image")
     *
     */
    private $experience;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="altText", type="text", length=125, nullable=true)
     */
    private $altText;

    /**
     * @Assert\NotBlank(message="Image is missing!")
     * @Assert\File(
     *     mimeTypes = {"images/*"},
     *     mimeTypesMessage = "Please upload a valid image.",
     *     maxSize="1024k",
     *     maxSizeMessage="File size too large."
     * )
     */
    private $file;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Images
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }



    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        //return 'images/Gallery';

        if($this->property)
        {
            return 'images/uploads/property';
        }
        elseif($this->tour)
        {
            return 'images/uploads/tour';
        }
        elseif($this->experience)
        {
            return 'images/uploads/experience';
        }
        elseif($this->rooms)
        {
            return 'images/uploads/rooms';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            file_exists($this->getUploadDir(). '/'.$this->temp) ? unlink($this->getUploadRootDir().'/'.$this->temp) : null;
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            file_exists($file) ? unlink($file) : null;
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getFile();
    }

    /**
     * Set property
     *
     * @param \AppBundle\Entity\Property $property
     *
     * @return Images
     */
    public function setProperty(\AppBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \AppBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set rooms
     *
     * @param \AppBundle\Entity\Rooms $rooms
     *
     * @return Images
     */
    public function setRooms(\AppBundle\Entity\Rooms $rooms = null)
    {
        $this->rooms = $rooms;

        return $this;
    }

    /**
     * Get rooms
     *
     * @return \AppBundle\Entity\Property
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set tour
     *
     * @param \OurguestBundle\Entity\Tours $tour
     *
     * @return Images
     */
    public function setTour(\OurguestBundle\Entity\Tours $tour = null)
    {
        $this->tour = $tour;

        return $this;
    }

    /**
     * Get tour
     *
     * @return \OurguestBundle\Entity\Tours
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * Set experience
     *
     * @param \OurguestBundle\Entity\Experiences $experience
     *
     * @return Images
     */
    public function setExperience(\OurguestBundle\Entity\Experiences $experience = null)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return \OurguestBundle\Entity\Experiences
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set Alt Text
     * @param string $altText
     * @return Images
     */
    public function setAltText($altText){
        $this->altText = $altText;

        return $this;
    }

    /**
     * Get Alt Text
     * @return string
     */
    public function getAltText(){
        return $this->altText;
    }
}
