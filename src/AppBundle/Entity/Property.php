<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use OurguestBundle\Entity\Experiences;
use OurguestBundle\Entity\Facilities;
use OurguestBundle\Entity\Suggested;
use OurguestBundle\Entity\Tours;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Property
 *
 * @ORM\Table(name="property")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PropertyRepository")
 */
class Property
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var int
     *
     * @ORM\Column(name="category", type="integer")
     */
    private $category;


    /**
     * @var string
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"image/jpg","image/jpeg","image/png"},
     *     mimeTypesMessage = "Please upload a valid image"
     * )
     * @ORM\Column(name="featimage", type="string", length=255)
     */
    private $featimage;

    /**
     * @var string
     * @ORM\Column(name="alt_text", type="string", length=125, nullable=true)
     */
    private $altText;


    /**
     * @var boolean
     *
     * @ORM\Column(name="flag", type="boolean", nullable=true)
     */
    private $isFeatured;

    /**
     * @var int
     *
     * @ORM\Column(name="display", type="integer", nullable=true)
     */
    private $display;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Images", mappedBy="property",cascade={"all"},orphanRemoval=true)
     */
    private $image;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Rooms", mappedBy="property",cascade={"all"},orphanRemoval=true)
     */
    private $rooms;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location",inversedBy="property")
     * @Assert\NotBlank()
     */
    protected $location;

    /**
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\Host",inversedBy="property")
     */
    protected $host;

    /**
     * @var int
     * @ORM\Column(name="max_infant_age", type="integer", length=2, nullable=false)
     */
    private $max_infant_age;

    /**
     * @var string
     *
     * @ORM\Column(name="video_url", type="string", length=255, nullable=true)
     */
    private $videoUrl;

    /**
     * @var string
     * @ORM\Column(name="what_is_good", type="text", nullable=true)
     */
    private $good;

    /**
     * @var string
     * @ORM\Column(name="what_is_not_good", type="text", nullable=true)
     */
    private $notGood;

    /**
     * @ORM\ManyToMany(targetEntity="OurguestBundle\Entity\Facilities", inversedBy="property")
     * @ORM\JoinColumn(name="facility_id", onDelete="SET NULL", nullable=true)
     */
    private $facilities;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="OurguestBundle\Entity\Reviews", mappedBy="property")
     */
    private $review;


    /**
     * @ORM\ManyToMany(targetEntity="OurguestBundle\Entity\Experiences", inversedBy="property")
     * @ORM\JoinColumn(name="experience_id", onDelete="SET NULL", nullable=true)
     */
    private $experiences;

    /**
     * @ORM\ManyToMany(targetEntity="OurguestBundle\Entity\Tours", mappedBy="property")
     * @ORM\JoinColumn(name="tour_id", onDelete="SET NULL", nullable=true)
     */
    private $tour;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PropertyDiscount", mappedBy="property")
     */
    private $discount;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true, nullable=true)
     */
    protected $slug;

    /**
     * @ORM\OneToOne(targetEntity="OurguestBundle\Entity\Suggested", mappedBy="homestay")
     */
    protected $suggested;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Property
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Property
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Property
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Property
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set category
     *
     * @param integer $category
     *
     * @return Property
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }


    /**
     * Set featimage
     *
     * @param string $featimage
     *
     * @return Property
     */
    public function setFeatimage($featimage)
    {
        $this->featimage = $featimage;
        return $this;
    }
    /**
     * Get featimage
     *
     * @return string
     */
    public function getFeatimage()
    {
        return $this->featimage;
    }

    /**
     * Set Alt Text
     * @param string $altText
     * @return Property
     */
    public function setAltText($altText){
        $this->altText = $altText;

        return $this;
    }

    /**
     * Get Alt Text
     * @return string
     */
    public function getAltText(){
        return $this->altText;
    }

    /**
     * @return boolean
     */
    public function isIsFeatured()
    {
        return $this->isFeatured;
    }

    /**
     * @param boolean $isFeatured
     */
    public function setIsFeatured($isFeatured)
    {
        $this->isFeatured = $isFeatured;
    }

    /**
     * @return int
     */
    public function getDisplay(){
        return $this->display;
    }

    /**
     * @param int $display
     */
    public function setDisplay($display){
        $this->display = $display;
    }

    /**
     * Set Keywords
     * @param string $keywords
     *
     * @return Property
     */
    public function setKeywords($keywords){
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get Keywords
     * @return string
     */
    public function getKeywords(){
        return $this->keywords;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->review = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rooms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
        $this->experiences = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tour = new \Doctrine\Common\Collections\ArrayCollection();
        $this->facilities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add room
     *
     * @param \AppBundle\Entity\Rooms $room
     *
     * @return Property
     */
    public function addRoom(\AppBundle\Entity\Rooms $room)
    {
        $this->rooms[] = $room;

        return $this;
    }

    /**
     * Remove room
     *
     * @param \AppBundle\Entity\Rooms $room
     */
    public function removeRoom(\AppBundle\Entity\Rooms $room)
    {
        $this->rooms->removeElement($room);
    }

    /**
     * Get rooms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * Set location
     *
     * @param \AppBundle\Entity\Location $location
     *
     * @return Property
     */
    public function setLocation(\AppBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \AppBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Property
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->image[] = $image;
        $image->setProperty($this);
        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set videoUrl
     *
     * @param string $videoUrl
     *
     * @return Property
     */
    public function setVideoUrl($videoUrl)
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    /**
     * Get videoUrl
     *
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->videoUrl;
    }

    /**
     * Set good
     * @param string $good
     * @return Property
     */
    public function setGood($good){
        $this->good = $good;
        return $this;
    }

    /**
     * Get good
     * @return string
     */
    public function getGood(){
        return $this->good;
    }

    /**
     * Set notGood
     * @param string $notGood
     * @return Property
     */
    public function setNotGood($notGood){
        $this->notGood = $notGood;
        return $this;
    }

    /**
     * Get notGood
     * @return string
     */
    public function getNotGood(){
        return $this->notGood;
    }

    /**
     * set maxInfantAge
     * @param integer $max_infant_age
     * @return Property
     */
    public function setMaxInfantAge($max_infant_age) {
        $this->max_infant_age = $max_infant_age;
        return $this;
    }

    /**
     * Get maxInfantAge
     * @return integer
     */
    public function getMaxInfantAge() {
        return $this->max_infant_age;
    }

    /**
     * Set host
     *
     * @param \OurguestBundle\Entity\Host $host
     *
     * @return Property
     */
    public function setHost(\OurguestBundle\Entity\Host $host = null)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return \OurguestBundle\Entity\Host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Add review
     *
     * @param \OurguestBundle\Entity\Reviews $review
     *
     * @return Property
     */
    public function addReview(\OurguestBundle\Entity\Reviews $review)
    {
        $this->review[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \OurguestBundle\Entity\Reviews $review
     */
    public function removeReview(\OurguestBundle\Entity\Reviews $review)
    {
        $this->review->removeElement($review);
    }

    /**
     * Get review
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     */
    public function getExperiences()
    {
        return $this->experiences;
    }

    public function addExperience(Experiences $experiences)
    {
        if (!$this->experiences->contains($experiences)) {
            $this->experiences[] = $experiences;
        }
        return $this;
    }
    public function removeExperience(Experiences $experiences)
    {
        if ($this->experiences->contains($experiences)) {
            $this->experiences->removeElement($experiences);
        }
        return $this;
    }

    /**
     */
    public function getFacilities()
    {
        return $this->facilities;
    }

    public function addFacility(Facilities $facilities)
    {
        if (!$this->facilities->contains($facilities)){
            $this->facilities[] = $facilities;
        }
        return $this;
    }

    public function removeFacility(Facilities $facilities){
        if ($this->facilities->contains($facilities)){
            $this->facilities->removeElement($facilities);
        }
        return $this;
    }

    /**
     */
    public function getTour(){
        return $this->tour;
    }

    public function addTour(Tours $tours){
        if(!$this->tour->contains($tours)){
            $this->tour[] = $tours;
        }
        return $this;
    }

    public function removeTour(Tours $tours){
        if($this->tour->contains($tours)){
            $this->tour->removeElement($tours);
        }
        return $this;
    }

    /**
     * Add Property Discount
     *
     * @param \AppBundle\Entity\PropertyDiscount $discount
     *
     * @return Property
     */
    public function addPropertyDiscount(\AppBundle\Entity\PropertyDiscount $discount)
    {
        $this->discount[] = $discount;

        return $this;
    }

    /**
     * Remove Property Discount
     *
     * @param \AppBundle\Entity\PropertyDiscount $discount
     */
    public function removePropertyDiscount(\AppBundle\Entity\PropertyDiscount $discount)
    {
        $this->discount->removeElement($discount);
    }

    /**
     * Get Property Discount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropertyDiscount()
    {
        return $this->discount;
    }

    public function setSlug($slug){
        $this->slug = $slug;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Set Suggested
     * @param Suggested $suggested
     * @return Property
     */
    public function setSuggested($suggested){
        $this->suggested = $suggested;

        return $this;
    }

    /**
     * Get Suggested
     * @return Suggested
     */
    public function getSuggested(){
        return $this->suggested;
    }
}