<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Discount
 * @ORM\Entity
 * @ORM\Table(name="discount")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiscountRepository")
 */

class Discount
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_category", type="integer")
     */
    private $userCategory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime")
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(name="rate", type="integer")
     */
    private $rate;

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set User
     *
     * @param integer $userCategory
     *
     * @return Discount
     */
    public function setUserCategory($userCategory)
    {
        $this->userCategory = $userCategory;

        return $this;
    }

    /**
     * Get User
     *
     * @return integer
     */
    public function getUserCategory()
    {
        return $this->userCategory;
    }

    /**
     * Set Start Date
     *
     * @param \DateTime $startDate
     *
     * @return Discount
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get Start Date
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set End Date
     *
     * @param \DateTime $endDate
     *
     * @return Discount
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get End Date
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set Rate
     *
     * @param integer $rate
     *
     * @return Discount
     *
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get Rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }
}