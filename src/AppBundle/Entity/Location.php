<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Location
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LocationRepository")
 */
class Location
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Property", mappedBy="location")
     */
    private $property;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="OurguestBundle\Entity\Experiences", mappedBy="location")
     */
    private $experience;

    /**
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\States", inversedBy="location")
     * @ORM\JoinColumn(name="state_id", onDelete="SET NULL", nullable=true)
     * @var
     */
    protected $state;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Location
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Location
     */
    public function setDescription($description){
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->property = new \Doctrine\Common\Collections\ArrayCollection();
        $this->experience = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add experience
     *
     * @param \OurguestBundle\Entity\Experiences $experience
     *
     * @return Location
     */
    public function addExperience(\OurguestBundle\Entity\Experiences $experience)
    {
        $this->experience[] = $experience;

        return $this;
    }

    /**
     * Remove experience
     *
     * @param \OurguestBundle\Entity\Experiences $experience
     */
    public function removeExperience(\OurguestBundle\Entity\Experiences $experience)
    {
        $this->experience->removeElement($experience);
    }

    /**
     * Get experience
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExperience()
    {
        return $this->experience;
    }


    /**
     * Add property
     *
     * @param \AppBundle\Entity\Property $property
     *
     * @return Location
     */
    public function addProperty(\AppBundle\Entity\Property $property)
    {
        $this->property[] = $property;

        return $this;
    }

    /**
     * Remove property
     *
     * @param \AppBundle\Entity\Property $property
     */
    public function removeProperty(\AppBundle\Entity\Property $property)
    {
        $this->property->removeElement($property);
    }

    /**
     * Get property
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set State
     *
     * @param \OurguestBundle\Entity\States $state
     *
     * @return Location
     */

    public function setState(\OurguestBundle\Entity\States $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get States
     *
     * @return \OurguestBundle\Entity\States
     */

    public function getState()
    {
        return $this->state;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
