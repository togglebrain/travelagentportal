<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraPerson
 *
 * @ORM\Table(name="extra_person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExtraPersonRepository")
 */
class ExtraPerson
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="adult_ep_cost", type="float")
     */
    private  $adult_ep_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="adult_cp_cost", type="float")
     */
    private  $adult_cp_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="adult_ap_cost", type="float")
     */
    private  $adult_ap_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="adult_map_cost", type="float")
     */
    private  $adult_map_cost;

    /**
     * @var float
     * @ORM\Column(name="child_with_bed_ep_cost", type="float")
     */
    private $child_with_bed_ep_cost;

    /**
     * @var float
     * @ORM\Column(name="child_with_bed_cp_cost", type="float")
     */
    private $child_with_bed_cp_cost;

    /**
     * @var float
     * @ORM\Column(name="child_with_bed_ap_cost", type="float")
     */
    private $child_with_bed_ap_cost;

    /**
     * @var float
     * @ORM\Column(name="child_with_bed_map_cost", type="float")
     */
    private $child_with_bed_map_cost;

    /**
     * @var float
     * @ORM\Column(name="child_without_bed_ep_cost", type="float")
     */
    private $child_without_bed_ep_cost;

    /**
     * @var float
     * @ORM\Column(name="child_without_bed_cp_cost", type="float")
     */
    private $child_without_bed_cp_cost;

    /**
     * @var float
     * @ORM\Column(name="child_without_bed_ap_cost", type="float")
     */
    private $child_without_bed_ap_cost;

    /**
     * @var float
     * @ORM\Column(name="child_without_bed_map_cost", type="float")
     */
    private $child_without_bed_map_cost;

    /**
     * @var int
     * 
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Rooms", inversedBy="extra_person")
     */
    private $room;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adult_ep_cost
     *
     * @param float  $adult_ep_cost
     *
     * @return ExtraPerson
     */
    public function setAdultEpCost( $adult_ep_cost)
    {
        $this->adult_ep_cost =  $adult_ep_cost;

        return $this;
    }

    /**
     * Get adult_ep_cost
     *
     * @return float
     */
    public function getAdultEpCost()
    {
        return $this->adult_ep_cost;
    }

    /**
     * Set adult_cp_cost
     *
     * @param float  $adult_cp_cost
     *
     * @return ExtraPerson
     */
    public function setAdultCpCost( $adult_cp_cost)
    {
        $this->adult_cp_cost =  $adult_cp_cost;

        return $this;
    }

    /**
     * Get adult_cp_cost
     *
     * @return float
     */
    public function getAdultCpCost()
    {
        return $this->adult_cp_cost;
    }

    /**
     * Set adult_ap_cost
     *
     * @param float  $adult_ap_cost
     *
     * @return ExtraPerson
     */
    public function setAdultApCost( $adult_ap_cost)
    {
        $this->adult_ap_cost =  $adult_ap_cost;

        return $this;
    }

    /**
     * Get adult_ap_cost
     *
     * @return float
     */
    public function getAdultApCost()
    {
        return $this->adult_ap_cost;
    }

    /**
     * Set adult_map_cost
     *
     * @param float  $adult_map_cost
     *
     * @return ExtraPerson
     */
    public function setAdultMapCost( $adult_map_cost)
    {
        $this->adult_map_cost =  $adult_map_cost;

        return $this;
    }

    /**
     * Get adult_map_cost
     *
     * @return float
     */
    public function getAdultMapCost()
    {
        return $this->adult_map_cost;
    }

    /**
     * Set childWithBedEpCost
     *
     * @param float $child_with_bed_ep_cost
     *
     * @return ExtraPerson
     */
    public function setChildWithBedEpCost($child_with_bed_ep_cost)
    {
        $this->child_with_bed_ep_cost = $child_with_bed_ep_cost;

        return $this;
    }

    /**
     * Get childWithBedEpCost
     *
     * @return float
     */
    public function getChildWithBedEpCost()
    {
        return $this->child_with_bed_ep_cost;
    }

    /**
     * Set childWithBedCpCost
     *
     * @param float $child_with_bed_cp_cost
     *
     * @return ExtraPerson
     */
    public function setChildWithBedCpCost($child_with_bed_cp_cost)
    {
        $this->child_with_bed_cp_cost = $child_with_bed_cp_cost;

        return $this;
    }

    /**
     * Get childWithBedCpCost
     *
     * @return float
     */
    public function getChildWithBedCpCost()
    {
        return $this->child_with_bed_cp_cost;
    }

    /**
     * Set childWithBedApCost
     *
     * @param float $child_with_bed_ap_cost
     *
     * @return ExtraPerson
     */
    public function setChildWithBedApCost($child_with_bed_ap_cost)
    {
        $this->child_with_bed_ap_cost = $child_with_bed_ap_cost;

        return $this;
    }

    /**
     * Get childWithBedApCost
     *
     * @return float
     */
    public function getChildWithBedApCost()
    {
        return $this->child_with_bed_ap_cost;
    }

    /**
     * Set childWithBedMapCost
     *
     * @param float $child_with_bed_map_cost
     *
     * @return ExtraPerson
     */
    public function setChildWithBedMapCost($child_with_bed_map_cost)
    {
        $this->child_with_bed_map_cost = $child_with_bed_map_cost;

        return $this;
    }

    /**
     * Get childWithBedMapCost
     *
     * @return float
     */
    public function getChildWithBedMapCost()
    {
        return $this->child_with_bed_map_cost;
    }

    /**
     * Set childWithoutBedEpCost
     *
     * @param float $child_without_bed_ep_cost
     *
     * @return ExtraPerson
     */
    public function setChildWithoutBedEpCost($child_without_bed_ep_cost)
    {
        $this->child_without_bed_ep_cost = $child_without_bed_ep_cost;

        return $this;
    }

    /**
     * Get childWithoutBedEpCost
     *
     * @return float
     */
    public function getChildWithoutBedEpCost()
    {
        return $this->child_without_bed_ep_cost;
    }

    /**
     * Set childWithoutBedCpCost
     *
     * @param float $child_without_bed_cp_cost
     *
     * @return ExtraPerson
     */
    public function setChildWithoutBedCpCost($child_without_bed_cp_cost)
    {
        $this->child_without_bed_cp_cost = $child_without_bed_cp_cost;

        return $this;
    }

    /**
     * Get childWithoutBedCpCost
     *
     * @return float
     */
    public function getChildWithoutBedCpCost()
    {
        return $this->child_without_bed_cp_cost;
    }

    /**
     * Set childWithoutBedApCost
     *
     * @param float $child_without_bed_ap_cost
     *
     * @return ExtraPerson
     */
    public function setChildWithoutBedApCost($child_without_bed_ap_cost)
    {
        $this->child_without_bed_ap_cost = $child_without_bed_ap_cost;

        return $this;
    }

    /**
     * Get childWithoutBedApCost
     *
     * @return float
     */
    public function getChildWithoutBedApCost()
    {
        return $this->child_without_bed_ap_cost;
    }

    /**
     * Set childWithoutBedMapCost
     *
     * @param float $child_without_bed_map_cost
     *
     * @return ExtraPerson
     */
    public function setChildWithoutBedMapCost($child_without_bed_map_cost)
    {
        $this->child_without_bed_map_cost = $child_without_bed_map_cost;

        return $this;
    }

    /**
     * Get childWithoutBedMapCost
     *
     * @return float
     */
    public function getChildWithoutBedMapCost()
    {
        return $this->child_without_bed_map_cost;
    }

    /**
     * Set Room 
     * @param \AppBundle\Entity\Rooms $room
     * @return ExtraPerson
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get Room
     * @return \AppBundle\Entity\Rooms
     */
    public function getRoom()
    {
        return $this->room;
    }
}

