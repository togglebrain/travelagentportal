<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Discount Default Rates
 * @ORM\Entity
 * @ORM\Table(name="discount_def_rates")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiscountDefRatesRepository")
 */

class DiscountDefRates
{
    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="user_category", type="integer")
     */
    private $userCategory;

    /**
     * @var int
     * @ORM\Column(name="default_rate", type="integer")
     */
    private $defRate;

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set User Category
     *
     * @param integer $userCategory
     *
     * @return DiscountDefRates
     */
    public function setUserCategory($userCategory)
    {
        $this->userCategory = $userCategory;

        return $this;
    }

    /**
     * Get User Category
     *
     * @return integer
     */
    public function getUserCategory()
    {
        return $this->userCategory;
    }

    /**
     * Set Default Rate
     *
     * @param integer $defRate
     *
     * @return DiscountDefRates
     */
    public function setDefRate($defRate)
    {
        $this->defRate = $defRate;

        return $this;
    }

    /**
     * Get Default Rate
     *
     * @return integer
     */
    public function getDefRate()
    {
        return $this->defRate;
    }
}