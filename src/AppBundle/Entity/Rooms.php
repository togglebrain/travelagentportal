<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rooms
 *
 * @ORM\Table(name="rooms")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoomsRepository")
 */
class Rooms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="float")
     */
    private $rate;


    /**
     * @var float
     *
     * @ORM\Column(name="single_cp_cost", type="float")
     */
    private $single_cp_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="single_ep_cost", type="float")
     */
    private $single_ep_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="single_ap_cost", type="float")
     */
    private $single_ap_cost;

    /**
     * @var float
     *
     * @ORM\Column(name="single_map_cost", type="float")
     */
    private $single_map_cost;

    /**
     * @var float
     * @ORM\Column(name="double_cp_cost", type="float")
     */
    private $double_cp_cost;

    /**
     * @var float
     * @ORM\Column(name="double_ep_cost", type="float")
     */
    private $double_ep_cost;

    /**
     * @var float
     * @ORM\Column(name="double_ap_cost", type="float")
     */
    private $double_ap_cost;

    /**
     * @var float
     * @ORM\Column(name="double_map_cost", type="float")
     */
    private $double_map_cost;

    /**
     * @var int
     *
     * @ORM\Column(name="adult", type="integer")
     */
    private $adult;

    /**
     * @var int
     *
     * @ORM\Column(name="max_person", type="integer")
     */
    private $max_person;

    /**
     * @var int
     *
     * @ORM\Column(name="room_count", type="integer")
     */
    private $room_count;


    /**
     * @var bool
     *
     * @ORM\Column(name="smoking", type="boolean")
     */
    private $smoking;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"image/jpg","image/jpeg"},
     *     mimeTypesMessage = "Please upload a valid image"
     * )
     * @ORM\Column(name="imageurl", type="string", length=255)
     */
    private $imageurl;


    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Images", mappedBy="rooms",cascade={"all"},orphanRemoval=true)
     */
    private $image;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Property", inversedBy="rooms")
     *
     */
    private $property;

    /**
     * @var int
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ExtraPerson", mappedBy="room", cascade={"all"}, orphanRemoval=true)
     *
     */
    private $extra_person;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Reservation", mappedBy="room")
     */
    private $reservation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Rooms
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return Rooms
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @return float
     */
    public function getSingleCpCost()
    {
        return $this->single_cp_cost;
    }

    /**
     * @param float $single_cp_cost
     */
    public function setSingleCpCost($single_cp_cost)
    {
        $this->single_cp_cost = $single_cp_cost;
    }

    /**
     * @return float
     */
    public function getSingleEpCost()
    {
        return $this->single_ep_cost;
    }

    /**
     * @param float $single_ep_cost
     */
    public function setSingleEpCost($single_ep_cost)
    {
        $this->single_ep_cost = $single_ep_cost;
    }

    /**
     * @return float
     */
    public function getSingleApCost()
    {
        return $this->single_ap_cost;
    }

    /**
     * @param float $single_ap_cost
     */
    public function setSingleApCost($single_ap_cost)
    {
        $this->single_ap_cost = $single_ap_cost;
    }

    /**
     * @return float
     */
    public function getSingleMapCost()
    {
        return $this->single_map_cost;
    }

    /**
     * @param float $single_map_cost
     */
    public function setSingleMapCost($single_map_cost)
    {
        $this->single_map_cost = $single_map_cost;
    }

    /**
     * @return float
     */
    public function getDoubleEpCost()
    {
        return $this->double_ep_cost;
    }

    /**
     * @param float $double_ep_cost
     */
    public function setDoubleEpCost($double_ep_cost)
    {
        $this->double_ep_cost = $double_ep_cost;
    }

    /**
     * @return float
     */
    public function getDoubleCpcost()
    {
        return $this->double_cp_cost;
    }

    /**
     * @param float $double_cp_cost
     */
    public function setDoubleCpCost($double_cp_cost)
    {
        $this->double_cp_cost = $double_cp_cost;
    }

    /**
     * @return float
     */
    public function getDoubleApCost()
    {
        return $this->double_ap_cost;
    }

    /**
     * @param float $double_ap_cost
     */
    public function setDoubleApCost($double_ap_cost)
    {
        $this->double_ap_cost = $double_ap_cost;
    }

    /**
     * @return float
     */
    public function getDoubleMapCost()
    {
        return $this->double_map_cost;
    }

    /**
     * @param float $double_map_cost
     */
    public function setDoubleMapCost($double_map_cost)
    {
        $this->double_map_cost = $double_map_cost;
    }

    // /**
    //  * @return float
    //  */
    // public function getExtraBed()
    // {
    //     return $this->extra_bed;
    // }

    // /**
    //  * @param float $extra_bed
    //  */
    // public function setExtraBed($extra_bed)
    // {
    //     $this->extra_bed = $extra_bed;
    // }

    /**
     * Set Extra Person Cost
     * @param AppBundle\Entity\ExtraPerson $extra_person
     * @return Rooms
     */
    public function setExtraPerson($extra_person) 
    {
        $this->extra_person = $extra_person;

        return $this;
    }

    /**
     * Get Extra Person Cost 
     * @return AppBundle\Entity\ExtraPerson
     */
    public function getExtraPerson()
    {
        return $this->extra_person;
    }

    /**
     * Set adult
     *
     * @param integer $adult
     *
     * @return Rooms
     */
    public function setAdult($adult)
    {
        $this->adult = $adult;

        return $this;
    }

    /**
     * Get adult
     *
     * @return int
     */
    public function getAdult()
    {
        return $this->adult;
    }

    /**
     * @return int
     */
    public function getMaxPerson()
    {
        return $this->max_person;
    }

    /**
     * @param int $max_person
     */
    public function setMaxPerson($max_person)
    {
        $this->max_person = $max_person;
    }



    /**
     * @return int
     */
    public function getRoomCount()
    {
        return $this->room_count;
    }

    /**
     * @param int $room_count
     */
    public function setRoomCount($room_count)
    {
        $this->room_count = $room_count;
    }



    /**
     * Set smoking
     *
     * @param boolean $smoking
     *
     * @return Rooms
     */
    public function setSmoking($smoking)
    {
        $this->smoking = $smoking;

        return $this;
    }

    /**
     * Get smoking
     *
     * @return bool
     */
    public function getSmoking()
    {
        return $this->smoking;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Rooms
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set imageurl
     *
     * @param string $imageurl
     *
     * @return Rooms
     */
    public function setImageurl($imageurl)
    {
        $this->imageurl = $imageurl;
        return $this;
    }
    /**
     * Get imageurl
     *
     * @return string
     */
    public function getImageurl()
    {
        return $this->imageurl;
    }


    /**
     * Set property
     *
     * @param \AppBundle\Entity\Property $property
     *
     * @return Rooms
     */
    public function setproperty(\AppBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \AppBundle\Entity\Property
     */
    public function getproperty()
    {
        return $this->property;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reservation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     *
     * @return Rooms
     */
    public function addReservation(\AppBundle\Entity\Reservation $reservation)
    {
        $this->reservation[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\AppBundle\Entity\Reservation $reservation)
    {
        $this->reservation->removeElement($reservation);
    }

    /**
     * Get reservation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    public function __toString()
    {
        return $this->getType();
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Rooms
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->image[] = $image;
        $image->setRooms($this);
        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }
}
