<?php
/**
 * Created by PhpStorm.
 * User: Demi
 * Date: 26-Feb-18
 * Time: 2:36 PM
 */
namespace AppBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserType extends AbstractType
{
    private $authorization;
    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorization = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('roles', ChoiceType::class, array(
//            'mapped' => false,
//            'required' => false,
//            'label'    => 'Role',
//            'choices' => array(
//                'User' => 'ROLE_USER',
//                'Staff' => 'ROLE_STAFF',
//                'Institute' => 'ROLE_INSTITUTE',
//            ),
//            'expanded'   => true,
//        ))
//        ->add('enabled')
        ->add('firstName')
        ->add('lastName')
        ->add('email')
        ->add('phone',TextType::class,array(
            'attr'=>array(
                'min' => '10',
                'max' => '10',
            )
        ))

        ;

        if($this->authorization->isGranted('ROLE_ADMIN')) {
            $builder->add('category', ChoiceType::class, array(
                'choices' => array(
                    'GradeA' => 1,
                    'GradeB' => 2,
                    'GradeC' => 3
                ),
                'placeholder' => '--Select Grade--'
            ))
                ->add('roles', ChoiceType::class, array(
                    'mapped' => false,
                    'required' => false,
                    'label'    => 'Role',
                    'choices' => array(
                        'Admin' => 'ROLE_ADMIN',
                        'Host' => 'ROLE_HOST',
                        'Agent' => 'ROLE_AGENT',
                    ),
                    'placeholder' => '--- Assign Role ---'
                ))
                ->add('enabled', ChoiceType::class, array(
                    'choices' => array(
                        'Active' => true,
                        'Inactive' => false
                    ),
                    'placeholder' => '--Select Status--'
                ));
        }
    }

//    public function getParent()
//    {
//        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
//    }

    public function getBlockPrefix()
    {
        return 'app_user_edit';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}