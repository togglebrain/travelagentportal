<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserDetailsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firm', TextType::class,array(
                'label'=>'Firm/Travel Agent Name',
                'required'=> true
            ))
            ->add('address',TextareaType::class,array(
                'label'=>'Your Address',
                'required'=> false
            ))
            ->add('website',TextType::class,array(
                'required'=> false
            ))
            ->add('source', ChoiceType::class,array(
                'label'=>'How did you hear about Ourguest',
                'choices'=>array(
                    'Travel Agent Reference'=>'Travel Agent',
                    'Facebook'=>'Facebook',
                    'Instagram'=>'Instagram',
                    'Twitter'=>'Twitter',
                    'Other' => 'Other'
                ),
                'placeholder'=>'---Select from option---',
                'required'=> true
            ))
            ->add('other',TextType::class,array(
                'required'=> false,
                'attr'=>array(
                    'placeholder'=>'---Please Specify---'
                )
            ))
            ->add('message',TextareaType::class,array(
                'required'=> false
            ))
            ->add('user');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UserDetails'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_userdetails';
    }


}
