<?php

namespace AppBundle\Form;

use OurguestBundle\Entity\Experiences;
use OurguestBundle\Entity\Facilities;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('description')
            ->add('url')
            ->add('category',ChoiceType::class,array(
                'choices'=>array(
                    '1 Star' => '1',
                    '2 Star' => '2',
                    '3 Star' => '3',
                    '4 Star' => '4',
                    '5 Star' => '5'
                )
            ))
            ->add('location')
//            ->add('host')
//            ->add('videoUrl')
//            ->add('facilities')
//            ->add('experiences',EntityType::class,array(
//                'class' => Experiences::class,
//                'multiple' => true,
//                'by_reference' => false,
////                'expanded' => true
//            ))
            ->add('host')

            ->add('image', CollectionType::class, array(
                'entry_type' => ImagesType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
                'required' => false
            ))
            ->add('featimage',FileType::class,array(
                'label'=>'Featured Image',
                'required'=>false,
                'data_class' => null
            ))
            ->add('altText', TextType::class, array('attr'=>array('placeholder'=>'Alt Text'), 'required'=>false))
            ->add('isFeatured')
            ->add('display', ChoiceType::class, array(
                'choices' => array(
                    'TA Site' => 1,
                    'OG Site' => 2,
                    'Both' => 0,
                ),
            ))
            ->add('experiences', EntityType::class, array(
                'label' => "Select Experiences",
                'class' => Experiences::class,
                'multiple' => true,
                'by_reference' => false,
                'expanded' => true,
            ))
            ->add('facilities', EntityType::class, array(
                'label' => 'Facilities Available',
                'class' => Facilities::class,
                'multiple' => true,
                'by_reference' => false,
                'expanded' => true,
            ))
            ->add('keywords', TextType::class, array(
                'label' => 'Keywords',
                'required' => true,
            ))
            ->add('good', TextareaType::class, array(
                'label' => 'What\'s Good',
                'required' => false,
            ))
            ->add('notGood', TextareaType::class, array(
                'label' => 'What\'s not Good',
                'required' => false,
            ))
            ->add('maxInfantAge', ChoiceType::class, array(
                'label' => 'Set Max Infant Age',
                'required' => true,
                'choices' => array(
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8,
                    '9' => 9,
                    '10' => 10,
                    '11' => 11,
                    '12' => 12,
                    '13' => 13,
                ),
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'Select'
            ))
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Property'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_property';
    }


}
