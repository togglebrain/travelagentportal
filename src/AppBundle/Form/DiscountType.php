<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DiscountType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userCategory', ChoiceType::class, array(
                'label' => 'User Category',
                'choices' => array(
                    'Grade A' => 1,
                    'Grade B' => 2,
                    'Grade C' => 3
                ),
                'placeholder' => '--Select Grade--'
            ))
            ->add('startDate', DateType::class, array(
                'label' => 'Start Date',
            ))
            ->add('endDate', DateType::class, array(
                'label' => 'End Date',
            ))
            ->add('rate', TextType::class, array(
                'label' => 'Discount %'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Discount',
            'property' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */

    public function getBlockPrefix()
    {
        return 'appbundle_discount';
    }
}