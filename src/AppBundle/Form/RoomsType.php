<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\ExtraPersonType;

class RoomsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type')->add('rate')->add('adult')->add('maxPerson')
//            ->add('amenities', ChoiceType::class, array(
//                'choices' => array(
//                    'Wifi' => 'fa-wifi',
//                    'Cab Service' => 'fa-cab',
//                    'Travel Desk'=>'fa-plane',
//                    'CCTV'=> 'fa-file-video-o',
//                    'Dr. On Call' => 'fa-medkit',
//                    'Power Backup' => 'fa-plug',
//                    'Laundry' => 'fa-shopping-basket',
//                    'Room Service' => 'fa-coffee',
//                    'Food' => 'fa-cutlery',
//                    'Casino' => 'fa-support',
//                    'AC'    => 'fa-snowflake-o',
//                    'Lift' => 'fa-sort-numeric-asc',
//                    'Parking' => 'fa-product-hunt',
//                    'Bar' => 'fa-glass',
//                    'Swimming Pool' => 'fa-stumbleupon'
//
//                ),
//                'multiple' => true,
//                'expanded' => true
//            ))
            ->add('smoking',ChoiceType::class,array(
                'choices'=>array(
                    'Yes'=> true,
                    'No'=> false
                )
            ))
            ->add('description')
            ->add('property')
            ->add('single_cp_cost',TextType::class,array(
                'label'=>'Individual CP Cost'
            ))
            ->add('single_ep_cost',TextType::class,array(
                'label'=>'Individual EP Cost'
            ))
            ->add('single_ap_cost',TextType::class,array(
                'label'=>'Individual AP Cost'
            ))
            ->add('single_map_cost',TextType::class,array(
                'label'=>'Individual MAP Cost'
            ))
            ->add('double_cp_cost', TextType::class, array(
                'label' => 'Dual CP Cost'
            ))
            ->add('double_ep_cost', TextType::class, array(
                'label' => 'Dual EP Cost'
            ))->add('double_ap_cost', TextType::class, array(
                'label' => 'Dual AP Cost'
            ))->add('double_map_cost', TextType::class, array(
                'label' => 'Dual MAP Cost'
            ))
            ->add('image', CollectionType::class, array(
                'entry_type' => ImagesType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
                'required' => false
            ))
            ->add('roomCount',TextType::class,array(
                'label'=> 'No. of Rooms'
            ))
            ->add('extra_person', ExtraPersonType::class, array(
                'label' => 'Extra Person Cost'
            ))

        ->add('imageurl',FileType::class,array(
            'label'=>'Room Thumbnail',
            'required'=>false,
            'data_class' => null
        ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Rooms'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_rooms';
    }


}
