<?php

namespace AppBundle\Form;

use AppBundle\Repository\RoomsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;

class ReservationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propertyId = $options['property'];
        $builder
            ->add('checkIn', DateType::class)
            ->add('checkOut',DateType::class)
            ->add('status',CheckboxType::class,array(
                'label'=>'Confirm Payment'
            ))
            ->add('room',EntityType::class,array(
                'class'=>'AppBundle\Entity\Rooms',
                'query_builder' => function(RoomsRepository $repository) use ($propertyId){
                    return $repository->filterByProperty($propertyId);
                }
            ))
            ->add('user')
            ->add('plan');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Reservation',
            'property' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_reservation';
    }


}
