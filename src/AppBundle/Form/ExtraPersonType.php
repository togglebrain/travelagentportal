<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExtraPersonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('room')
            ->add('adult_ep_cost', TextType::class, array(
                'label' => 'Adult EP Cost',
                'required' => true
            ))
            ->add('adult_cp_cost', TextType::class, array(
                'label' => 'Adult CP Cost',
                'required' => true
            ))
            ->add('adult_ap_cost', TextType::class, array(
                'label' => 'Adult AP Cost',
                'required' => true
            ))
            ->add('adult_map_cost', TextType::class, array(
                'label' => 'Adult MAP Cost',
                'required' => true
            ))
            ->add('child_with_bed_ep_cost', TextType::class, array(
                'label' => 'Child With Bed EP Cost',
                'required' => true
            ))
            ->add('child_with_bed_cp_cost', TextType::class, array(
                'label' => 'Child With Bed CP Cost',
                'required' => true
            ))
            ->add('child_with_bed_ap_cost', TextType::class, array(
                'label' => 'Child With Bed AP Cost',
                'required' => true
            ))
            ->add('child_with_bed_map_cost', TextType::class, array(
                'label' => 'Child With Bed MAP Cost',
                'required' => true
            ))
            ->add('child_without_bed_ep_cost', TextType::class, array(
                'label' => 'Child Without Bed EP Cost',
                'required' => true
            ))
            ->add('child_without_bed_cp_cost', TextType::class, array(
                'label' => 'Child Without Bed CP Cost',
                'required' => true
            ))
            ->add('child_without_bed_ap_cost', TextType::class, array(
                'label' => 'Child Without Bed AP Cost',
                'required' => true
            ))
            ->add('child_without_bed_map_cost', TextType::class, array(
                'label' => 'Child Without Bed MAP Cost',
                'required' => true
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ExtraPerson'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_extra_person';
    }
}