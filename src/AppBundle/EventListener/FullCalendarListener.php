<?php
/**
 * Created by PhpStorm.
 * User: ejjak
 * Date: 08/03/19
 * Time: 9:29 PM
 */

namespace AppBundle\EventListener;

use AppBundle\Repository\ReservationRepository;
use Toiba\FullCalendarBundle\Entity\Event;
use Toiba\FullCalendarBundle\Event\CalendarEvent;
use Doctrine\ORM\EntityManager;

class FullCalendarListener
{
//    private $reservationRepository;
//
//    public function __construct(
//        ReservationRepository $reservationRepository
//    ) {
//        $this->reservationRepository = $reservationRepository;
//    }

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function loadEvents(CalendarEvent $calendar)
    {

        $postDate1=  '2019-03-09';
        $checkin = date('Y-m-d', strtotime($postDate1));
        $postDate2= '2019-03-18';
        $checkout = date('Y-m-d', strtotime($postDate2));

        $startDate = $calendar->getStart();
        $endDate = $calendar->getEnd();
        $filters = $calendar->getFilters();
//        $bookings = $this->reservationRepository
//            ->createQueryBuilder('b')
//            ->andWhere('b.beginAt BETWEEN :startDate and :endDate')
//            ->setParameter('startDate', $startDate->format('Y-m-d H:i:s'))
//            ->setParameter('endDate', $endDate->format('Y-m-d H:i:s'))
//            ->getQuery()
//            ->getResult()
//        ;

        $companyEvents = $this->entityManager->getRepository('AppBundle:Reservation')
            ->createQueryBuilder('rev')
            ->leftJoin('rev.room','rm')
            ->leftJoin('rm.property','p')
            ->where('p.id = :id')
            ->setParameter('id', 1)
//            ->add('select','SUM(room.roomCount) - SUM(rev.roomBooked)')
//            ->where('rev.roomBooked BETWEEN :startDate and :endDate')
//            ->setParameter('startDate', $startDate->format('Y-m-d H:i:s'))
//            ->setParameter('endDate', $endDate->format('Y-m-d H:i:s'))
            ->getQuery()->getResult();

////        $postDate1=  str_replace("/","-",$_POST['checkin']);
//        $checkin = $startDate->format('Y-m-d H:i:s');
////        $postDate2= str_replace("/","-",$_POST['checkout']);
//        $checkout = $endDate->format('Y-m-d H:i:s');
//
//        $propertyId= 1;
        $conn = $this->entityManager->getConnection();
//        $sql = 'SELECT rm.id,rm.type,rv.checkIn,rv.checkOut,
//                rm.room_count - IFNULL(rv.reserved_rooms, 0) AS free_rooms
//                FROM rooms rm
//                LEFT JOIN (
//                SELECT rv.room_id,
//                COUNT(1) AS num_reserved_rooms,
//                SUM(rv.room_booked) AS reserved_rooms,rv.checkIn, rv.checkOut
//                FROM reservation rv
//                LEFT JOIN reservation as rv2 ON rv2.room_id = rv2.id
//                WHERE rv.checkIn < rv2.checkOut AND rv.checkOut > rv2.checkIn
//                GROUP BY rv.room_id
//                ) rv ON rv.room_id = rm.id
//                LEFT JOIN property AS p ON rm.property_id = p.id WHERE p.name = ?';

//        $sql = 'SELECT rm.id,rm.type,rv.checkIn,rv.checkOut, rm.room_count - rv.room_booked AS free_rooms FROM reservation rv
//LEFT JOIN rooms as rm ON rm.id = rv.room_id
//LEFT JOIN reservation as rv2 ON rv2.room_id = rv2.id
//LEFT JOIN property AS p ON rm.property_id = p.id WHERE p.name = ? ORDER BY rm.id';
//
        $sql = 'SELECT rm.id,rm.type,rv.checkIn, rv.checkOut, rm.room_count - IFNULL(SUM(rv.room_booked), 0) AS free_rooms
FROM rooms rm
LEFT JOIN reservation rv ON rm.id = rv.room_id
LEFT JOIN property AS p ON rm.property_id = p.id WHERE p.name = ?
AND rv.checkIn <= rv.checkOut AND rv.checkOut >= rv.checkIn
GROUP BY rm.id, rv.room_booked';
//        $sql = 'SELECT r.id,r.type,r.imageurl,r.description,r.amenities,r.rate,r.plan,r.cost,r.smoking,r.adult FROM rooms as r LEFT JOIN property as p ON r.property_id = p.id WHERE p.id = ? AND r.plan = ? AND r.id NOT IN ( SELECT DISTINCT room_id FROM reservation WHERE checkIn <= ? AND checkOut >= ?)';
        $stmt = $conn->prepare($sql);
//        $stmt->bindValue(1, $endDate->format('Y-m-d H:i:s'));
//        $stmt->bindValue(2, $startDate->format('Y-m-d H:i:s'));
        $stmt->bindValue(1, 'Mazong');
//        $stmt->bindValue(4, $adult);
//        $stmt->bindValue(5, $room);
        $stmt->execute();
        $book = $stmt->fetchAll();

//        dump($companyEvents);die;
//        return $stmt->fetchAll();

//        dump($book);die;
//        $res = array();
        foreach ($book as $booking) {

            $checkin = new \DateTime($booking['checkIn']);
            $checkout = new \DateTime($booking['checkOut']);
            $bookingEvent = new Event(
                $booking['type'],
                $booking= $checkin,
                $booking= $checkout
//                $booking->getCheckOut() // If the end date is null or not defined, it creates a all day event
            );



//            $res[] = array(
//               $booking->getCheckIn()
//            );
//            // this create the events with your own entity (here booking entity) to populate calendar
////            $sd = \DateTime::createFromFormat('Y-m-d H:i:s', $booking['checkIn']);
//
//            $bookingEvent = new Event(
////                $booking['type'],
////                $booking= $sd
//                $booking->getPlan(),
//                $booking->getCheckIn()
////                $booking->getCheckOut() // If the end date is null or not defined, it creates a all day event
//            );
//
//            $daterange = new \DatePeriod($booking->getCheckIn(), new \DateInterval('P1D'), $booking->getCheckOut());
//
//            foreach($daterange as $date){
////                echo $date->format("Y-m-d H:i:s") . "<br>";
//                dump($date->format("Y-m-d H:i:s"));
//            }



            /*
             * Optional calendar event settings
             *
             * For more information see : Toiba\FullCalendarBundle\Entity\Event
             * and : https://fullcalendar.io/docs/event-object
             */
            // $bookingEvent->setUrl('http://www.google.com');
//             $bookingEvent->setAllDay($booking->getRoomBooked());
            // $bookingEvent->setCustomField('borderColor', $booking->getColor());

            // finally, add the booking to the CalendarEvent for displaying on the calendar
            $calendar->addEvent($bookingEvent);
        }
//        dump($res);


//        // You may want to make a custom query to populate the calendar
//
//        $calendar->addEvent(new Event(
//            'Event 1',
//            new \DateTime('Tuesday this week'),
//            new \DateTime('Wednesdays this week')
//        ));
//
//        // If the end date is null or not defined, it creates a all day event
//        $calendar->addEvent(new Event(
//            'Event All day',
//            new \DateTime('Friday this week')
//        ));
    }
}