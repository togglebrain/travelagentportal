<?php
/**
 * Created by PhpStorm.
 * User: ejjak
 * Date: 08/05/18
 * Time: 8:23 PM
 */

namespace AppBundle\EventListener;


use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RegistrationListener implements EventSubscriberInterface
{


    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => array(
                array('onRegistrationSuccess',  -10),
            ),
        );
    }

    public function onRegistrationSuccess(FormEvent $event) {
        $form = $event->getForm();
        $email = $form['email']->getData();
        $name = $form['firstName']->getData();
        $html = "
        <div style='background: #FFF; box-shadow: 5px 5px 5px #000; border: 1px dashed #dbdbdb; padding: 40px 30px'>
        <strong id=\"docs-internal-guid-3369a4d6-40ba-04d4-f524-c8ae5316a74c\">
<p dir=\"ltr\">Dear $name,<br />
  <br />
  Congratulations, We have successfully registered your Agency with Ourguest.in. <br />
  <br />
</p>
<!--
<p dir=\"ltr\">Please follow the below steps to login into the OurGuest TA Site :</p>
<ol>
  <li dir=\"ltr\">
    <p dir=\"ltr\">Click on the link : <a href=\"http://www.ourguest.in/Login.php\">www.Ourguest.in/Login.php</a></p>
  </li>
  <li dir=\"ltr\">
    <p dir=\"ltr\">Login with below credentials</p>
  </li>
</ol>
<p dir=\"ltr\">Username: [Travel Agency Email Id]<br />
  Temporary Password: [system generated password</p>
<ol start=\"3\">
  <li dir=\"ltr\">
    <p dir=\"ltr\">Reset the password</p>
  </li>
  <li dir=\"ltr\">
    <p dir=\"ltr\">Please remember your login details for future usage.</p>
  </li>
</ol>
-->
<p dir=\"ltr\"><br />
  Support<br />
  For any queries with respect to your relationship with us you can always contact us directly using the following Information.<br />
  <br />
  Email Address: ourguesttravel@gmail.com<br />
  Tel No.: +91 8286088321 / +91 9733503347</p>
<p dir=\"ltr\">Note:<br />
  In case you are receiving our e-mail in your Junk-mail/Spam, mark this e-mail as Not Junk/Spam or add it to your Safe Sender's list.</p>
<br />
<p dir=\"ltr\">Unsubscribe Instructions:<br />
  You are subscribed to this email announcement because you are a Customer. If you do not wish to receive any further announcements from us you can unsubscribe using the instructions below:</p>
<p dir=\"ltr\"><br />
  1. Click on the link <a href=\"http://www.ourguest.in/unsubscribe\">http://www.ourguest.in/unsubscribe<br />
</a>2. Select 'NO' radio button and submit.</p>
<br />
<p dir=\"ltr\">OurGuest</p>
<p dir=\"ltr\">This is a system generated email please do not reply to this email id</p>
</strong><br>
</div>
        ";
        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
            ->setUsername('ourguesttravel@gmail.com')
            ->setPassword('hvqpbzwijzeenipx');

        $mailer = \Swift_Mailer::newInstance($transport);
        $message = \Swift_Message::newInstance();
        $message->setSubject('Ourguest Account Created')
            ->setFrom(array("a@gmail.com"=>"Ourguest.in"))
            ->setTo(array(
                $email=>'Ourguest',
                'ourguesttravel@gmail.com'=>'Ourguest',
            ))
            ->setBody($html, 'text/html');

        $mailer->send($message);
    }
}