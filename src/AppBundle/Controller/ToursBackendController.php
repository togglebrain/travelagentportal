<?php

namespace AppBundle\Controller;

use OurguestBundle\Entity\Tours;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Tours Backend controller.
 * Class ToursBackendController
 * @package AppBundle\Controller
 * @Route("admin")
 */
class ToursBackendController extends Controller
{
    /**
     * Lists all tour entities.
     *
     * @Route("/tour", name="tour_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tours = $em->getRepository('OurguestBundle:Tours')->findAll();

        return $this->render('ourguest/tours/backend/index.html.twig', array(
            'tours' => $tours,
        ));
    }

    /**
     * Creates a new tour entity.
     *
     * @Route("/tour/new", name="tour_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tour = new Tours();
        $form = $this->createForm('OurguestBundle\Form\ToursType', $tour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $tour->getFeatimage();
            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('directory'),
                $fileName
            );
            $tour->setFeatimage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($tour);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'New Tour added successfully!');

            return $this->redirectToRoute('tour_index');
        }

        return $this->render('ourguest/tours/backend/new.html.twig', array(
            'tour' => $tour,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tour entity.
     *
     * @Route("/tour/{id}/show", name="tour_show")
     * @Method("GET")
     */
    public function showAction(Tours $tour)
    {
        $deleteForm = $this->createDeleteForm($tour);

        return $this->render('ourguest/tours/backend/show.html.twig', array(
            'tour' => $tour,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tour entity.
     *
     * @Route("tour/{id}/edit", name="tour_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Tours $tour)
    {
        $fileName=$tour->getFeatimage();
        $deleteForm = $this->createDeleteForm($tour);
        $tour->setFeatimage(
            new File($this->getParameter('directory').'/'.$tour->getFeatimage())
        );
        $editForm = $this->createForm('OurguestBundle\Form\ToursType', $tour);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $file = $tour->getFeatimage();
            if ($file)
            {
                $file_path='images/uploads/'.$fileName;
                unlink($file_path);
                $fileName1 = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('directory'),
                    $fileName1
                );
                $tour->setFeatimage($fileName1);

            }
            else
            {
                $tour->setFeatimage($fileName);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($tour);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Tour updated successfully!');
            return $this->redirectToRoute('tour_index');
        }

        return $this->render('ourguest/tours/backend/edit.html.twig', array(
            'tour' => $tour,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tour entity.
     *
     * @Route("tour/{id}", name="tour_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Tours $tour)
    {
        $fileName=$tour->getFeatimage();
        $form = $this->createDeleteForm($tour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file_path='images/uploads/'.$fileName;
            unlink($file_path);
            $em = $this->getDoctrine()->getManager();
            $em->remove($tour);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Tour deleted successfully!');
        }

        return $this->redirectToRoute('tour_index');
    }

    /**
     * Creates a form to delete a tour entity.
     *
     * @param Tours $tour The tour entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tours $tour)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tour_delete', array('id' => $tour->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }




    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
