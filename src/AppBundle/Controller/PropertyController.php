<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Property;
use AppBundle\Entity\Rooms;
use AppBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Property controller.
 *
 * @Route("property")
 */
class PropertyController extends Controller
{
    /**
     * Lists all property entities.
     *
     * @Route("/", name="property_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $properties = $em->getRepository('AppBundle:Property')->findAll();

        return $this->render('property/index.html.twig', array(
            'properties' => $properties,
        ));
    }

    /**
     * Creates a new property entity.
     *
     * @Route("/new", name="property_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, FileUploader $fileUploader)
    {
        $property = new Property();
        $form = $this->createForm('AppBundle\Form\PropertyType', $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $property->getFeatimage();
            $fileName = $fileUploader->upload($file);
            $property->setFeatimage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($property);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'New property created successfully !');
            return $this->redirectToRoute('property_index');
        }

        return $this->render('property/new.html.twig', array(
            'property' => $property,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a property entity.
     *
     * @Route("/{id}", name="property_show")
     * @Method("GET")
     */
    public function showAction(Property $property)
    {
        $deleteForm = $this->createDeleteForm($property);

        return $this->render('property/show.html.twig', array(
            'property' => $property,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing property entity.
     *
     * @Route("/{id}/edit", name="property_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Property $property, FileUploader $fileUploader)
    {
        $fileName=$property->getFeatimage();
        $deleteForm = $this->createDeleteForm($property);
        if (file_exists($this->getParameter('directory').'/'.$fileName))
            $property->setFeatimage(
                new File($this->getParameter('directory').'/'.$property->getFeatimage())
            );
        $editForm = $this->createForm('AppBundle\Form\PropertyType', $property);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $file = $property->getFeatimage();
            if ($file)
            {
                $file_path='images/uploads/'.$fileName;
                file_exists($file_path) ? unlink($file_path) : null;
                $fileName1 = $fileUploader->upload($file);
                $property->setFeatimage($fileName1);

            }
            else
            {
                $property->setFeatimage($fileName);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($property);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Property updated successfully !');
            return $this->redirectToRoute('property_index');
        }

        return $this->render('property/edit.html.twig', array(
            'property' => $property,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'propertyThumbnail' => $fileName
        ));
    }

    /**
     * Deletes a property entity.
     *
     * @Route("/{id}", name="property_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Property $property)
    {
        $fileName=$property->getFeatimage();
        $form = $this->createDeleteForm($property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file_path='images/uploads/'.$fileName;
            unlink($file_path);
            $em = $this->getDoctrine()->getManager();
            $em->remove($property);
            $em->flush();
        }

        return $this->redirectToRoute('property_index');
    }

    /**
     * Creates a form to delete a property entity.
     *
     * @param Property $property The property entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Property $property)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('property_delete', array('id' => $property->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * get Room costs
     * 
     * @Route("/{propertyId}/room_costs", name="get_room_costs", options={"expose"=true})
     * @Method("GET")
     */
    public function getPropertyRoomsCosts(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $propertyId = $request->attributes->get('propertyId');

        $rooms = $em->getRepository('AppBundle:Rooms')->getRoomsCosts($propertyId);
        
        return new JsonResponse($rooms);
    }
}
