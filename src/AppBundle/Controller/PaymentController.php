<?php 

namespace AppBundle\Controller;

use paytm\checksum\PaytmChecksumLibrary;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
/**
 * Class PaymentController
 * @package AppBundle\Controller
 * @Route("payment")
 */
class PaymentController extends Controller {
    /**
     * @Route("/initiate", name="initiate_transaction")
     */
    public function initiateTransaction(Request $request) {
        $paytmParams = array();
        $paytmParams["body"] = array(
            "requestType"   => "Payment",
            "mid"           => "bhglZL96218262699517",
            "websiteName"   => "WEBSTAGING",
            "orderId"       => "ORDERID_98765",
            "callbackUrl"   => "",
            "txnAmount"     => array(
                "value"     => "1700.00",
                "currency"  => "INR"
            ),
            "userInfo"      => array(
                "custId"    => "CUST_001"
            )
        );

        //checksum required for verifying transaction
        $checksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), "bhglZL96218262699517");
        $paytmParams["head"] = array(
            "signature" => $checksum
        );

        $post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);

        $url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=bhglZL96218262699517&orderId=ORDERID_98765";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        $response = curl_exec($ch);

        print_r($response);
    }
}