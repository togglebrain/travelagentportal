<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DiscountDefRates;
use Symfony\Component\Form\AbstractType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default Discount Rates Controller
 *
 * @Route("defrates")
 */

class DiscountDefRatesController extends Controller
{
    /**
     * Lists all Default Discount Rates
     * @Route("/", name="defrates_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $defrates = $em->getRepository('AppBundle:DiscountDefRates')->findAll();
        return $this->render('defrates/index.html.twig', array(
            'defrates' => $defrates,
        ));
    }

    /**
     * Adds a new Default Discount Rate
     * @Route("/new", name="defrates_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $defrates = new DiscountDefRates;
        $form = $this->createForm('AppBundle\Form\DiscountDefRatesType', $defrates);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($defrates);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'New Default Discount added successfully!');
            return $this->redirectToRoute('defrates_index');
        }
        return $this->render('defrates/new.html.twig', array(
            'defrates'=>$defrates,
            'form'=>$form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Default Discount Entity
     *
     * @Route("/{id}/edit", name="defrates_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, DiscountDefRates $defRates)
    {
        $editForm = $this->createForm('AppBundle\Form\DiscountDefRatesType', $defRates);
        $editForm->handleRequest($request);
        $deleteForm = $this->createDeleteForm($defRates);

        if($editForm->isSubmitted() && $editForm->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()->getFlashBag()
                ->add('success', 'Default Discount Rates updated successfully!');
            return $this->redirectToRoute('discount_index');
        }
        return $this->render('defrates/edit.html.twig',array(
            'defrates' => $defRates,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a DiscountDefRates Entity
     *
     * @Route("/{id}", name="defrates_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, DiscountDefRates $defRates){
        $form = $this->createDeleteForm($defRates);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($defRates);
            $em->flush();
        }
        return $this->redirectToRoute('discount_index');
    }

    /**
     * Creates a form to delete default discount rate entity.
     *
     * @param DiscountDefRates $defRates
     *
     * @return \Symfony\Component\Form\Form
     */
    public function createDeleteForm(DiscountDefRates $defRates){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('defrates_delete', array('id' => $defRates->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}