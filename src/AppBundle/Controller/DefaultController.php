<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Images;
use AppBundle\Entity\Property;
use AppBundle\Entity\Reservation;
use AppBundle\Entity\Rooms;
use AppBundle\Entity\DiscountDefRates;
use AppBundle\Entity\User;
use AppBundle\Form\ReservationType;
use OurguestBundle\Entity\Suggested;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 * @Route("og-admin")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="og_admin")
     */
    public function ogAdminAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/portal-home", name="landing")
     */
    public function landingAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/landing.html.twig');
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function adminAction(Request $request)
    {
        return $this->render(':default:dashboard.html.twig');
    }


    /**
     * @Route("/property-list", name="property_list")
     */
    public function propertyListAction(Request $request)
    {
        $session = $request->getSession();
        $filter = $session->get('postData', array());
        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository('AppBundle:Property')->findTAProperty();
        $locations = $em->getRepository('AppBundle:Location')->findLocation();
        $res=array();
        $data=array();
        foreach ($property as $var)
        {

            $id = $var->getId();
            $name = $var->getName();
            $room = $var->getRooms();
            foreach ($room as $item) {
                if($item instanceof Rooms)
                {
                    $room=$item->getType();
                }
            }

            $res[]= array('id'=>$id,'name'=>$name,'room'=>$room);
        }
        foreach ($locations as $var){
            $location = $var->getName();
            $locationId = $var->getId();
            $data[] = array(
                'location' => $location,
                'id'=>$locationId
            );

        }
        return $this->render(':default:search-form.html.twig', array(
            'res'=>$res,
            'data'=>$data,
            'postData'=>$filter
        ));
    }


    /**
     * @Route("/calendar/result", name="calendar-search",options={"expose"= true})
     */
    public function calendarSearchAction(Request $request)
    {
        $userGrade = $this->getUser()->getCategory();
        $usedId = $this->getUser()->getID();
        $id = $request->request->get('destination');
        $session = $request->getSession();
        $session->remove('cart');
        $session->set('postData', $request->request->all());
        $filter = $session->get('postData', array());
        $em = $this->getDoctrine()->getManager();
        $rooms = $em->getRepository('AppBundle:Property')->filterCalRoomAction();
        $users = $em->getRepository('AppBundle:DiscountDefRates')->findDefaultRate($userGrade);

        //retrieving property discount object using property name
        $propertyDiscounts = $em->getRepository('AppBundle:PropertyDiscount')->findProperty($id);
        $defRate = 0;

        /* @var $user \AppBundle\Entity\DiscountDefRates */
        foreach($users as $user)
        {
            $defRate = $user->getDefRate();
        }

        $discountRate = 0;

        /* @var $discounts \AppBundle\Entity\PropertyDiscount */
        $checkin = \DateTime::createFromFormat('d-m-Y', $filter['checkin']);
        foreach($propertyDiscounts as $discounts){
            $diff1 = date_diff($discounts->getStartDate(), $checkin);
            $diff2 = date_diff($discounts->getEndDate(), $checkin);
            $diff1 = $diff1->format('%R%a');
            $diff2 = $diff2->format('%R%a');
            if($diff1 >= 0 and $diff2 <= 0){
                $discountRate = $discounts->getRate();
            }
        }

        return $this->render('default/calendar-search-result.html.twig', array(
            'session'=> $session->get('postData'),
            'response'=>$rooms,
            'discountRate'=>$discountRate,
            'defRate'=>$defRate,
        ));
    }

    /**
     * @Route("/search/result", name="search",options={"expose"= true})
     */
    public function searchAction(Request $request)
    {
        $userGrade = $this->getUser()->getCategory();
        $usedId = $this->getUser()->getID();
        $session = $request->getSession();
        $session->set('postData', $request->request->all());
        $filter = $session->get('postData', array());
        $id = $request->request->get('destination');
        
        $em = $this->getDoctrine()->getManager();
        $rooms = $em->getRepository('AppBundle:Property')->filterRoomsAction();
        $detail = array();
        $property = $em->getRepository('AppBundle:Property')->findBy(array('name'=>$id));

        $photos = $em->getRepository('AppBundle:Rooms')->findAll();

        $users = $em->getRepository('AppBundle:DiscountDefRates')->findDefaultRate($userGrade);

        //retrieving property discount object using property name
        $propertyDiscounts = $em->getRepository('AppBundle:PropertyDiscount')->findProperty($id);

        $discountRate = 0;
        $defRate = 0;

        /* @var $user \AppBundle\Entity\DiscountDefRates */
        foreach($users as $user)
        {
            $defRate = $user->getDefRate();
        }


        /* @var $discounts \AppBundle\Entity\PropertyDiscount */
        $checkin = \DateTime::createFromFormat('d-m-Y', $filter['checkin']);
        foreach($propertyDiscounts as $discounts){
            $diff1 = date_diff($discounts->getStartDate(), $checkin);
            $diff2 = date_diff($discounts->getEndDate(), $checkin);
            $diff1 = $diff1->format('%R%a');
            $diff2 = $diff2->format('%R%a');
            if($diff1 >= 0 and $diff2 <= 0){
                $discountRate = $discounts->getRate();
            }
        }
        $res =array();
        foreach($photos as $value)
        {
            if($value instanceof  Rooms)
            {
                $imageDetail= $value->getImage();
                $id= $value->getId();
                $detail = array();
                foreach ($imageDetail as $val) {
                    if ($val instanceof Images) {
                        $detail[] = array(
                            'path' => $val->getPath(),
                            'id' => $val->getId()
                        );
                    }
                }
                $response[] = array('images' => $detail,'id'=>$id);
            }
        }
        foreach($property as $val)
        {
            $id = $val->getId();
            $name = $val->getName();
            $desc = $val->getDescription();
            $image = $val->getImage();
            $detail = array();
            foreach ($image as $images)
            {
                if ($images instanceof Images)
                {
                    $detail[] = array('path' => $images->getPath());
                }
            }
            $res[]= array('id'=>$id,'name'=>$name,'desc'=>$desc,
                'images'=>$detail);
        }
        if ($request->isMethod('POST')) {
            $session->remove('cart');
        }
       
        return $this->render('default/search-result.html.twig', array(
            'session'=> $session->get('postData'),
            'rooms'=>$rooms,
            'res'=>$res,
            'images'=>$response,
            'discountRate'=>$discountRate,
            'defRate'=>$defRate,
        ));
    }
    /**
     * @Route("/search-property", name="search_property")
     */
    public function searchPropertyAction(Request $request)
    {
        $userGrade = $this->getUser()->getCategory();
        $usedId = $this->getUser()->getID();
        $session = $request->getSession();
        $session->set('postData', $request->request->all());
        $filter = $session->get('postData', array());
        $id = $filter['location'];
        $em = $this->getDoctrine()->getManager();
        $detail = array();
        if($id == 0){
            $property = $em->getRepository('AppBundle:Property')->findTAProperty();
        }else{
            $property = $em->getRepository('AppBundle:Property')->findPropertyLocation($id);
        }
        $photos = $em->getRepository('AppBundle:Rooms')->findAll();
        $res =array();
        foreach($photos as $value)
        {
            if($value instanceof  Rooms)
            {
                $imageDetail= $value->getImage();
                $id= $value->getId();
                $detail = array();
                foreach ($imageDetail as $val) {
                    if ($val instanceof Images) {
                        $detail[] = $val->getPath();
                    }
                }
                $response[] = array('images' => $detail,'id'=>$id);
            }
        }
        foreach($property as $val)
        {
            $id = $val->getId();
            $location = $val->getLocation();
            $name = $val->getName();
            $desc = $val->getDescription();
            $image = $val->getImage();
            $room = $val->getRooms()->first();
            $featimg = file_exists($this->getParameter('directory').'/'.$val->getFeatimage()) ? $val->getFeatimage() : null;
            $minRate = $em->getRepository('AppBundle:Rooms')->getCheapestRate($id);
            foreach ($image as $images)
            {
                if ($images instanceof Images)
                {
                    $detail[] = array('path' => $images->getPath());
                }
            }
            if(!$room == null ){

                foreach ($room as $rooms)
                {
                    if ($rooms instanceof Rooms)
                    {
                        $roomType[] = array('name' => $rooms->getType());
                    }
                }
            }
            $res[]= array(
                'id'=>$id,
                'location' => $location,
                'name'=>$name,
                'desc'=>$desc, 
                'featImg'=>$featimg,
                'images'=>$detail,
                'roomtype' => $room,
                'rate' => $minRate
            );
        }
//        dump($res);die;
        return $this->render('default/search-property.html.twig', array(
            'session'=> $session->get('postData'),
            'res'=>$res,
        ));
    }

//    /**
//     * @Route("/rooms-list", name="rooms_data")
//     */
//    public function RoomSearchAction(Request $request)
//    {
//        $session = $request->getSession();
//        $em = $this->getDoctrine()->getManager();
//        $rooms = $em->getRepository('AppBundle:Rooms')->filterRoomsAction($request);
//        return $this->render(':default:rooms-list.html.twig', array(
//            'session'=> $session->get('postData'),
//            'response'=>$rooms,
//        ));
//    }


    /**
     * @Route("/booking", name="booking")
     */
    public function BookingAction(Request $request)
    {
        $userGrade = $this->getUser()->getCategory();
        $userId = $this->getUser()->getID();
        $email = $this->getUser()->getEmail();
        $userName = $this->getUser()->getFirstName();
        $session = $request->getSession();
        $filter = $session->get('postData', array());
        $cart = $session->get('cart',array());
        $form = $this->createFormBuilder()
            ->add('firstname', TextType::class, array(
                'required' => false,
            ))
            ->add('lastname', TextType::class, array(
                'required' => false,
            ))
            ->add('phone', TextType::class, array(
                'required' => false,
                'attr'=> array(
                    'min' => '10',
                    'max' => '10',
                    'maxlength' => 10
                ),
                'constraints' => array(
                    new Assert\Type('digit'),
                    new Assert\Regex(array(
                        'pattern' => '/^[0-9]{10}$/',
                        'message' => 'Please use valid mobile number'
                        )
                    ),
                    new Assert\Length(array('min' => 10,'max' => 10))
                )
            ))
            ->add('address', TextType::class, array(
                'required' => false,
            ))
            ->add('message', TextareaType::class, array(
                'label'=>'Any special request',
                'required' => false,
            ))
            ->add('terms', CheckboxType::class, array(
                'required' => true,

            ))
            ->add('amount', TextType::class, array(
                'required' => false,
            ))
            ->add('amountA', TextType::class, array(
                'required' => false,
            ))
            ->getForm();
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:DiscountDefRates')->findDefaultRate($userGrade);
        $defRate = 0;

        /* @var $user \AppBundle\Entity\DiscountDefRates */
        foreach($users as $user)
        {
            $defRate = $user->getDefRate();
        }
        $propertyDiscounts = $em->getRepository('AppBundle:PropertyDiscount')->findProperty($filter['destination']);
        $discountRate = 0;

        /* @var $discount \AppBundle\Entity\Discount */
        $checkin = \DateTime::createFromFormat('d-m-Y', $filter['checkin']);
        foreach($propertyDiscounts as $discounts){
            $diff1 = date_diff($discounts->getStartDate(), $checkin);
            $diff2 = date_diff($discounts->getEndDate(), $checkin);
            $diff1 = $diff1->format('%R%a');
            $diff2 = $diff2->format('%R%a');
            if($diff1 >= 0 and $diff2 <= 0){
                $discountRate = $discounts->getRate();
            }
        }
        $res =array();
        foreach($cart as $val)
        {
            $id = $val['id'];
            $name = $val['type'];
            $plan = $val['plan'];
            $price = $val['room_cost'];
            // $cp = $val['cp'];
            // $ep = $val['ep'];
            // $ap = $val['ap'];
            // $map = $val['map'];
            // $bed = $val['bed'];
            // $img = $val['imageurl'];
            $adult_quantity = $val['adult_quantity'];
            $child_quantity = $val['child_quantity'];
            // $quantity = $val['quantity'];
            // $total = $val['total'];
            // $gross = $val['gross'];
            // $dPercent = $val['discount'];
            $res[]= array('id'=>$id,'name'=>$name,'price'=>$price,'plan'=>$plan,'adult_quantity'=>$adult_quantity,'child_quantity'=>$child_quantity);
        }
        
        return $this->render('default/reservation.html.twig', array(
            'res'=>$res,
            'postData'=>$filter,
            'discountRate'=>$discountRate,
            'defRate'=>$defRate,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/success", name="booking_success")
     */
    public function BookingSuccessAction(Request $request)
    {
        return $this->render('default/success.html.twig');
    }


    public function FeaturedProopertyAction(Request $request){
        $session = $request->getSession();
        $postData = $session->get('postData', array());
        $em = $this->getDoctrine()->getManager();
        $detail= array();
        $view = $em->getRepository('AppBundle:Property')->featuredProperty();
        foreach($view as $value)
        {
            $minRate = $em->getRepository('AppBundle:Rooms')->getCheapestRate($value->getId()); 
            if ($value instanceof Property) {
                $image = file_exists($this->getParameter('directory').'/'.$value->getFeatimage()) ? $value->getFeatimage() : null;
                $title = $value->getName();
                $desc = $value->getDescription();
                $url = $value->getUrl();
                $id = $value->getId();
                $rooms = $value->getRooms()->first();
                if($rooms instanceof  Rooms){
                    $roomId=$rooms->getId();
                }
                $location = $value->getLocation();
                $detail[] = array(
                    'title' =>$title,
                    'image'=>$image,
                    'id' => $id,
                    'roomId'=>$roomId, 
                    'desc'=>$desc,
                    'url'=>$url, 
                    $rooms, 
                    'location'=>$location, 
                    'locationId' => $location->getId(), 
                    'rate'=>$minRate
                );
            }
        }
        return $this->render(':default:featured-property.html.twig', array(
            'detail' => $detail,
            'postData' => $postData
        ));
    }

    /**
     * @Route("/cart", name="add_cart")
     */
    public function cartProductAction(Request $request){
        $userGrade = $this->getUser()->getCategory();
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $filter = $session->get('postData', array());
        $users = $em->getRepository('AppBundle:DiscountDefRates')->findDefaultRate($userGrade);
        $defRate = 0;

        /* @var $user \AppBundle\Entity\DiscountDefRates */
        foreach ($users as $user)
        {
            $defRate = $user->getDefRate();
        }
        $discounts = $em->getRepository('AppBundle:Discount')->findUserCategory($userGrade);
        $discountRate = 0;

        /* @var $discount \AppBundle\Entity\Discount */
        $checkin = \DateTime::createFromFormat('d-m-Y', $filter['checkin']);
        $checkout = \DateTime::createFromFormat('d-m-Y', $filter['checkout']);
        foreach($discounts as $discount){
            $diff1 = date_diff($discount->getStartDate(), $checkin);
            $diff2 = date_diff($discount->getEndDate(), $checkin);
            $diff1 = $diff1->format('%R%a');
            $diff2 = $diff2->format('%R%a');
            if($diff1 >=0 and $diff2 <= 0){
                $discountRate = $discount->getRate();
            }
        }
        
        $discountRate = $defRate + $discountRate;

        $cart = $em->getRepository('AppBundle:Rooms')->findCartAction($request, $discountRate);
        $count = count($cart);

        return $this->render(':default:cart.html.twig', array(
            'cart' => $cart,
            'count' => $count,
            'postData'=>$session->get('postData'),
            'discountRate'=>$discountRate,
            'defRate'=>$defRate,
            'numOfDays' => date_diff($checkin, $checkout)->format('%a')
        ));
    }


    /**
     * @Route("/rooms-gallery", name="room_gallery")
     */
    public function RoomsGalleryAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $photos = $em->getRepository('AppBundle:Rooms')->findAll();
        foreach($photos as $value)
        {
            if($value instanceof  Rooms)
            {
                $imageDetail= $value->getImage();
                $id= $value->getId();
                $detail = array();
                foreach ($imageDetail as $val) {
                    if ($val instanceof Images) {
                        $detail[] = $val->getPath();
                    }
                }
                $response[] = array('images' => $detail,'id'=>$id);
            }
        }

        return $this->render('default/room-gallery.html.twig', array(
            'images'=>$response,
        ));
    }

    /**
     * @Route("/terms-and-condition", name="terms")
     */
    public function TermsConditionAction(Request $request){
        return $this->render(':default:terms-condition.html.twig');
    }

    /**
     * @Route("/contact", name="contact_us")
     */
    public function ContactAction(Request $request){
        return $this->render(':default:contact-us.html.twig');
    }

    /**
     * @Route("/property-overview", name="property_overview")
    */
    public function PropertyOverviewAction(Request $request) {
        $session = $request->getSession();
        $session->set('postData', $request->request->all());
        $id = $request->request->get('destination');
        $em = $this->getDoctrine()->getManager();
        $rooms = $em->getRepository('AppBundle:Property')->filterRoomsAction();
        $detail = array();
        $property = $em->getRepository('AppBundle:Property')->findBy(array('name'=>$id));
        $photos = $em->getRepository('AppBundle:Rooms')->findAll();
        $res =array();
        foreach($photos as $value)
        {
            if($value instanceof  Rooms)
            {
                $imageDetail= $value->getImage();
                $id= $value->getId();
                $detail = array();
                foreach ($imageDetail as $val) {
                    if ($val instanceof Images) {
                        $detail[] = $val->getPath();
                    }
                }
                $response[] = array('images' => $detail,'id'=>$id);
            }
        }
        foreach($property as $val)
        {
            $id = $val->getId();
            $name = $val->getName();
            $desc = $val->getDescription();
            $image = $val->getImage();
            foreach ($image as $images)
            {
                if ($images instanceof Images)
                {
                    $detail[] = array('path' => $images->getPath());
                }
            }
            $res[]= array('id'=>$id,'name'=>$name,'desc'=>$desc,
                'images'=>$detail);
        }
        return $this->render(':default:property-overview.html.twig', array(
            'session' => $session->get('postData'),
            'response' => $rooms,
            'res'=>$res,
            'images' => $response,
        ));
    }

    /**
     * @Route("/property-listing/{location}", name="property_listing", options={"expose"= true})
     */
    public function listPropertyAction(Request $request)
    {
        // Get Entity manager and repository
        $em = $this->getDoctrine()->getManager();
        $wardsRepository = $em->getRepository("AppBundle:Property");

        $wards = $wardsRepository->createQueryBuilder("q")
            ->leftJoin("q.location",'location')
            ->where("location.id = :id")
            ->andWhere("q.display = :val1 OR q.display = :val2")
            ->setParameter("id", $request->attributes->get('location'))
            ->setParameter('val1', 0)
            ->setParameter('val2', 1)
            ->orderBy('q.name', 'ASC')
            ->getQuery()
            ->getResult();

        $responseArray = array();
        foreach($wards as $ward){
            $responseArray[] = array(
                "id" => $ward->getId(),
                "name" => $ward->getName()
            );
        }
        return new JsonResponse($responseArray);
    }

    /**
     * @Route("/room-availability/{property}/{roomtype}", name="calendar_data",options={"expose"= true})
     */
    public function roomAvailability(Request $request){
        $property = $request->attributes->get('property');
        $roomtype = $request->attributes->get('roomtype');
        $session = $request->getSession();
        $filter = $session->get('postData', array());
        $em = $this->getDoctrine()->getManager();
        $rooms = $em->getRepository('AppBundle:Rooms')->getCalendarData($property,$roomtype);
        $type = $em->getRepository('AppBundle:Rooms')->findRoomType($property);
        $gallery = $em->getRepository('AppBundle:Rooms')->findRoomGallery($property,$roomtype);
        $location = $em->getRepository('AppBundle:Property')->findPropertyLocation($property);

        $res=array();
        $images=array();
        foreach ($rooms as $val){
            if($val['available'] == null){
                $val['available'] = $val['totalroom'];
            }else{
                $val['available'];
            }
            $res[]=array(
                'title' => $val['available'],
                'type' => $val['type'],
                'start' => $val['dt'],
                'end' => $val['dt'],
//                'customRender'=> true,
                'freeRoom'=> $val['available'],
                'roomCount'=> $val['booked'],
                'totalroom'=> $val['totalroom'],
//                'rendering'=>'background',
                'backgroundColor'=>'transparent',
                'color'=> 'transparent',
                'textColor'=>'green',
                'allDay'=>true
            );
        }
        foreach($gallery as $value)
        {
            if($value instanceof  Rooms)
            {
                $imageDetail= $value->getImage();
                foreach ($imageDetail as $val) {
                    if ($val instanceof Images) {
                        $images[] = $val->getPath();
                    }
                }
            }
        }
//        dump($res);die;
//        return new JsonResponse($rooms);
        return $this->render(':default:room-availability.html.twig', array(
            'data' => json_encode($res),
            'roomtype' => $type,
            'images' => $images,
            'postData'=>$filter,
        ));

    }

    /**
     * @Route("/ourguest-settings", name="ourguest_settings")
     * @Method({"GET","POST"})
     */
    public function ourguestSettingsAction(){

        return $this->render('ourguest/default/ourguest-settings.html.twig', array());
    }
}
