<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Discount;
use AppBundle\Entity\DiscountDefRates;
use AppBundle\Entity\PropertyDiscount;
use AppBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Discount Controller.
 *
 * @Route("discount")
 */

class DiscountController extends Controller
{
    /**
     * Lists all discount entities
     *
     * @Route("/", name="discount_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $propertyDiscounts = $em->getRepository('AppBundle:PropertyDiscount')->findAll();
        $discounts = $em->getRepository('AppBundle:DiscountDefRates')->findAll();

        return $this->render('discount/index.html.twig', array(
            'discounts' => $discounts,
            'propertyDiscounts' => $propertyDiscounts,
        ));
    }

    /**
     * Creates a new Discount Entity
     *
     * @Route("/new", name="discount_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $discount = new DiscountDefRates();
        $form = $this->createForm('AppBundle\Form\DiscountDefRatesType', $discount);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($discount);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'New Discount created successfully!');
            return $this->redirectToRoute('discount_index');
        }

        return $this->render('defrates/new.html.twig', array(
            'discount' => $discount,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Discount Entity
     *
     * @Route("/{id}/edit", name="discount_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Discount $discount)
    {
        $editForm = $this->createForm('AppBundle\Form\DiscountType', $discount);
        $editForm->handleRequest($request);

        if($editForm->isSubmitted() && $editForm->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Discount updated successfully!');
            return $this->redirectToRoute('discount_index', array('id' => $discount->getId()));
        }
        return $this->render('defrates/edit.html.twig', array(
            'discount' => $discount,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Discount Entity
     *
     * @Route("/{id}", name="discount_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Discount $discount)
    {
        $deleteForm = $this->createDeleteForm($discount);
        $deleteForm->handleRequest($request);

        if($deleteForm->isSubmitted() && $deleteForm->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($discount);
            $em->flush();
        }
        return $this->render('discount/index.html.twig', array(
            'discount' => '$discount',
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Create a form to delete Discount Entity
     *
     * @param Discount $discount The discount Entity
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm(Discount $discount)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('discount_delete', array('id' => $discount->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}