<?php

namespace AppBundle\Controller;

use OurguestBundle\Entity\Experiences;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

/**
 * ExperienceBackendController.
 * @package AppBundle/Controller
 * @Route("admin")
 */
class ExperiencesBackendController extends Controller
{
    /**
     * Lists all experience entities.
     *
     * @Route("/experience", name="experiences_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $experiences = $em->getRepository('OurguestBundle:Experiences')->findAll();

        return $this->render('ourguest/experiences/backend/index.html.twig', array(
            'experiences' => $experiences,
        ));
    }

    /**
     * Creates a new experience entity.
     *
     * @Route("/experience/new", name="experiences_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $experience = new Experiences();
        $form = $this->createForm('OurguestBundle\Form\ExperiencesType', $experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $experience->getFeatimage();
            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('directory'),
                $fileName
            );
            $experience->setFeatimage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($experience);
            $em->flush();

            return $this->redirectToRoute('experiences_show', array('id' => $experience->getId()));
        }

        return $this->render('ourguest/experiences/backend/new.html.twig', array(
            'experience' => $experience,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a experience entity.
     *
     * @Route("/experience/{id}", name="experiences_show")
     * @Method("GET")
     */
    public function showAction(Experiences $experience)
    {
        $deleteForm = $this->createDeleteForm($experience);

        return $this->render('ourguest/experiences/backend/show.html.twig', array(
            'experience' => $experience,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing experience entity.
     *
     * @Route("/experience/{id}/edit", name="experiences_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Experiences $experience)
    {
        $fileName=$experience->getFeatimage();
        $deleteForm = $this->createDeleteForm($experience);
        $experience->setFeatimage(
            new File($this->getParameter('directory').'/'.$experience->getFeatimage())
        );
        $editForm = $this->createForm('OurguestBundle\Form\ExperiencesType', $experience);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $file = $experience->getFeatimage();
            if ($file)
            {
                $file_path='images/uploads/'.$fileName;
                unlink($file_path);
                $fileName1 = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('directory'),
                    $fileName1
                );
                $experience->setFeatimage($fileName1);

            }
            else
            {
                $experience->setFeatimage($fileName);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($experience);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Property updated successfully !');
            return $this->redirectToRoute('experiences_index');
        }

        return $this->render('ourguest/experiences/backend/edit.html.twig', array(
            'experience' => $experience,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a experience entity.
     *
     * @Route("/experience/{id}", name="experiences_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Experiences $experience)
    {
        $fileName=$experience->getFeatimage();
        $form = $this->createDeleteForm($experience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file_path='images/uploads/'.$fileName;
            unlink($file_path);
            $em = $this->getDoctrine()->getManager();
            $em->remove($experience);
            $em->flush();
        }

        return $this->redirectToRoute('experiences_index');
    }

    /**
     * Creates a form to delete a experience entity.
     *
     * @param Experiences $experience The experience entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Experiences $experience)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('experiences_delete', array('id' => $experience->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }


    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
