<?php
/**
 * Created by PhpStorm.
 * User: Demi
 * Date: 26-Feb-18
 * Time: 3:47 PM
 */
namespace AppBundle\Controller;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{

    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $users = $em->getRepository('AppBundle:User')->findAll();
        }else
        {
            $users = $em->getRepository('AppBundle:User')->findUserById($this->getUser()->getId());
        }
        return $this->render('user/index.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page :( you are not admin');
        $user = new User();
        $form = $this->createForm('AppBundle\Form\RegistrationType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            $password = $this->get('security.password_encoder')
//                ->encodePassword($user, $user->getPassword());
//            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $role = $form->get('roles')->getData();
            $user->setRoles(array($role));
            $em->persist($user);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'New user created successfully !');
            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user, $id)
    {
//      $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page :( you are not admin');
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
//        $editForm->remove('password');
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $role = $editForm->get('roles')->getData();
//            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
//            $user->setPassword($password);
            $user->setRoles(array($role));
            $userManager->updateUser($user);
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'User details updated successfully !');
            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('user/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }
        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }


    /**
     * Creates a new user entity.
     *
     * @Route("/{id}/change-password", name="change-password")
     * @Method({"GET", "POST"})
     */
    public function changePwdAction(Request $request,User $user, $id)
    {
        $userdetialid = $user->getUserDetails()->getId();
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page :( you are not admin');
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id'=> $id));
        $form = $this->createFormBuilder()
            ->add('pwd', TextType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $pwd = $form->get('pwd')->getData();
            $user->setPlainPassword($pwd);
            $userManager->updateUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Password changed successfully !');
        }

        return $this->render('user/change-password.html.twig', array(
            'form' => $form->createView(),
            'userdetailsid'=>$userdetialid
        ));
    }
}