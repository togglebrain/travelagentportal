<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DiscountDefRates;
use AppBundle\Entity\PropertyDiscount;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PropertyDiscountController
 * @package AppBundle\Controller
 * @Route("property_discount")
 */
class PropertyDiscountController extends Controller{

    /**
     * Creates a new property discount entity
     *
     * @Route("/new", name="property_discount_new")
     * @Method({"GET","POST"})
     */
    public function newPropertyDiscountAction(Request $request)
    {
        $discount = new PropertyDiscount();
        $form = $this->createForm('AppBundle\Form\PropertyDiscountType', $discount);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($discount);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'New Property Discount created successfully!');
            return $this->redirectToRoute('discount_index');
        }

        return $this->render('property-discount/new.html.twig', array(
            'discount' => $discount,
            'form' => $form->createView(),
        ));
    }

    /**
     *Displays a form to edit an existing Property Discount Entity
     *
     * @Route("/{id}/edit", name="property_discount_edit")
     * @Method({"GET","POST"})
     */
    public function editPropertyDiscountAction(Request $request, PropertyDiscount $propertyDiscount){

        $deleteForm = $this->createDeleteForm($propertyDiscount);
        $editForm = $this->createForm('AppBundle\Form\PropertyDiscountType', $propertyDiscount);
        $editForm->handleRequest($request);

        if($editForm->isSubmitted() && $editForm->isValid())
        {
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Property Discount updated succcessfully!');
            return $this->redirectToRoute('discount_index');
        }
        return $this->render('property-discount/edit.html.twig', array(
            'propertyDiscount' => $propertyDiscount,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Property Discount Entity
     *
     * @Route("/{id}", name="property_discount_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PropertyDiscount $discount)
    {
        $deleteForm = $this->createDeleteForm($discount);
        $deleteForm->handleRequest($request);

        if($deleteForm->isSubmitted() && $deleteForm->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($discount);
            $em->flush();
        }
        return $this->redirectToRoute('discount_index');
    }

    /**
     * Create a form to delete Property Discount Entity
     *
     * @param PropertyDiscount $discount The property discount Entity
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm(PropertyDiscount $discount)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('property_discount_delete', array('id' => $discount->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}