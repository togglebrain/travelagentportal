<?php

namespace OurguestBundle\Repository;

/**
 * ToursRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ToursRepository extends \Doctrine\ORM\EntityRepository
{

    public function findLastFeaturedTourImage(){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT t.featimage FROM tours t WHERE t.id=(SELECT MAX(id) FROM tours WHERE flag = 1)';

        $stmt = $conn->prepare($sql);

        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findTour($id){

        $qb = $this->createQueryBuilder('t')
            ->where('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();

        return $qb;
    }

    public function featuredTours(){
        $qb = $this->createQueryBuilder('p')
            ->where('p.isFeatured = :val')
            ->setParameter('val', true)
            ->getQuery()
            ->execute();

        return $qb;
    }

    public function findCustomisableToursByState($id){
        $qb = $this->createQueryBuilder('t')
            ->leftJoin('t.state', 's')
            ->where('s.id = :id')
            ->andWhere('t.tourType = :val')
            ->setParameter('id', $id)
            ->setParameter('val', 0)
            ->setMaxResults(6)
            ->getQuery()
            ->execute();

        return $qb;
    }

    public function findFixedDepartures(){
        $qb = $this->createQueryBuilder('t')
            ->where('t.tourType = :val')
            ->setParameter('val', 1)
            ->setMaxResults(6)
            ->getQuery()
            ->execute();

        return $qb;
    }

    public function findMoreCustomisableTours($id, $toursCount) {

        $id = substr($id, 6); //slicing 'state_' from $id
        $qb = $this->createQueryBuilder('t')
            ->where('t.state = :id')
            ->andWhere('t.tourType = :val')
            ->setParameter('id', $id)
            ->setParameter('val', 0)
            ->setFirstResult($toursCount)
            ->setMaxResults(6)
            ->getQuery()
            ->execute();

        return $qb;
    }

    public function findMoreFixedDepartures($toursCount){

        $qb = $this->createQueryBuilder('t')
            ->where('t.tourType = :val')
            ->setParameter('val', 1)
            ->setFirstResult($toursCount)
            ->setMaxResults(6)
            ->getQuery()
            ->execute();

        return $qb;
    }

    public function findSimilarTours($state_id, $tourType){

        if ($tourType == 0){
            $qb = $this->createQueryBuilder('t')
                ->leftJoin('t.state', 's')
                ->where('s.id = :state_id')
                ->andWhere('t.tourType = :tourType')
                ->setParameter('state_id', $state_id)
                ->setParameter('tourType', $tourType)
                ->getQuery()
                ->execute();

            return $qb;
        }
        else{
            $qb = $this->createQueryBuilder('t')
                ->leftJoin('t.state', 's')
                ->where('t.tourType = :tourType')
                ->setParameter('tourType', $tourType)
                ->getQuery()
                ->execute();

            return $qb;
        }
    }

    public function getAllTours(){

        $qb = $this->createQueryBuilder('t')
            ->orderBy('t.id', 'DESC');

        return $qb;
    }

    public function findUpcomingFixedDeparture(){

        $qb = $this->createQueryBuilder('t')
            ->orderBy('t.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->execute();

        return $qb;
    }
}
