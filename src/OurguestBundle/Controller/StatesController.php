<?php

namespace OurguestBundle\Controller;

use OurguestBundle\Entity\States;
use AppBundle\Entity\Property;
use AppBundle\Entity\Rooms;
use OurguestBundle\Entity\Experiences;
use OurguestBundle\Entity\Tours;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class SatesController
 * @Route("states")
 */
class StatesController extends Controller{

    /**
     * Lists all our States Entities
     * @Route("/", name="states_index")
     * @Method("GET")
     */
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();

        $states = $em->getRepository('OurguestBundle:States')->findAll();

        return $this->render('ourguest/states/index.html.twig', array(
            'states' => $states,
        ));
    }

    /**
     * Creates a new States Entity
     *
     * @Route("/new", name="states_new")
     * @Method({"GET","POST"})
     */
    public function newAction(Request $request){

        $states = new States();

        $form = $this->createForm('OurguestBundle\Form\StatesType', $states);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($states);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'New State added successfully!');

            return $this->redirectToRoute('states_index');
        }

        return $this->render('ourguest/states/new.html.twig', array(
            'form' => $form->createView(),
            'states' => $states,
        ));
    }

    /**
     * Displays a form to edit existing States Entity
     *
     * @Route("/{id}/edit", name="states_edit")
     * @Method({"GET","POST"})
     */
    public function editAction(Request $request, States $states){

        $deleteForm = $this->createDeleteForm($states);
        $editForm = $this->createForm('OurguestBundle\Form\StatesType', $states);
        $editForm->handleRequest($request);

        if($editForm->isSubmitted() && $editForm->isValid()){
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'State updated successfully!');
            return $this->redirectToRoute('states_index');
        }

        return $this->render('ourguest/states/edit.html.twig', array(
            'states' => $states,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a States Entity
     *
     * @Route("/{id}", name="states_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, States $states){
        $deleteForm = $this->createDeleteForm($states);
        $deleteForm->handleRequest($request);

        if($deleteForm->isSubmitted() && $deleteForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($states);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'State deleted successfully!');
        }
        return $this->redirectToRoute('states_index');
    }

    public function createDeleteForm(States $states){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('states_delete', array('id' => $states->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    //function to retrieve tours
    public function toursRetrieveAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        $tours = $em->getRepository('OurguestBundle:Tours')->findCustomisableToursByState($id);
        $detail = array();

        foreach ($tours as $tour) {
            if ($tour instanceof Tours){
                $detail[] = array(
                    'id' => $tour->getId(),
                    'name' => $tour->getName(),
                    'featimg' => $tour->getFeatimage(),
                    'rate' => $tour->getRate(),
                    'duration' => $tour->getNoOfDays(),
                    'slug' => $tour->getSlug(),
                );
            }
        }
        return $this->render('ourguest/tours/frontend/tab-content.html.twig', array(
            'detail' => $detail,
        ));
    }

    //function to retrieve homestays
    public function homestaysRetrieveAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        $homestays = $em->getRepository('AppBundle:Property')->findPropertiesByState($id);

        return $this->retrievePropertiesInfo($homestays, $id);
    }

    public function retrievePropertiesInfo($homestays, $state_id){

        $detail = array();

        foreach ($homestays as $value){
            if($value instanceof Property){
                $rooms = $value->getRooms();
                $rate = $this->getCheapestRoom($rooms);
                $detail[] = array(
                    'stateId' => $state_id,
                    'id' => $value->getId(),
                    'name' => $value->getName(),
                    'location' => $value->getLocation()->getName(),
                    'featimg' => $value->getFeatimage(),
                    'rate' => $rate,
                    'slug' => $value->getSlug(),
                );
            }
        }

        return $this->render('ourguest/homestays/tab-content.html.twig', array(
            'detail' => $detail,
            'stateId' => $state_id,
        ));
    }

    public function getCheapestRoom($rooms){
        $min_rate = 10000;
        foreach ($rooms as $room){
            if ($room instanceof Rooms){
                $rate = $room->getRate() + $room->getCpCost();
                if ($min_rate > $rate)
                    $min_rate = $rate;
            }
        }

        return $min_rate;
    }

    //function to retrieve experiences
    public function experiencesRetrieveAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        $experiences = $em->getRepository('OurguestBundle:Experiences')->findExperiencesByState($id);
        $detail = array();

        foreach ($experiences as $experience){
            if ($experience instanceof Experiences){
                $detail[] = array(
                    'id' => $experience->getId(),
                    'name' => $experience->getName(),
                    'featimg' => $experience->getFeatimage(),
                    'colNum' => 4,
                    'slug' => $experience->getSlug(),
                    'route' => 'homestay_template'
                );
            }
        }
        return $this->render('ourguest/experiences/frontend/tab-content.html.twig', array(
            'detail' => $detail,
        ));
    }
}