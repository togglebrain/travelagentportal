<?php

//default email where all tour enquiries are sent
define('TOUR_EMAIL', 'ourgueststay@gmail.com');

//default email where all homestay enquiries are sent
define('HOMESTAY_EMAIL', 'info@ourguest.in');

//default email where all customise tour enquiries are sent
define('CUSTOMISE_TOUR_EMAIL', 'ourgueststay@gmail.com');

//default email where all experiences enquiries are sent
define('EXPERIENCES_EMAIL', 'tsemibhutia@gmail.com');

//default email where all contact form enquiries are sent
define('CONTACT_EMAIL', 'contact@ourguest.in');

//default email where all newsletter signups are sent
define('NEWSLETTER_EMAIL', 'contact@ourguest.in');

