<?php

namespace OurguestBundle\Controller;

include_once ('GlobalConstants.php');

use AppBundle\Entity\Images;
use OurguestBundle\Entity\bookingsExperience;
use OurguestBundle\Entity\Experiences;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Experience Controller
 * @package OurguestBundle/Controller
 * @Route("experiences")
 */
class ExperiencesController extends Controller{

    //function to render experiences tab content
    public function tabContentExperiencesAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        $experiences = $em->getRepository('OurguestBundle:Experiences')->findExperiencesByState($id);

        return $this->retrieveExperiencesInfo($experiences);
    }

    /**
     * @Route("/load-more-experiences/", name="load_more_experiences", options={"expose"=true})
     */
    public function loadMoreExperiences(Request $request){
        $em = $this->getDoctrine()->getManager();

        $id = $request->request->get('id');
        $expCount = $request->request->get('itemCount');

        $experiences = $em->getRepository('OurguestBundle:Experiences')->findMoreExperiences($id, $expCount);

        return $this->retrieveExperiencesInfo($experiences);
    }

    /**
     * @Route("/experience-popup/{id}", name="experience_popup", options={"expose"=true})
     */
    public function popupAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $experiencesRepository = $em->getRepository('OurguestBundle:Experiences');

        $responseArray = array();
        $imagesArray = array();

        $experience = $experiencesRepository->createQueryBuilder('e')
            ->where('e.id = :id')
            ->setParameter('id', $request->attributes->get('id'))
            ->getQuery()
            ->execute();

        $images = $em->getRepository('AppBundle:Images')->findByExperience($request->attributes->get('id'));
        foreach ($images as $image){
            if ($image instanceof Images){
                $imagesArray[] = array(
                    'path' => $image->getPath(),
                    'altText' => $image->getAltText(),
                );
            }
        }

        foreach ($experience as $value){
            if ($value instanceof Experiences){
                $responseArray[] = array(
                    'name' => $value->getName(),
                    'location' => $value->getLocation()->getName(),
                    'description' => $value->getDescription(),
                    'featimg' => $value->getFeatimage(),
                    'altText' => $value->getAltText(),
                    'images' => $imagesArray,
                );
            }
        }

        return new JsonResponse($responseArray);
    }

    public function retrieveExperiencesInfo($experiences){

        $detail = array();

        foreach ($experiences as $experience){
            if ($experience instanceof Experiences){
                $detail[] = array(
                    'id' => $experience->getId(),
                    'name' => $experience->getName(),
                    'featimg' => $experience->getFeatimage(),
                );
            }
        }

        return $this->render('ourguest/experiences/frontend/tab-content.html.twig', array(
            'detail' => $detail,
        ));
    }

    /**
     * @Route("experiences-booking", name="experiences_booking", options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function experiencesBookingAction(Request $request){

        $booking = new bookingsExperience();

        $form_experiences = $this->createForm('OurguestBundle\Form\bookingsExperienceType', $booking);
        $form_experiences->handleRequest($request);

        if ($form_experiences->isSubmitted() && $form_experiences->isValid()){

            $data = $form_experiences->getData();

            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                ->setUsername('ourgueststay@gmail.com')
                ->setPassword('lvsgpevkmwfhozxz');

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance();

            $message->setSubject('New Experiences Enquiry')
                ->setFrom(array("ourgueststay@gmail.com"=>"OurGuest"))
                ->setTo(array(EXPERIENCES_EMAIL=>'Email Owner'))
                ->setBody($this->renderView('ourguest/experiences/frontend/email-template.html.twig',array('data' => $data)), 'text/html');

            $mailer->send($message);

            $em = $this->getDoctrine()->getManager();
            $em->persist($booking);
            $em->flush();
        }

        return $this->render(':ourguest/experiences/frontend:booking-form.html.twig', array(
            'form_experience' => $form_experiences->createView(),
        ));
    }
}