<?php

namespace OurguestBundle\Controller;

include_once ('GlobalConstants.php');

use AppBundle\Entity\Images;
use AppBundle\Entity\Location;
use AppBundle\Entity\Property;
use AppBundle\Entity\Rooms;
use OurguestBundle\Entity\bookingsHomestay;
use OurguestBundle\Entity\Reviews;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OurguestBundle\Entity\Experiences;


/**
 * Class HomestayController
 * @package OurguestBundle\Controller
 * @Route("homestays")
 */
class HomestayController extends Controller{

    /**
     * @Route("/state-properties/{id}", name="state_properties", options={"expose"=true})
     */
    public function tabContentStateAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $id = $request->attributes->get('id');
        $homestays = $em->getRepository('AppBundle:Property')->findPropertiesByState($id);

        return $this->retrievePropertiesInfo($homestays);
    }

    /**
     * @Route("/location-properties/{id}", name="location_properties", options={"expose"=true})
     */
    public function tabContentLocationAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $id = $request->attributes->get('id');
        $homestays = $em->getRepository('AppBundle:Property')->findPropertiesByLocation($id);

        return $this->retrievePropertiesInfo($homestays);
    }

    /**
     * @Route("/location-listing/{id}", name="location_listing", options={"expose"=true})
     */
    public function retrieveLocationsAction(Request $request){
        $id = $request->attributes->get('id');
        $em = $this->getDoctrine()->getManager();

        $locations = $em->getRepository('AppBundle:Location')->findLocationByState($id);

        $response = array();
        foreach ($locations as $location){
            if ($location instanceof Location){
                $response[] = array(
                    'id' => $location->getId(),
                    'name' => $location->getName(),
                );
            }
        }
        return $this->render('ourguest/homestays/location-listing.html.twig', array(
            'response' => $response,
            'stateId' => $id,
        ));
    }

    /**
     * @Route("/load-more-homestays/", name="load_more_homestays", options={"expose"=true})
     */
    public function loadMoreHomestaysAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        $propCount = $request->request->get('itemCount');
        $homestays = $em->getRepository('AppBundle:Property')->findMoreProperties($id, $propCount);

        return $this->retrievePropertiesInfo($homestays);
    }

    /**
     * @Route("/{slug}/", name="homestay_template", options={"expose"=true})
     * @ParamConverter("homestay", options={"mapping": {"slug": "slug"}})
     */
    public function singlePageAction(Request $request, Property $homestay){

        $em = $this->getDoctrine()->getManager();

        $id = $homestay->getId();
        $homestayName = $homestay->getName();

        $booking = new bookingsHomestay();
        $form = $this->createForm('OurguestBundle\Form\bookingsHomestayType', $booking, array(
            'homestayId' => $id,
        ));
        $form->handleRequest($request);

        $review = new Reviews();
        $review_form = $this->createForm('OurguestBundle\Form\ReviewsType', $review);
        $review_form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $data = $form->getData();
            //            dump($form_customise['tourDestination']->getData());die;
            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                ->setUsername('ourgueststay@gmail.com')
                ->setPassword('lvsgpevkmwfhozxz');

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance();
//                $header = $message->embed(\Swift_Image::fromPath('CAT2/images/header.jpg'));
            $message->setSubject('New ' . $homestayName . ' Enquiry')
                ->setFrom(array("ourguesttravel@gmail.com" => "OurGuest"))
                ->setTo(array(HOMESTAY_EMAIL => 'Email Owner'))
                ->setBody($this->renderView('ourguest/homestays/email-template.html.twig',array('data' => $data)), 'text/html');

            $mailer->send($message);

            $em->persist($booking);
            $em->flush();
        }

        if ($review_form->isSubmitted() && $review_form->isValid()){
            $review->setProperty($em->getRepository('AppBundle:Property')->find($id));
            $em->persist($review);
            $em->flush();

            return $this->redirectToRoute('homestay_template', array('slug' => $request->attributes->get('slug')));
        }

        $rooms_array = array();

        $rooms = $em->getRepository('AppBundle:Rooms')->findByProperty($id);

        $min_rate = 1000000;
        foreach ($rooms as $room) {
            if ($room instanceof Rooms){
                $images_array = array();
                $images = $room->getImage();
                $rate = $room->getRate() + $room->getCpCost();
                if ($min_rate > $rate)
                    $min_rate = $rate;

                foreach ($images as $image){
                    if ($image instanceof Images){
                        $images_array[] = array(
                            'path' => $image->getPath(),
                            'altText' => $image->getAltText(),
                        );
                    }
                }
                $rooms_array[] = array(
                    'type' => $room->getType(),
                    'images' => $images_array,
                    'rate' => $rate,
                    'featimage' => $room->getImageurl(),
                );
            }
        }
        $displayRate = $min_rate;

        return $this->render('ourguest/homestays/single-page.html.twig', array(
            'homestay' => $homestay,
            'page_title' => $homestay->getName(). ' | ' .$homestay->getLocation()->getName(),
            'rooms' => $rooms_array,
            'minRate' => $displayRate,
            'form_homestay' => $form->createView(),
            'form_review' => $review_form->createView(),
        ));
    }

    public function similarHomestaysAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $state_id = $request->attributes->get('stateId');
        $id = $request->attributes->get('id');

        $homestays = $em->getRepository('AppBundle:Property')->findPropertiesByState($state_id);
        shuffle($homestays);
        $detail = array();
        $i = 1;

        foreach ($homestays as $value){
            if($value instanceof Property and $value->getId() != $id){
                $rooms = $value->getRooms();
                $rate = $this->getCheapestRoom($rooms);
                $detail[] = array(
                    'stateId' => $state_id,
                    'id' => $value->getId(),
                    'slug' => $value->getSlug(),
                    'name' => $value->getName(),
                    'location' => $value->getLocation()->getName(),
                    'featimg' => $value->getFeatimage(),
                    'rate' => $rate,
                );
                if ($i++ == 3) break;
            }
        }

        return $this->render('ourguest/homestays/tab-content.html.twig', array(
            'detail' => $detail,
        ));
    }

    public function experiencesForHomestayAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $id = $request->attributes->get('id');
        $experiences = $em->getRepository('OurguestBundle:Experiences')->findByProperty($id);

        return $this->retrieveExperiencesInfo($experiences);
    }

    public function retrievePropertiesInfo($homestays){

        $detail = array();
        $state_id=0;

        foreach ($homestays as $value){
            if($value instanceof Property){
                $state_id = $value->getLocation()->getState()->getId();
                $rooms = $value->getRooms();
                $rate = $this->getCheapestRoom($rooms);
                $detail[] = array(
                    'stateId' => $state_id,
                    'id' => $value->getId(),
                    'name' => $value->getName(),
                    'slug' => $value->getSlug(),
                    'location' => $value->getLocation()->getName(),
                    'featimg' => $value->getFeatimage(),
                    'rate' => $rate,
                );
            }
        }

        return $this->render('ourguest/homestays/tab-content.html.twig', array(
            'detail' => $detail,
            'stateId' => $state_id,
        ));
    }

    public function getCheapestRoom($rooms){
        $min_rate = 10000;
        foreach ($rooms as $room){
            if ($room instanceof Rooms){
                $rate = $room->getRate() + $room->getCpCost();
                if ($min_rate > $rate)
                    $min_rate = $rate;
            }
        }
        return $min_rate;
    }

    public function retrieveExperiencesInfo($experiences){

        $detail = array();

        foreach ($experiences as $experience){
            if ($experience instanceof Experiences){
                $detail[] = array(
                    'id' => $experience->getId(),
                    'name' => $experience->getName(),
                    'featimg' => $experience->getFeatimage(),
                    'colNum' => 6,

                );
            }
        }

        return $this->render('ourguest/default/multi-item-carousel.html.twig', array(
            'detail' => $detail,
        ));
    }
}