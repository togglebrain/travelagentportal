<?php

namespace OurguestBundle\Controller;

use OurguestBundle\Entity\Itinerary;
use OurguestBundle\Entity\Tours;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Itinerary Controller
 * @Route("itinerary")
 */
class ItineraryController extends Controller{

    /**
     * List all Itineraries
     *
     * @Route("/", name="itinerary_index")
     * @Method("GET")
     */
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();

        $itineraries = $em->getRepository('OurguestBundle:Itinerary')->findAll();
        return $this->render('ourguest/itinerary/index.html.twig', array('itineraries' => $itineraries));
    }

    /**
     * New Itinerary Creation
     *
     * @Route("/new", name="itinerary_new")
     * @Method({"GET","POST"})
     */
    public function newAction(Request $request){

        $itinerary = new Itinerary();
        $form = $this->createForm('OurguestBundle\Form\ItineraryType', $itinerary);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($itinerary);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('sucess', 'Itinerary added successfully!');
        }

        return $this->render('ourguest/itinerary/new.html.twig', array(
            'form' => $form->createView(),
            'itinerary' => $itinerary,
        ));
    }

    /**
     * Edit Itinerary
     *
     * @Route("/{id}/edit", name="itinerary_edit")
     * @Method({"GET","POST"})
     */
    public function editAction(Request $request, Itinerary $itinerary){
        $deleteForm = $this->createDeleteForm($itinerary);
        $editForm = $this->createForm('OurguestBundle\Form\ItineraryType', $itinerary);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($itinerary);
            $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Itinerary edited successfully!');

            return $this->redirectToRoute('itinerary_index');
        }

        return $this->render('ourguest/itinerary/edit.html.twig', array(
            'itinerary' => $itinerary,
            'edit_form' => $editForm,
            'delete_form' => $deleteForm,
        ));
    }

    /**
     * Deletes an Itinerary Entity
     *
     * @Route("/{id}", name="rooms_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Itinerary $itinerary){
        $form = $this->createDeleteForm($itinerary);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($itinerary);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Itinerary removed successfully!');
        }

        return $this->redirectToRoute('itinerary_index');
    }

    /**
     * Creates a form to delete an itinerary object
     *
     * @param Itinerary $itinerary
     * @return \Symfony\Component\Form\Form
     */
    public function createDeleteForm(Itinerary $itinerary){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('itinerary_delete', array('id' => $itinerary->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}