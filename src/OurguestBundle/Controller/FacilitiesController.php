<?php

namespace OurguestBundle\Controller;

use OurguestBundle\Entity\Facilities;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Facility controller.
 *
 * @Route("admin")
 */
class FacilitiesController extends Controller
{
    /**
     * Lists all facility entities.
     *
     * @Route("/facilities", name="facilities_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $facilities = $em->getRepository('OurguestBundle:Facilities')->findAll();

        return $this->render('ourguest/facilities/index.html.twig', array(
            'facilities' => $facilities,
        ));
    }

    /**
     * Creates a new facility entity.
     *
     * @Route("/facilities/new", name="facilities_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $facility = new Facilities();
        $form = $this->createForm('OurguestBundle\Form\FacilitiesType', $facility);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($facility);
            $em->flush();

            return $this->redirectToRoute('facilities_index');
        }

        return $this->render('ourguest/facilities/new.html.twig', array(
            'facility' => $facility,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a facility entity.
     *
     * @Route("/facilities/{id}", name="facilities_show")
     * @Method("GET")
     */
    public function showAction(Facilities $facility)
    {
        $deleteForm = $this->createDeleteForm($facility);

        return $this->render('ourguest/facilities/show.html.twig', array(
            'facility' => $facility,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing facility entity.
     *
     * @Route("/facilities/{id}/edit", name="facilities_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Facilities $facility)
    {
        $deleteForm = $this->createDeleteForm($facility);
        $editForm = $this->createForm('OurguestBundle\Form\FacilitiesType', $facility);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('facilities_index');
        }

        return $this->render('ourguest/facilities/edit.html.twig', array(
            'facility' => $facility,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a facility entity.
     *
     * @Route("/facilities/{id}", name="facilities_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Facilities $facility)
    {
        $form = $this->createDeleteForm($facility);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($facility);
            $em->flush();
        }

        return $this->redirectToRoute('facilities_index');
    }

    /**
     * Creates a form to delete a facility entity.
     *
     * @param Facilities $facility The facility entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Facilities $facility)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('facilities_delete', array('id' => $facility->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
