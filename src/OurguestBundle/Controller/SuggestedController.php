<?php

namespace OurguestBundle\Controller;

use OurguestBundle\Entity\Suggested;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SuggestedController
 * @package OurguestBundle\Controller
 * @Route("suggested")
 */
class SuggestedController extends Controller{

    /**
     * @Route("/", name="suggested_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $suggested = $em->getRepository('OurguestBundle:Suggested')->findAll();

        $suggestedArray = [];
        foreach ($suggested as $val){
            if ($val instanceof Suggested){
                $suggestedArray[] = array(
                    'tour' => $val->getTour(),
                    'homestay' => $val->getHomestay(),
                    'id' => $val->getId(),
                );
            }
        }

        return $this->render('ourguest/suggested/index.html.twig', array(
            'suggested' => $suggestedArray,
        ));
/*
        if ($settings){
            $form = $this->createForm('OurguestBundle\Form\SuggestedType', $settings);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()){

                $em->persist($settings);
                $em->flush();

                return $this->redirectToRoute('ourguest_settings');
            }
        }
        else{
            $settings = new Suggested();

            $form = $this->createForm('OurguestBundle\Form\SuggestedType', $settings);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()){

                $em->persist($settings);
                $em->flush();

                return $this->redirectToRoute('ourguest_settings');
            }
        }
        return $this->render('ourguest/suggested/index.html.twig', array(
            'form_suggested' => $form->createView(),
        ));*/
    }

    /**
     * @Route("/new", name="suggested_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request){

        $suggested = new Suggested();

        $form = $this->createForm('OurguestBundle\Form\SuggestedType', $suggested);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($suggested);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Recommendations added successfully');

            return $this->redirectToRoute('ourguest_settings');
        }

        return $this->render('ourguest/suggested/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", name="suggested_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Suggested $suggested){

        $form = $this->createForm('OurguestBundle\Form\SuggestedType', $suggested);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($suggested);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Recommendations updated successfully');

            return $this->redirectToRoute('ourguest_settings');
        }
        return $this->render('ourguest/suggested/edit.html.twig', array(
            'edit_form' => $form->createView(),
        ));
    }
}