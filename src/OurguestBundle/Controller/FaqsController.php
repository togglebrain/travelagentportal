<?php

namespace OurguestBundle\Controller;
use OurguestBundle\Entity\Faqs;
use OurguestBundle\Entity\Qa;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Faqs Controller
 * Class Faqs Controller
 * @package Ourguest\Controller
 * @Route("faqs")
 */
class FaqsController extends Controller{

    /**
     * List FAQs
     * @Route("/", name="faqs_index")
     * @Method("GET")
     */
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();

        $faqs = $em->getRepository('OurguestBundle:Faqs')->findAll();
        $faqs_id = 0;
        $qaData = array();
        foreach ($faqs as $val){
            if($val instanceof Faqs){
                $faqs_id = $val->getId();
                $qa = $val->getQuestionAnswer();
                foreach ($qa as $item){
                    if ($item instanceof Qa){
                        $qaData[] = array(
                            'id' => $item->getId(),
                            'question' => $item->getQuestion(),
                            'answer' => $item->getAnswer(),
                        );
                    }
                }
            }
        }

        return $this->render('ourguest/faqs/index.html.twig', array(
            'faqs_id' => $faqs_id,
            'qa' => $qaData,
        ));
    }

    /**
     * New FAQ
     * @Route("/new", name="faqs_new")
     * @Method({"GET","POST"})
     */
    public function newAction(Request $request){

        $faqs = new Faqs();
        $form = $this->createForm('OurguestBundle\Form\FaqsType', $faqs);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($faqs);
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', 'FAQs initialized successfully!');

            return $this->redirectToRoute('ourguest_settings');
        }

        return $this->render('ourguest/faqs/edit.html.twig', array(
            'form' => $form->createView(),
            'faqs' => $faqs,
        ));
    }

    /**
     * @Route("/{id}/edit", name="faqs_edit")
     * @Method({"GET","POST"})
     */
    public function editAction(Request $request, Faqs $faqs){

        $faqForm = $this->createForm('OurguestBundle\Form\FaqsType', $faqs);
        $faqForm->handleRequest($request);

        if ($faqForm->isSubmitted() && $faqForm->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->persist($faqs);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'FAQs updated successfully!');

            return $this->redirectToRoute('ourguest_settings');
        }

        return $this->render('ourguest/faqs/faq.html.twig', array(
            'faq_form' => $faqForm->createView(),
        ));
    }

    /**
     * @Route("/show", name="faqs_display")
     */
    public function displayFaqsAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        $faqs = $em->getRepository('OurguestBundle:Faqs')->findAll();

        $qaData = array();
        $faqs_id = 0;

        foreach ($faqs as $item){
            if ($item instanceof Faqs){
                $faqs_id = $item->getId();
                $qa = $item->getQuestionAnswer();

                foreach ($qa as $val){
                    if ($val instanceof Qa){
                        $qaData[] = array(
                            'id' => $val->getId(),
                            'question' => $val->getQuestion(),
                            'answer' => $val->getAnswer(),
                        );
                    }
                }
            }
        }
        return $this->render('ourguest/default/faqs.html.twig', array(
            'faqs_id' => $faqs_id,
            'qa' => $qaData,
        ));
    }
}

