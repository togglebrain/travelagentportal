<?php

namespace OurguestBundle\Controller;

use OurguestBundle\Entity\Host;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Host controller.
 *
 * @Route("host")
 */
class HostController extends Controller
{
    /**
     * Lists all host entities.
     *
     * @Route("/", name="host_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hosts = $em->getRepository('OurguestBundle:Host')->findAll();

        return $this->render('ourguest/host/index.html.twig', array(
            'hosts' => $hosts,
        ));
    }

    /**
     * Creates a new host entity.
     *
     * @Route("/new", name="host_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $host = new Host();
        $form = $this->createForm('OurguestBundle\Form\HostType', $host);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $host->getPhoto();
            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('directory'),
                $fileName
            );
            $host->setPhoto($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($host);
            $em->flush();

            return $this->redirectToRoute('host_index');
        }

        return $this->render('ourguest/host/new.html.twig', array(
            'host' => $host,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a host entity.
     *
     * @Route("/{id}", name="host_show")
     * @Method("GET")
     */
    public function showAction(Host $host)
    {
        $deleteForm = $this->createDeleteForm($host);

        return $this->render('ourguest/host/show.html.twig', array(
            'host' => $host,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing host entity.
     *
     * @Route("/{id}/edit", name="host_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Host $host)
    {
        $fileName=$host->getPhoto();
        $deleteForm = $this->createDeleteForm($host);
        $host->setPhoto(
            new File($this->getParameter('directory').'/'.$host->getPhoto())
        );
        $editForm = $this->createForm('OurguestBundle\Form\HostType', $host);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $file = $host->getPhoto();
            if ($file)
            {
                $file_path='images/uploads/'.$fileName;
                unlink($file_path);
                $fileName1 = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('directory'),
                    $fileName1
                );
                $host->setPhoto($fileName1);

            }
            else
            {
                $host->setPhoto($fileName);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($host);
            $em->flush();

            return $this->redirectToRoute('host_index');
        }

        return $this->render('ourguest/host/edit.html.twig', array(
            'host' => $host,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a host entity.
     *
     * @Route("/{id}", name="host_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Host $host)
    {
        $fileName=$host->getPhoto();
        $form = $this->createDeleteForm($host);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file_path='images/uploads/'.$fileName;
            unlink($file_path);
            $em = $this->getDoctrine()->getManager();
            $em->remove($host);
            $em->flush();
        }

        return $this->redirectToRoute('host_index');
    }

    /**
     * Creates a form to delete a host entity.
     *
     * @param Host $host The host entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Host $host)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('host_delete', array('id' => $host->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
