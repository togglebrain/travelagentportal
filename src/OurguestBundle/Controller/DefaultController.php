<?php

namespace OurguestBundle\Controller;

include_once ('GlobalConstants.php');

use AppBundle\Entity\Location;
use OurguestBundle\Entity\bookingsCustomise;
use OurguestBundle\Entity\Suggested;
use OurguestBundle\Entity\Contact;
use OurguestBundle\Entity\NewsletterSignup;
use OurguestBundle\Entity\States;
use OurguestBundle\Entity\Tours;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use function Composer\Autoload\includeFile;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="ourguest_homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $lastFeaturedTourImage = $em->getRepository('OurguestBundle:Tours')->findLastFeaturedTourImage();
        $lastFeaturedHomestayImage = $em->getRepository('AppBundle:Property')->findLastFeaturedHomestayImage();
        $lastFeaturedExperienceImage = $em->getRepository('OurguestBundle:Experiences')->findLastFeaturedExperienceImage();

        return $this->render('ourguest/default/index.html.twig', array(
            'feat_tour' => $lastFeaturedTourImage,
            'feat_homestay' => $lastFeaturedHomestayImage,
            'feat_experience' => $lastFeaturedExperienceImage,
        ));
    }

    /**
     * @Route("/blogs", name="blogs")
     */
    public function blogsAction(Request $request){

        return $this->render(':ourguest/default:blogs.html.twig');
    }

    /**
     * @Route("/contact-us", name="contact_og", options={"expose"=true})
     */
    public function contactPageAction(Request $request){

        $contact = new Contact();
        $contact_form = $this->createForm('OurguestBundle\Form\ContactType', $contact);
        $contact_form->handleRequest($request);

        if ($contact_form->isSubmitted() && $contact_form->isValid()){

            $data = $contact_form->getData();

//            dump($form_customise['tourDestination']->getData());die;
            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                ->setUsername('ourgueststay@gmail.com')
                ->setPassword('lvsgpevkmwfhozxz');

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance();
//                $header = $message->embed(\Swift_Image::fromPath('CAT2/images/header.jpg'));
            $message->setSubject('New Contact Request')
                ->setFrom(array("ourgueststay@gmail.com"=>"OurGuest"))
                ->setTo(array(CONTACT_EMAIL=>'Email Owner'))
                ->setBody($this->renderView('ourguest/default/contact-us-email-template.html.twig',array('data' => $data)), 'text/html');

            $mailer->send($message);

            $em = $this->getDoctrine()->getManager();

            $em->persist($contact);
            $em->flush();
        }

        return $this->render('ourguest/default/contact-us.html.twig', array(
            'contact_form' => $contact_form->createView(),
        ));
    }

    /**
     * @Route("/about-us", name="about_us")
     */
    public function aboutUsAction(){
        return $this->render('ourguest/default/about-us.html.twig');
    }

    /**
     * @Route("/travel-advisory", name="travel_advisory")
     */
    public function travelAdvisoryAction(){
        return $this->render('ourguest/default/travel-advisory.html.twig');
    }

    /**
     * @Route("/customisable-tours", name="customisable_tours_main")
     */
    public function customisableToursMainAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $states = $em->getRepository('OurguestBundle:States')->findAll();

        $bookings_customise = new bookingsCustomise();
        $form_customise = $this->createForm('OurguestBundle\Form\bookingsCustomiseType', $bookings_customise);
        $form_customise->handleRequest($request);
        if($form_customise->isSubmitted() && $form_customise->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($bookings_customise);
            $em->flush();

            return $this->redirectToRoute('booking_successful');
        }

        $detail = array();

        foreach ($states as $state){
            if ($state instanceof States){
                $id = $state->getId();
                $name = $state->getName();
                $detail[] = array(
                    'id' => $id,
                    'name' => $name,
                );
            }
        }

        return $this->render('ourguest/tours/frontend/customize-tour-main.html.twig', array(
            'detail' => $detail,
            'form_customise' => $form_customise->createView(),
        ));
    }

    /**
     * @Route("/fixed-departures", name="fixed_departures_main")
     */
    public function fixedDeparturesMainAction(Request $request)
    {

        $bookings_customise = new bookingsCustomise();
        $form_customise = $this->createForm('OurguestBundle\Form\bookingsCustomiseType', $bookings_customise);
        $form_customise->handleRequest($request);
        if($form_customise->isSubmitted() && $form_customise->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($bookings_customise);
            $em->flush();

            return $this->redirectToRoute('booking_successful');
        }

        return $this->render('ourguest/tours/frontend/fixed-departure-main.html.twig', array(
            'form_customise' => $form_customise->createView(),
        ));
    }

    /**
     * @Route("/experiences", name="experiences_main")
     */
    public function experiencesMainAction()
    {
        $em = $this->getDoctrine()->getManager();
        $states = $em->getRepository('OurguestBundle:States')->findAll();

        $detail = array();

        foreach ($states as $state){
            if ($state instanceof States){
                $detail[] = array(
                    'id' => $state->getId(),
                    'name' => $state->getName(),
                );
            }
        }

        return $this->render('ourguest/experiences/frontend/main.html.twig', array(
            'detail' => $detail,
        ));
    }

    /**
     * @Route("/homestays", name="homestays_main")
     */
    public function homestaysMainAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $states = $em->getRepository('OurguestBundle:States')->findAll();
        $bookings_customise = new bookingsCustomise();
        $form_customise = $this->createForm('OurguestBundle\Form\bookingsCustomiseType', $bookings_customise);
        $form_customise->handleRequest($request);

        if($form_customise->isSubmitted() && $form_customise->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($bookings_customise);
            $em->flush();

            return $this->redirectToRoute('booking_successful');
        }

        $detail = array();
        foreach ($states as $state){
            if ($state instanceof States){
                $detail[] = array(
                    'id' => $state->getId(),
                    'name' => $state->getName(),
                );
            }
        }

        $locations = array();
        $sikkimLocations = $em->getRepository('AppBundle:Location')->findLocationByState(1);
        foreach ($sikkimLocations as $location){
            if ($location instanceof Location){
                $locations[] = array(
                    'id' => $location->getId(),
                    'name' => $location->getName(),
                );
            }
        }

        return $this->render('ourguest/homestays/main.html.twig', array(
            'detail' => $detail,
            'sikkim_locations' => $locations,
            'form_customise' => $form_customise->createView(),
        ));
    }

    /**
     * @Route("/booking-successful", name="booking_successful")
     */
    public function thankYouAction(Request $request){
        return $this->render('ourguest/default/booking-successful.html.twig');
    }

    //function to retrieve featured products from db
    public function featuredToursAction(){
        $em = $this->getDoctrine()->getManager();
        $detail = array();
        $view = $em->getRepository('OurguestBundle:Tours')->featuredTours();
        foreach ($view as $value){
            if($value instanceof Tours){
                $detail[] = array(
                    'id' => $value->getId(),
                    'title' => $value->getName(),
                    'featImage' => $value->getFeatimage(),
                    'state' => $value->getState(),
                    'duration' => $value->getNoOfDays(),
                    'slug' => $value->getSlug(),
                );
            }
        }
        return $this->render(':ourguest/default:featured-products.html.twig', array(
            'detail' => $detail,
        ));
    }

    //controller function to render the location navbar
    public function locationNavbarAction(){

        $em = $this->getDoctrine()->getManager();
        $detail = array();
        $locations_array = array();
        $states = $em->getRepository('OurguestBundle:States')->findAll();

        foreach ($states as $state){
            if($state instanceof States){
                $detail[] = array(
                    'id' => $state->getId(),
                    'slug' => $state->getSlug(),
                    'name' => $state->getName(),
                    'locations' => $locations_array,
                );
            }
        }



        return $this->render('ourguest/default/location-navbar.html.twig', array(
            'detail' => $detail,
        ));
    }

    //retrieving the destinations/states for main navigation bar
    public function navigationBarAction(){
        $em = $this->getDoctrine()->getManager();
        $detail = array();
        $list = $em->getRepository('OurguestBundle:States')->findAll();

        foreach ($list as $value){
            if($value instanceof States){
                $detail[] = array(
                    'id' => $value->getId(),
                    'slug' => $value->getSlug(),
                    'name' => $value->getName(),
                );
            }
        }
        return $this->render('ourguest/default/main-navbar.html.twig', array(
            'detail' => $detail,
        ));
    }

    /**
     * @Route("ourguest/{slug}/", name="location_page_main")
     * @ParamConverter("state", options={"mapping": {"slug": "slug"}})
     * @Method("GET")
     */
    public function locationPageMainAction(Request $request, States $state)
    {
        $detail = array();
        if ($state instanceof States){
            $detail[] = array(
                'id' => 'state_'.$state->getId(),
                'name' => $state->getName(),
            );
        }

        return $this->render('ourguest/location-page/location.html.twig', array(
            'detail' => $detail,
            'page_title' => $state->getName(). ' | OurGuest',
        ));
    }

    /**
     * @Route("/customize-tour-form", name="customize_tour_form",options={"expose"= true})
     * @Method({"GET", "POST"})
     */
    public function customizeTourFormAction(Request $request)
    {
        $customiseTour = new bookingsCustomise();

        $form_customise = $this->createForm('OurguestBundle\Form\bookingsCustomiseType', $customiseTour);
        $form_customise->handleRequest($request);
        if($form_customise->isSubmitted() && $form_customise->isValid()){

            $data = $form_customise->getData();

//            dump($form_customise['tourDestination']->getData());die;
            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                ->setUsername('ourgueststay@gmail.com')
                ->setPassword('lvsgpevkmwfhozxz');

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance();
//                $header = $message->embed(\Swift_Image::fromPath('CAT2/images/header.jpg'));
            $message->setSubject('Customise My Tour Enquiry')
                ->setFrom(array("ourgueststay@gmail.com"=>"OurGuest"))
                ->setTo(array(CUSTOMISE_TOUR_EMAIL=>'Email Owner'))
                ->setBody($this->renderView('ourguest/default/customize-form-email-template.html.twig',array('data' => $data)), 'text/html');

            $mailer->send($message);

            $em = $this->getDoctrine()->getManager();

            $em->persist($customiseTour);
            $em->flush();
        }

        return $this->render('ourguest/default/customise-form.html.twig', array(
            'form_customise' => $form_customise->createView(),
        ));
    }

    /**
     * @Route("/newsletter_signup", name="newsletter-signup", options={"expose"=true})
     * @Method("POST")
     */
    public function newsletterSignupAction(Request $request){

        $signup = new NewsletterSignup();
        $newsletterForm = $this->createForm('OurguestBundle\Form\NewsletterSignupType', $signup);
        $newsletterForm->handleRequest($request);

        if ($newsletterForm->isSubmitted() && $newsletterForm->isValid()){

            $data = $newsletterForm->getData();

//            dump($form_customise['tourDestination']->getData());die;
            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
                ->setUsername('ourgueststay@gmail.com')
                ->setPassword('lvsgpevkmwfhozxz');

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance();
//                $header = $message->embed(\Swift_Image::fromPath('CAT2/images/header.jpg'));
            $message->setSubject('New Newsletter Signup')
                ->setFrom(array("ourgueststay@gmail.com"=>"OurGuest"))
                ->setTo(array(NEWSLETTER_EMAIL=>'Email Owner'))
                ->setBody($this->renderView('ourguest/default/newsletter-signup-email-template.html.twig',array('data' => $data)), 'text/html');

            $mailer->send($message);

            $em = $this->getDoctrine()->getManager();

            $em->persist($signup);
            $em->flush();
        }

        return $this->render('ourguest/default/newsletter-signup-form.html.twig', array(
            'form_newsletter' => $newsletterForm->createView(),
        ));
    }

    /**
     * @Route("/suggesstions", name="suggesstions")
     */
    public function suggestedAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        $upcomingFixedDeparture = $em->getRepository('OurguestBundle:Tours')->findUpcomingFixedDeparture();
        $suggested = $em->getRepository('OurguestBundle:Suggested')->findAll();
        $suggestedArray = [];
        foreach ($suggested as $val){
            if ($val instanceof Suggested){
                $suggestedArray[] = array(
                    'tour' => $val->getTour(),
                    'homestay' => $val->getHomestay(),
                    'id' => $val->getId(),
                );
            }
        }

        return $this->render('ourguest/default/suggestions-pane.html.twig', array(
            'upcoming_fd' => $upcomingFixedDeparture,
            'suggested' => $suggestedArray,
        ));
    }
}
