<?php

namespace OurguestBundle\Controller;

use OurguestBundle\Entity\Reviews;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ReviewsController
 * @package OurguestBundle\Controller
 * @Route("reviews")
 */
class ReviewsController extends Controller {
    /**
     * @Route("/index", name="review_index")
     * @Method("GET")
     */
    public function indexAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $reviews = $em->getRepository('OurguestBundle:Reviews')->findAll();

        return $this->render('ourguest/reviews/index.html.twig', array(
            'reviews' => $reviews,
        ));
    }

    /**
     * @Route("/{id}/edit", name="review_edit")
     * @Method({"GET","POST"})
     */
    public function editAction(Request $request, Reviews $review){

        $editForm = $this->createForm('OurguestBundle\Form\ReviewsType', $review);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->persist($review);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Review approved!');

            return $this->redirectToRoute('review_index');
        }

        return $this->render('ourguest/reviews/edit.html.twig', array(
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * @Route("/update-review-status/", name="update_review_status", options={"expose"=true})
     * @Method("POST")
     */
    public function hideReviewAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('reviewId');
        $status = $request->request->get('status');
        $response = array();

        $review = $em->getRepository('OurguestBundle:Reviews')->find($id);

        $form = $this->createForm('OurguestBundle\Form\ReviewsType', $review);

        if ($request->isMethod('POST')){
            $form->handleRequest($request);

            if ($request->isXmlHttpRequest()){
                $response['code'] = 'OK';
                $review->setStatus($status);
                $em->persist($review);
                $em->flush();
            }
            else{
                $response['code'] = 'ERR';
            }
        }
        if ($request->isXmlHttpRequest() == true){
            $response = new JsonResponse($response);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        return $this->redirectToRoute('review_index');
    }
}