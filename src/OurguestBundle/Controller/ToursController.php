<?php

namespace OurguestBundle\Controller;

include_once ('GlobalConstants.php');

use OurguestBundle\Entity\Tours;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use OurguestBundle\Entity\Reviews;
use AppBundle\Entity\Rooms;
use AppBundle\Entity\Property;
use OurguestBundle\Entity\Experiences;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use OurguestBundle\Entity\bookingsTour;


/**
 * class ToursController
 * @package OurguestBundle/Controller
 * @Route("tours")
 */
class ToursController extends Controller{

    //tours functions
    //function to render tab-content for Customisable Tours
    public function tabContentCustomiseTourAction($id){
        $em = $this->getDoctrine()->getManager();
        $tours = $em->getRepository('OurguestBundle:Tours')->findCustomisableToursByState($id);

        return $this->retrieveToursInfo($tours);
    }

    //function to render tab-content for Fixed Departures
    public function tabContentFixedDepartureAction(){
        $em = $this->getDoctrine()->getManager();

        $tours = $em->getRepository('OurguestBundle:Tours')->findFixedDepartures();

        return $this->retrieveToursInfo($tours);
    }

    /**
     * @Route("/load-more-customisable-tours/", name="load_more_customisable_tours", options={"expose"=true})
     * @Method("POST")
     */
    public function loadMoreCustomisableTours(Request $request){
        $em = $this->getDoctrine()->getManager();

        $id = $request->request->get('id');
        $toursCount = $request->request->get('itemCount');

        $tours = $em->getRepository('OurguestBundle:Tours')->findMoreCustomisableTours($id, $toursCount);

        return $this->retrieveToursInfo($tours);
    }

    /**
     * @Route("/load-more-fixed-departures/", name="load_more_fixed_departures", options={"expose"=true})
     * @Method("POST")
     */
    public function loadMoreFixedDepartures(Request $request){
        $em = $this->getDoctrine()->getManager();

        $toursCount = $request->request->get('itemCount');
        $tours = $em->getRepository('OurguestBundle:Tours')->findMoreFixedDepartures($toursCount);

        return $this->retrieveToursInfo($tours);
    }

    /**
     * @Route("/{slug}/", name="tour_template", options={"expose"=true})
     * @ParamConverter("tour", options={"mapping": {"slug": "slug"}})
     * @Method({"GET", "POST"})
     */
    public function singlePageAction(Request $request, Tours $tour){
        $em = $this->getDoctrine()->getManager();
        $id = $tour->getId();

        $tourDuration = $tour->getNoOfDays();
        $tourName = $tour->getName();
        $booking = new bookingsTour();
        $form_tour = $this->createForm('OurguestBundle\Form\bookingsTourType', $booking, array(
            'tourDuration' => $tourDuration,
        ));
        $form_tour->handleRequest($request);

        $review = new Reviews();
        $review_form = $this->createForm('OurguestBundle\Form\ReviewsType', $review);
        $review_form->handleRequest($request);

        if ($form_tour->isSubmitted() && $form_tour->isValid()){


            $data = $form_tour->getData();

            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername('ourgueststay@gmail.com')
                ->setPassword('lvsgpevkmwfhozxz');

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance();

            $message->setSubject('New ' . $tourName . ' Enquiry')
                ->setFrom(array('ourgueststay@gmail.com' => 'OurGuest'))
                ->setTo(array(TOUR_EMAIL => 'Email Owner'))
                ->setBody($this->renderView('ourguest/tours/frontend/email-template.html.twig', array('data' => $data)), 'text/html');

            $mailer->send($message);

            $em->persist($booking);
            $em->flush();
        }

        if ($review_form->isSubmitted() && $review_form->isValid()){
            $review->setTour($em->getRepository('OurguestBundle:Tours')->find($id));
            $em->persist($review);
            $em->flush();

            return $this->redirectToRoute('tour_template', array('slug' => $request->attributes->get('slug')));
        }

        return $this->render('ourguest/tours/frontend/single-page.html.twig', array(
            'tour' => $tour,
            'page_title' => $tour->getName(). ' | ' .$tour->getState()->getName(),
            'form_tour' => $form_tour->createView(),
            'form_review' => $review_form->createView(),
        ));
    }

    public function homestaysForTourAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $id = $request->attributes->get('id');
        $homestays = $em->getRepository('AppBundle:Property')->findPropertiesByTour($id);

        return $this->retrievePropertiesInfo($homestays);
    }

    public function experiencesForTourAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $id = $request->attributes->get('id');

        $experiences = $em->getRepository('OurguestBundle:Experiences')->findByTour($id);

        return $this->retrieveExperiencesForToursInfo($experiences);
    }

    public function similarToursAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $id = $request->attributes->get('id');
        $state_id = $request->attributes->get('stateId');
        $tourType = $request->attributes->get('tourType');
        $tours = $em->getRepository('OurguestBundle:Tours')->findSimilarTours($state_id, $tourType);
        shuffle($tours);
        $detail = array();
        $i = 1;

        foreach ($tours as $tour){
            if ($tour instanceof Tours and $tour->getId() != $id){
                $detail[] = array(
                    'id' => $tour->getId(),
                    'name' => $tour->getName(),
                    'featimg' => $tour->getFeatimage(),
                    'rate' => $tour->getRate(),
                    'duration' => $tour->getNoOfDays(),
                    'slug' => $tour->getSlug(),
                );
                if ($i++ == 3) break;
            }
        }

        return $this->render('ourguest/tours/frontend/tab-content.html.twig', array(
            'detail' => $detail,
        ));
    }

    /**
     * @Route("/duration/{id}", name="tour_duration", options={"expose"=true})
     */
    public function tourDurationAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $tourRepository = $em->getRepository('OurguestBundle:Tours');

        $tour = $tourRepository->createQueryBuilder('t')
            ->where('t.id = :id')
            ->setParameter('id', $request->attributes->get('id'))
            ->getQuery()
            ->execute();

        $responseArray = array();

        foreach ($tour as $value){
            if ($value instanceof Tours){
                $responseArray[] = array(
                    'duration' => $value->getNoOfDays()
                );
            }
        }
        return new JsonResponse($responseArray);
    }

    public function retrieveToursInfo($tours){
        $detail = array();

        foreach ($tours as $tour){
            if ($tour instanceof Tours){
                $detail[] = array(
                    'id' => $tour->getId(),
                    'name' => $tour->getName(),
                    'featimg' => $tour->getFeatimage(),
                    'rate' => $tour->getRate(),
                    'duration' => $tour->getNoOfDays(),
                    'slug' => $tour->getSlug(),
                );
            }
        }
        return $this->render('ourguest/tours/frontend/tab-content.html.twig', array(
            'detail' => $detail,
        ));
    }

    public function retrievePropertiesInfo($homestays){

        $detail = array();

        foreach ($homestays as $value){
            if($value instanceof Property){
                $rate = 0;
                $detail[] = array(
                    'id' => $value->getId(),
                    'name' => $value->getName(),
                    'featimg' => $value->getFeatimage(),
                    'rate' => $rate,
                    'colNum' => 6,
                    'slug' => $value->getSlug(),
                    'route' => 'homestay_template',
                );
            }
        }

        return $this->render('ourguest/default/multi-item-carousel.html.twig', array(
            'detail' => $detail,
        ));
    }

    public function getCheapestRoom($rooms){
        $min_rate = 10000;
        foreach ($rooms as $room){
            if ($room instanceof Rooms){
                $rate = $room->getRate() + $room->getCpCost();
                if ($min_rate > $rate)
                    $min_rate = $rate;
            }
        }

        return $min_rate;
    }

    public function retrieveExperiencesForToursInfo($experiences){

        $detail = array();

        foreach ($experiences as $experience){
            if ($experience instanceof Experiences){
                $detail[] = array(
                    'id' => $experience->getId(),
                    'name' => $experience->getName(),
                    'featimg' => $experience->getFeatimage(),
                    'colNum' => 6,
                );
            }
        }
        return $this->render('ourguest/default/multi-item-carousel.html.twig', array(
            'detail' => $detail,
        ));
    }
}
