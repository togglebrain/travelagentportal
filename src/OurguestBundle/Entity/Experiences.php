<?php

namespace OurguestBundle\Entity;

use AppBundle\Entity\Property;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use OurguestBundle\OurguestBundle;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Experiences
 *
 * @ORM\Table(name="experiences")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\ExperiencesRepository")
 */
class Experiences
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"image/jpg","image/jpeg", "image/png"},
     *     mimeTypesMessage = "Please upload a valid image"
     * )
     * @ORM\Column(name="featimage", type="string", length=255)
     */
    private $featimage;

    /**
     * @var string
     * @ORM\Column(name="altText", type="string", length=125, nullable=true)
     */
    private $altText;

    /**
     * @var string
     *
     * @ORM\Column(name="videoUrl", type="string", length=255, nullable=true)
     */
    private $videoUrl;

    /**
     * @var boolean
     * @ORM\Column(name="flag", type="boolean", nullable=true)
     */
    private $isFeatured;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location",inversedBy="experience")
     * @Assert\NotBlank()
     */
    protected $location;


    /**
     * @ORM\ManyToMany(targetEntity="OurguestBundle\Entity\Tours", mappedBy="experiences")
     * @ORM\JoinColumn(name="tour_id", onDelete="SET NULL", nullable=true)
     * @var
     */
    protected $tour;

    /*/**
     * @ORM\ManyToMany(targetEntity="OurguestBundle\Entity\bookingsExperience", mappedBy="experiences")
     * @ORM\JoinColumn(name="bookingsExperiences_id", onDelete="SET NULL", nullable=true)

    protected $bookingsExperience;*/

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="OurguestBundle\Entity\Reviews", mappedBy="experience")
     */
    private $review;


    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Images", mappedBy="experience",cascade={"all"},orphanRemoval=true)
     */
    private $image;


    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Property", mappedBy="experiences")
     * @ORM\JoinColumn(name="property_id", onDelete="SET NULL", nullable=true)
     * @var
     */
    private $property;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true, nullable=true)
     */
    protected $slug;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Experiences
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Experiences
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set featimage
     *
     * @param string $featimage
     *
     * @return Experiences
     */
    public function setFeatimage($featimage)
    {
        $this->featimage = $featimage;
        return $this;
    }
    /**
     * Get featimage
     *
     * @return string
     */
    public function getFeatimage()
    {
        return $this->featimage;
    }

    /**
     * Set Alt Text
     * @param string $altText
     * @return Experiences
     */
    public function setAltText($altText){
        $this->altText = $altText;

        return $this;
    }

    /**
     * Get Alt Text
     * @return string
     */
    public function getAltText(){
        return $this->altText;
    }

    /**
     * Set videoUrl
     *
     * @param string $videoUrl
     *
     * @return Experiences
     */
    public function setVideoUrl($videoUrl)
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    /**
     * Get videoUrl
     *
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->videoUrl;
    }

    /**
     * @return boolean
     */
    public function isIsFeatured(){
        return $this->isFeatured;
    }

    /**
     * Set Is Featured
     * @param boolean $isFeatured
     */
    public function setIsFeatured($isFeatured){
        $this->isFeatured = $isFeatured;
    }

    /**
     * Get Is Featured
     * @return boolean
     */
    public function getIsFeatured(){
        return $this->isFeatured;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->review = new \Doctrine\Common\Collections\ArrayCollection();
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
        $this->property = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tour = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set location-page
     *
     * @param \AppBundle\Entity\Location $location
     *
     * @return Experiences
     */
    public function setLocation(\AppBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location-page
     *
     * @return \AppBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }


    /**
     * Add review
     *
     * @param \OurguestBundle\Entity\Reviews $review
     *
     * @return Experiences
     */
    public function addReview(\OurguestBundle\Entity\Reviews $review)
    {
        $this->review[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \OurguestBundle\Entity\Reviews $review
     */
    public function removeReview(\OurguestBundle\Entity\Reviews $review)
    {
        $this->review->removeElement($review);
    }

    /**
     * Get review
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Experiences
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->image[] = $image;
        $image->setExperience($this);
        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     */
    public function getProperty()
    {
        return $this->property;
    }

    public function addProperty(Property $property)
    {
        if (!$this->property->contains($property)) {
            $this->property[] = $property;
        }
        return $this;
    }
    public function removeProperty(Property $property)
    {
        if ($this->property->contains($property)) {
            $this->property->removeElement($property);
        }
        return $this;
    }


    /**
     */
    public function getTour()
    {
        return $this->tour;
    }

    public function addTour(Tours $tours)
    {
        if (!$this->tour->contains($tours)) {
            $this->tour[] = $tours;
        }
        return $this;
    }
    public function removeTour(Tours $tours)
    {
        if ($this->tour->contains($tours)) {
            $this->tour->removeElement($tours);
        }
        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function setSlug($slug){
        $this->slug = $slug;
    }

    public function getSlug(){
        return $this->slug;
    }

}
