<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class bookingsCustomise
 * @ORM\Table(name="bookings_customise")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\bookingsCustomiseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class bookingsCustomise{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="customer_name", type="string", length=255)
     */
    private $customerName;

    /**
     * @var array
     * @ORM\Column(name="tour_destination", type="json_array")
     */
    private $tourDestination;

    /**
     * @var string
     * @ORM\Column(name="tour_duration", type="integer")
     */
    private $tourDuration;

    /**
     * @var \DateTime
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;
    
    /**
     * @var int
     * @ORM\Column(name="adult_quantity", type="integer")
     */
    private $adultQuantity;

    /**
     * @var int
     * @ORM\Column(name="child_quantity", type="integer")
     */
    private $childQuantity;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=10)
     * @Assert\Type(type="digit", message="Mobile Number has to be numeric!")
     * @Assert\Regex(pattern="/^[0-9]{10}$/", message="Mobile number has to be 10 digits!")
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="email", type="string")
     * @Assert\Regex(pattern="/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", message="Enter a valid Email ID!")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * Get Customise Tour Booking id
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set Customer Name
     * @param string $customerName;
     * @return bookingsCustomise
     */
    public function setName($customerName){
        $this->customerName = $customerName;
        return $this;
    }

    /**
     * Get Customer Name
     * @return string
     */
    public function getName(){
        return $this->customerName;
    }
    
    /**
     * Set Tour Destination
     * @param array $tourDestination
     * @return bookingsCustomise
     */
    public function setTourDestination($tourDestination){
        $this->tourDestination = $tourDestination;

        return $this;
    }
    
    /**
     * Get Tour Destination
     * @return array
     */
    public function getTourDestination(){
        return $this->tourDestination;
    }

    /**
     * Set Tour Duration
     * @param string $tourDuration
     * @return bookingsCustomise
     */
    public function setTourDuration($tourDuration){
        $this->tourDuration = $tourDuration;
        return $this;
    }

    /**
     * Get Tour Duration
     * @return string
     */
    public function getTourDuration(){
        return $this->tourDuration;
    }

    /**
     * Set Start Date
     * @param \DateTime $startDate
     * @return bookingsCustomise
     */
    public function setStartDate($startDate){
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get Start Date
     * @return \DateTime
     */
    public function getStartDate(){
        return $this->startDate;
    }

    /**
     * Set Number of Guests above 6 years
     * @param int $adultQuantity
     * @return bookingsCustomise
     */
    public function setAdultQuantity($adultQuantity){
        $this->adultQuantity = $adultQuantity;
        return $this;
    }

    /**
     * Get Number of Guests above 6 years
     * @return int
     */
    public function getAdultQuantity(){
        return $this->adultQuantity;
    }

    /**
     * Set Number of Guests below 6 years
     * @param int $childQuantity
     * @return bookingsCustomise
     */
    public function setChildQuantity($childQuantity){
        $this->childQuantity = $childQuantity;
        return $this;
    }

    /**
     * Get Number of Guests below 6 years
     * @return int
     */
    public function getChildQuantity(){
        return $this->childQuantity;
    }

    /**
     * Set Customer Phone Number
     * @param string $phone
     * @return bookingsCustomise
     */
    public function setPhone($phone){
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get Customer Phone Number
     * @return string
     */
    public function getPhone(){
        return $this->phone;
    }

    /**
     * Set Customer Email
     * @param string $email
     * @return bookingsCustomise
     */
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    /**
     * Get Customer Email
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Set Message
     * @param string $message
     * @return bookingsCustomise
     */
    public function setMessage($message){
        $this->message = $message;
        return $this;
    }

    /**
     * Get Message
     * @return string
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * Set Created
     * @param \DateTime $created
     * @return bookingsCustomise
     */
    public function setCreated($created){
        $this->created = $created;

        return $this;
    }

    /**
     * Get Created
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }

    /**
     * Triggered on Insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->created = new \DateTime("now");
    }
}