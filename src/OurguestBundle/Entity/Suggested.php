<?php

namespace OurguestBundle\Entity;

use AppBundle\Entity\Property;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Suggested
 * @package OurguestBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="suggested")
 */
class Suggested{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="OurguestBundle\Entity\Tours", inversedBy="suggested")
     */
    private $tour;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Property", inversedBy="suggested")
     */
    private $homestay;

    /**
     * Get Id
     * @return int
     */
    public function getId(){

        return $this->id;
    }

    /**
     * Set Tour Id
     * @param Tours $tour
     * @return Suggested
     */
    public function setTour($tour){
        $this->tour = $tour;
        return $this;
    }

    /**
     * Get Tour Id
     * @return Tours
     */
    public function getTour(){
        return $this->tour;
    }

    /**
     * Set Homestay Id
     * @param Property $homestay
     * @return Suggested
     */
    public function setHomestay($homestay){
        $this->homestay = $homestay;

        return $this;
    }

    /**
     * Get Homestay Id
     * @return Property
     */
    public function getHomestay(){
        return $this->homestay;
    }
}