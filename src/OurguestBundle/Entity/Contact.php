<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Contact
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\ContactRepository")
 * @ORM\HasLifecycleCallbacks
 */

class Contact{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Regex(pattern="/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", message="Enter a valid Email ID!")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=10)
     * @Assert\Type(type="digit", message="Mobile Number has to be numeric!")
     * @Assert\Regex(pattern="/^[0-9]{10}$/", message="Mobile number has to be 10 digits!")
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="subject", type="string", length=50)
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(name="message", type="text", length=255)
     */
    private $message;

    /**
     * @var \DateTime
     * @ORM\Column(name="received_on", type="datetime")
     */
    private $created;

    /**
     * Get Id
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set Name
     * @param string $name
     * @return Contact
     */
    public function setName($name){
        $this->name = $name;

        return $this;
    }

    /**
     * Get Name
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set Email
     * @param string $email
     * @return Contact
     */
    public function setEmail($email){
        $this->email = $email;

        return $this;
    }

    /**
     * Get Email
     * @return string
     */
    public function getEMail(){
        return $this->email;
    }

    /**
     * Set Phone Number
     * @param string $phone
     * @return Contact
     */
    public function setPhone($phone){
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get Phone Number
     * @return string
     */
    public function getPhone(){
        return $this->phone;
    }

    /**
     * Set Subject
     * @param string $subject
     * @return Contact
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Get Subject
     * @return string
     */
    public function getSubject(){
        return $this->subject;
    }

    /**
     * Set Message
     * @param string $message
     * @return Contact
     */
    public function setMessage($message){
        $this->message = $message;

        return $this;
    }

    /**
     * Get Message
     * @return string
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * Set Created
     * @param \DateTime $created
     * @return Contact
     */
    public function setCreated($created){
        $this->created = $created;

        return $this;
    }

    /**
     * Get Created
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }

    /**
     * Triggered on Insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->created = new \DateTime("now");
    }
}