<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reviews
 *
 * @ORM\Table(name="reviews")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\ReviewsRepository")
 */
class Reviews
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255)
     */
    private $comment;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Property",inversedBy="review")
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $property;

    /**
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\Experiences",inversedBy="review")
     * @ORM\JoinColumn(name="experience_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $experience;


    /**
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\Tours",inversedBy="review")
     * @ORM\JoinColumn(name="tour_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $tour;

    /**
     * @var bool $approved
     * @ORM\Column(name="status", type="boolean", options={"default": false}, nullable=true)
     */
    protected $status;

    /**
     * @var \DateTime $created
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    protected $created;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Rating
     *
     * @param int $rating
     * @return Reviews
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * Get Rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Reviews
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set user
     *
     * @param string $name
     *
     * @return Reviews
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Email
     *
     * @param string $email
     *
     * @return Reviews
     */
    public function setEmail($email){
        $this->email = $email;

        return $this;
    }

    /**
     * Get Email
     *
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Set experience
     *
     * @param \OurguestBundle\Entity\Experiences $experience
     *
     * @return Reviews
     */
    public function setExperience(\OurguestBundle\Entity\Experiences $experience = null)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return \OurguestBundle\Entity\Experiences
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set tour
     *
     * @param \OurguestBundle\Entity\Tours $tour
     *
     * @return Reviews
     */
    public function setTour(\OurguestBundle\Entity\Tours $tour = null)
    {
        $this->tour = $tour;

        return $this;
    }

    /**
     * Get tour
     *
     * @return \OurguestBundle\Entity\Tours
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * Set property
     *
     * @param \AppBundle\Entity\Property $property
     *
     * @return Reviews
     */
    public function setProperty(\AppBundle\Entity\Property $property = null)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \AppBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set Status
     *
     * @param bool $status
     * @return Reviews
     */
    public function setStatus($status){
        $this->status = $status;

        return $this;
    }

    /**
     * Get Status
     *
     * @return bool
     */
    public function getStatus(){
        return $this->status;
    }

    /**
     * Set Created
     *
     * @param \DateTime $created
     * @return Reviews
     */
    public function setCreated($created){
        $this->created = $created;

        return $this;
    }

    /**
     * Get Created
     *
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist()
     */
    public function onPrePersist(){
        $this->created = new \DateTime('now');
    }

    public function __toString()
    {
        return $this->getName();
    }
}
