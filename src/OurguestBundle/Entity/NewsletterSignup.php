<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields={"email"}, message="Email is already registered")
 * @ORM\Table(name="newsletter_signups")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\NewsletterSignupRepository")
 * @ORM\HasLifecycleCallbacks
 */

class NewsletterSignup{

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $email
     * @ORM\Column(name="email", type="string")
     * @Assert\Regex(pattern="/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", message="Enter a valid Email ID!")
     */
    private $email;

    /**
     * Get Id
     * @return integer
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set Email
     * @param string $email
     * @return NewsletterSignup
     */
    public function setEMail($email){
        $this->email = $email;

        return $this;
    }

    /**
     * Get Email
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    protected $created;


    /**
     * Set Created
     * @param \DateTime $created
     * @return NewsletterSignup
     */
    public function setCreated($created){
        $this->created = $created;

        return $this;
    }

    /**
     * Get Created
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }

    /**
     * Triggered on Insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->created = new \DateTime("now");
    }
}
