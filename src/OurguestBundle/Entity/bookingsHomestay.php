<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class bookingsHomestay
 * @ORM\Table(name="bookings_homestay")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\bookingsHomestayRepository")
 * @ORM\HasLifecycleCallbacks
 */
class bookingsHomestay{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="homestay_name", type="string", length=255)
     */
    private $homestayName;

    /**
     * @var string
     * @ORM\Column(name="customer_name", type="string", length=255)
     */
    private $customerName;

    /**
     * @var \DateTime
     * @ORM\Column(name="check_in", type="datetime")
     */
    private $checkIn;

    /**
     * @var \DateTime
     * @ORM\Column(name="check_out", type="datetime")
     */
    private $checkOut;

    /**
     * @var string
     * @ORM\Column(name="room_type", type="string", length=255)
     */
    private $room;

    /**
     * @var int
     * @ORM\Column(name="adult_quantity", type="integer")
     */
    private $adultQuantity;

    /**
     * @var int
     * @ORM\Column(name="child_quantity", type="integer")
     */
    private $childQuantity;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=10)
     * @Assert\Type(type="digit", message="Mobile Number has to be numeric!")
     * @Assert\Regex(pattern="/^[0-9]{10}$/", message="Mobile Number has to be 10 digits.")
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="email", type="string")
     * @Assert\Regex(pattern="/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", message="Enter a valid Email ID!")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="message", type="text", length=255)
     */
    private $message;

    /**
     * Get Homestay Booking ID
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set Customer Name
     * @param string $customerName
     * @return bookingsHomestay
     */
    public function setName($customerName){
        $this->customerName = $customerName;
        return $this;
    }

    /**
     * Get Customer Name
     * @return string
     */
    public function getName(){
        return $this->customerName;
    }

    /**
     * @return string
     */
    public function getHomestayName()
    {
        return $this->homestayName;
    }

    /**
     * @param string $homestayName
     */
    public function setHomestayName($homestayName)
    {
        $this->homestayName = $homestayName;
    }

    /**
     * Set Check In Date
     * @param \DateTime $checkIn
     * @return bookingsHomestay
     */
    public function setCheckIn($checkIn){
        $this->checkIn = $checkIn;
        return $this;
    }

    /**
     * Get Check In Date
     * @return \DateTime
     */
    public function getCheckIn(){
        return $this->checkIn;
    }

    /**
     * Set Check Out Date
     * @param \DateTime $checkOut
     * @return bookingsHomestay
     */
    public function setCheckOut($checkOut){
        $this->checkOut = $checkOut;
        return $this;
    }

    /**
     * Get Check Out Date
     * @return \DateTime
     */
    public function getCheckOut(){
        return $this->checkOut;
    }

    /**
     * Get Number of Guests above 6 years
     * @return integer
     */
    public function getAdultQuantity(){
        return $this->adultQuantity;
    }

    /**
     * Set Number of Guests above 6 years
     * @param integer $adultQuantity
     * @return bookingsHomestay
     */
    public function setAdultQuantity($adultQuantity){
        $this->adultQuantity = $adultQuantity;
        return $this;
    }

    /**
     * Get Number of Guests below 6 years
     * @return integer
     */
    public function getChildQuantity(){
        return $this->childQuantity;
    }

    /**
     * Set Number of Guests below 6 years
     * @param integer $childQuantity
     * @return bookingsHomestay
     */
    public function setChildQuantity($childQuantity){
        $this->childQuantity = $childQuantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param string $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param string $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * Set Customer Phone Number
     * @param string $phone
     * @return bookingsHomestay
     */
    public function setPhone($phone){
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get Customer Phone Number
     * @return string
     */
    public function getPhone(){
        return $this->phone;
    }

    /**
     * Set Customer Email
     * @param string $email
     * @return bookingsHomestay
     */
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    /**
     * Get Customer Email
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Set Message
     * @param string $message
     * @return bookingsHomestay
     */
    public function setMessage($message){
        $this->message = $message;
        return $this;
    }

    /**
     * Get Message
     * @return string
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    protected $created;


    /**
     * Set Created
     * @param \DateTime $created
     * @return bookingsHomestay
     */
    public function setCreated($created){
        $this->created = $created;

        return $this;
    }

    /**
     * Get Created
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }

    /**
     * Triggered on Insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->created = new \DateTime("now");
    }
}
