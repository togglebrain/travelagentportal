<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Property;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Facilities
 *
 * @ORM\Table(name="facilities")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\FacilitiesRepository")
 */
class Facilities
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Property", mappedBy="facilities")
     * @ORM\JoinColumn(name="property_id", onDelete="SET NULL", nullable=true)
     * @var
     */
    private $property;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Facilities
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->property = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     */
    public function getProperty()
    {
        return $this->property;
    }

    public function addProperty(Property $property)
    {
        if (!$this->property->contains($property)) {
            $this->property[] = $property;
        }
        return $this;
    }
    public function removeProperty(Property $property)
    {
        if ($this->property->contains($property)) {
            $this->property->removeElement($property);
        }
        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
