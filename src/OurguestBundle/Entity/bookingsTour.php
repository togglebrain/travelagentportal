<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class bookingsTour
 * @ORM\Table(name="bookings_tour")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\bookingsTourRepository")
 * @ORM\HasLifecycleCallbacks
 */
class bookingsTour{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="tour_name", type="string", length=255)
     */
    private $tourName;

    /**
     * @var string
     * @ORM\Column(name="customer_name", type="string", length=255)
     */
    private $customerName;

    /**
     * @var integer
     * @ORM\Column(name="tour_duration", type="integer")
     */
    private $tourDuration;

    /**
     * @var \DateTime
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var int
     * @ORM\Column(name="adult_quantity", type="integer")
     */
    private $adultQuantity;

    /**
     * @var int
     * @ORM\Column(name="child_quantity", type="integer")
     */
    private $childQuantity;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=10)
     * @Assert\Type(type="digit", message="Mobile Number has to be numeric!")
     * @Assert\Regex(pattern="/^[0-9]{10}$/", message="Mobile number has to be 10 digits!")
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="email", type="string")
     * @Assert\Regex(pattern="/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", message="Enter a valid Email ID!")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var \DateTime
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;

    /**
     * Get Tour Booking id
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set Customer Name
     * @param string $customerName;
     * @return bookingsTour
     */
    public function setName($customerName){
        $this->customerName = $customerName;
        return $this;
    }

    /**
     * Get Customer Name
     * @return string
     */
    public function getName(){
        return $this->customerName;
    }

    /**
     * @return string
     */
    public function getTourName()
    {
        return $this->tourName;
    }

    /**
     * @param string $tourName
     */
    public function setTourName($tourName)
    {
        $this->tourName = $tourName;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param string $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }



    /**
     * Set Tour Duration
     * @param integer $tourDuration
     * @return bookingsTour
     */
    public function setTourDuration($tourDuration){
        $this->tourDuration = $tourDuration;
        return $this;
    }

    /**
     * Get Tour Duration
     * @return integer
     */
    public function getTourDuration(){
        return $this->tourDuration;
    }

    /**
     * Set Start Date
     * @param \DateTime $startDate
     * @return bookingsTour
     */
    public function setStartDate($startDate){
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get Start Date
     * @return \DateTime
     */
    public function getStartDate(){
        return $this->startDate;
    }

    /**
     * Set Number of Adult Guests
     * @param integer $adultQuantity
     * @return bookingsTour
     */
    public function setAdultQuantity($adultQuantity){
        $this->adultQuantity = $adultQuantity;
        return $this;
    }

    /**
     * Get Number of Adult Guests
     * @return integer
     */
    public function getAdultQuantity(){
        return $this->adultQuantity;
    }

    /**
     * Set Number of Child Guests
     * @param integer $childQuantity
     * @return bookingsTour
     */
    public function setChildQuantity($childQuantity){
        $this->childQuantity = $childQuantity;
        return $this;
    }

    /**
     * Get Number of Guests
     * @return integer
     */
    public function getChildQuantity(){
        return $this->childQuantity;
    }

    /**
     * Set Customer Phone Number
     * @param string $phone
     * @return bookingsTour
     */
    public function setPhone($phone){
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get Customer Phone Number
     * @return string
     */
    public function getPhone(){
        return $this->phone;
    }

    /**
     * Set Customer Email
     * @param string $email
     * @return bookingsTour
     */
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    /**
     * Get Customer Email
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Set Message
     * @param string $message
     * @return bookingsTour
     */
    public function setMessage($message){
        $this->message = $message;
        return $this;
    }

    /**
     * Get Message
     * @return string
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * Set Created
     * @param \DateTime $created
     * @return bookingsTour
     */
    public function setCreated($created){
        $this->created = $created;
        return $this;
    }

    /**
     * Get Created
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }

    /**
     * Triggered on Insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
    }
}