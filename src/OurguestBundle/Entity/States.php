<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * States
 *
 * @ORM\Table(name="states")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\StatesRepository")
 */
class States
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */

    private $name;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Location", mappedBy="state")
     */

    private $location;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="OurguestBundle\Entity\Tours", mappedBy="state")
     */
    private $tour;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true, nullable=true)
     */
    protected $slug;

    /**
     * Get id
     *
     * @return int
     */

    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return States
     */

    public function setName($name){
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */

    public function getName(){
        return $this->name;
    }

    /**
     * Constructor
     */

    public function __construct()
    {
        $this->location = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tour = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add Location
     * @param \AppBundle\Entity\Location $location
     * @return States
     */

    public function addLocation(\AppBundle\Entity\Location $location){
        $this->location[] = $location;
        return $this;
    }

    /**
     * Remove Location
     * @param \AppBundle\Entity\Location $location
     */

    public function removeLocation(\AppBundle\Entity\Location $location){
        $this->location->removeElement($location);
    }

    /**
     * Get Location
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocation(){
        return $this->location;
    }

    /**
     * Add tour
     *
     * @param \OurguestBundle\Entity\Tours $tour
     *
     * @return States
     */
    public function addTour(\OurguestBundle\Entity\Tours $tour)
    {
        $this->tour[] = $tour;

        return $this;
    }

    /**
     * Remove tour
     *
     * @param \OurguestBundle\Entity\Tours $tour
     */
    public function removeTour(\OurguestBundle\Entity\Tours $tour)
    {
        $this->tour->removeElement($tour);
    }

    /**
     * Get tour
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTour()
    {
        return $this->tour;
    }

    public function setSlug($slug){
        $this->slug = $slug;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function __toString()
    {
        return $this->getName();
    }

}
