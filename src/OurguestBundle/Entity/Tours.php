<?php

namespace OurguestBundle\Entity;

use AppBundle\Entity\Property;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Tours
 *
 * @ORM\Table(name="tours")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\ToursRepository")
 */
class Tours
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="noOfDays", type="integer")
     */
    private $noOfDays;

    /**
     * @var \DateTime
     * @ORM\Column(name="start_date", type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="end_date", type="date", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\OneToMany(targetEntity="OurguestBundle\Entity\Itinerary", mappedBy="tour", cascade={"all"}, orphanRemoval=true)
     */
    private $itinerary;


    /**
     * @var string
     *
     * @ORM\Column(name="tour_overview", type="text")
     */
    private $tourOverview;

    /**
     * @var string
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"image/jpg","image/jpeg","image/png"},
     *     mimeTypesMessage = "Please upload a valid image"
     * )
     * @ORM\Column(name="featimage", type="string", length=255)
     */
    private $featimage;

    /**
     * @var string
     * @ORM\Column(name="altText", type="string", length=125, nullable=true)
     */
    private $altText;

    /**
     * @var string
     *
     * @ORM\Column(name="videoUrl", type="string", length=255, nullable=true)
     */
    private $videoUrl;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $tourType;

    /**
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\States",inversedBy="tour")
     * @Assert\NotBlank()
     */
    protected $state;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Images", mappedBy="tour",cascade={"all"},orphanRemoval=true)
     */
    private $image;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="OurguestBundle\Entity\Reviews", mappedBy="tour")
     */
    private $review;

    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="float")
     */
    private $rate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="flag", type="boolean", nullable=true)
     */
    private $isFeatured;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;

    /**
     * @ORM\ManyToMany(targetEntity="OurguestBundle\Entity\Experiences", inversedBy="tour")
     * @ORM\JoinColumn(name="experiences", onDelete="SET NULL", nullable=true)
     */
    private $experiences;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Property", inversedBy="tour")
     * @ORM\JoinColumn(name="property_id", onDelete="SET NULL", nullable=true)
     */
    private $property;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true, nullable=true)
     */
    protected $slug;

    /**
     * @ORM\OneToOne(targetEntity="OurguestBundle\Entity\Suggested", mappedBy="tour")
     */
    protected $suggested;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tours
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set noOfDays
     *
     * @param integer $noOfDays
     *
     * @return Tours
     */
    public function setNoOfDays($noOfDays)
    {
        $this->noOfDays = $noOfDays;

        return $this;
    }

    /**
     * Get noOfDays
     *
     * @return int
     */
    public function getNoOfDays()
    {
        return $this->noOfDays;
    }

    /**
     * Set Start Date
     * @param \DateTime $startDate
     * @return Tours
     */
    public function setStartDate($startDate){
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get Start Date
     * @return \DateTime
     */
    public function getStartDate(){
        return $this->startDate;
    }

    /**
     * Set End Date
     * @param \DateTime $endDate
     * @return Tours
     */
    public function setEndDate($endDate){
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * Get End Date
     * @return \DateTime
     */
    public function getEndDate(){
        return $this->endDate;
    }

    /**
     * Set featimage
     *
     * @param string $featimage
     *
     * @return Tours
     */
    public function setFeatimage($featimage)
    {
        $this->featimage = $featimage;
        return $this;
    }
    /**
     * Get featimage
     *
     * @return string
     */
    public function getFeatimage()
    {
        return $this->featimage;
    }

    /**
     * Set Alt Text
     * @param string $altText
     * @return Tours
     */
    public function setAltText($altText){
        $this->altText = $altText;

        return $this;
    }

    /**
     * Get Alt Text
     * @return string
     */
    public function getAltText(){
        return $this->altText;
    }

    /**
     * Set videoUrl
     *
     * @param string $videoUrl
     *
     * @return Tours
     */
    public function setVideoUrl($videoUrl)
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    /**
     * Get videoUrl
     *
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->videoUrl;
    }

    /**
     * Set tourType
     *
     * @param integer $tourType
     *
     * @return Tours
     */
    public function setTourType($tourType)
    {
        $this->tourType = $tourType;

        return $this;
    }

    /**
     * Get tourType
     *
     * @return int
     */
    public function getTourType()
    {
        return $this->tourType;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->image = new \Doctrine\Common\Collections\ArrayCollection();
        $this->review = new \Doctrine\Common\Collections\ArrayCollection();
        $this->experiences = new \Doctrine\Common\Collections\ArrayCollection();
        $this->property = new \Doctrine\Common\Collections\ArrayCollection();
        $this->itinerary = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set State
     *
     * @param \OurguestBundle\Entity\States $state
     *
     * @return Tours
     */
    public function setState(\OurguestBundle\Entity\States $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get State
     *
     * @return \OurguestBundle\Entity\States
     */
    public function getState()
    {
        return $this->state;
    }


    /**
     * Add image
     *
     * @param \AppBundle\Entity\Images $image
     *
     * @return Tours
     */
    public function addImage(\AppBundle\Entity\Images $image)
    {
        $this->image[] = $image;
        $image->setTour($this);
        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\Images $image
     */
    public function removeImage(\AppBundle\Entity\Images $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * Add itinerary
     *
     * @param \OurguestBundle\Entity\Itinerary $itinerary
     *
     * @return Tours
     */
    public function addItinerary(\OurguestBundle\Entity\Itinerary $itinerary)
    {
        $this->itinerary[] = $itinerary;
        $itinerary->setTour($this);
        return $this;
    }

    /**
     * Remove itinerary
     *
     * @param \OurguestBundle\Entity\Itinerary
     */
    public function removeItinerary(\OurguestBundle\Entity\Itinerary $itinerary)
    {
        $this->itinerary->removeElement($itinerary);
    }

    /**
     * Get itinerary
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItinerary()
    {
        return $this->itinerary;
    }



    /**
     * Add review
     *
     * @param \OurguestBundle\Entity\Reviews $review
     *
     * @return Tours
     */
    public function addReview(\OurguestBundle\Entity\Reviews $review)
    {
        $this->review[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \OurguestBundle\Entity\Reviews $review
     */
    public function removeReview(\OurguestBundle\Entity\Reviews $review)
    {
        $this->review->removeElement($review);
    }

    /**
     * Get review
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReview()
    {
        return $this->review;
    }


    /**
     * Set tourOverview
     *
     * @param string $tourOverview
     *
     * @return Tours
     */
    public function setTourOverview($tourOverview)
    {
        $this->tourOverview = $tourOverview;

        return $this;
    }

    /**
     * Get tourOverview
     *
     * @return string
     */
    public function getTourOverview()
    {
        return $this->tourOverview;
    }

    /**
     * Set rate
     *
     * @param float $rate
     *
     * @return Tours
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }


    /**
     * @return boolean
     */
    public function isIsFeatured()
    {
        return $this->isFeatured;
    }

    /**
     * @param boolean $isFeatured
     */
    public function setIsFeatured($isFeatured)
    {
        $this->isFeatured = $isFeatured;
    }

    /**
     * Get isFeatured
     *
     * @return boolean
     */
    public function getIsFeatured()
    {
        return $this->isFeatured;
    }

    /**
     * Set Keywords
     *
     * @param string $keywords
     *
     * @return Tours
     */
    public function setKeywords($keywords){
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get Keywords
     *
     * @return string
     */
    public function getKeywords(){
        return $this->keywords;
    }

    /**
     */
    public function getExperiences()
    {
        return $this->experiences;
    }

    public function addExperience(Experiences $experiences)
    {
        if (!$this->experiences->contains($experiences)) {
            $this->experiences[] = $experiences;
        }
        return $this;
    }
    public function removeExperience(Experiences $experiences)
    {
        if ($this->experiences->contains($experiences)) {
            $this->experiences->removeElement($experiences);
        }
        return $this;
    }

    /**
     */
    public function getProperty(){
        return $this->property;
    }

    public function addProperty(Property $property){
        if(!$this->property->contains($property)){
            $this->property[] = $property;
        }
        return $this;
    }

    public function removeProperty(Property $property){
        if($this->property->contains($property)){
            $this->property->removeElement($property);
        }
        return $this;
    }

    public  function setSlug($slug) {
        $this->slug = $slug;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Set Suggested
     * @param Suggested $suggested
     * @return Tours
     */
    public function setSuggested($suggested){
        $this->suggested = $suggested;

        return $this;
    }

    /**
     * Get Suggested
     * @return Suggested
     */
    public function getSuggested(){
        return $this->suggested;
    }
}
