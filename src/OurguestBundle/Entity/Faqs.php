<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * FAQs
 *
 * @ORM\Table(name="faqs")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\FaqsRepository")
 */
class Faqs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="OurguestBundle\Entity\Qa", mappedBy="faq", cascade={"all"}, orphanRemoval=true)
     */
    private $questionAnswer;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Faqs
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questionAnswer = new ArrayCollection();
    }

    /**
     * Add questionAnswer
     *
     * @param \OurguestBundle\Entity\Qa $questionAnswer
     *
     * @return Faqs
     */
    public function addQuestionAnswer(\OurguestBundle\Entity\Qa $questionAnswer)
    {

        $this->questionAnswer[] = $questionAnswer;
        $questionAnswer->setFaq($this);
        return $this;
    }

//    public function setQa($qa)
//    {
//        $this->qa=$qa;
//        return $this;
//    }

    /**
     * Remove questionAnswer
     *
     * @param \OurguestBundle\Entity\Qa $questionAnswer
     */
    public function removeQuestionAnswer(\OurguestBundle\Entity\Qa $questionAnswer)
    {
        $this->questionAnswer->removeElement($questionAnswer);
    }


    /**
     * Get questionAnswer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionAnswer()
    {
        return $this->questionAnswer;
    }
}
