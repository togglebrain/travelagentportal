<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Qa
 * @package OurguestBundle\Entity
 * @ORM\Table(name="qa")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\QaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Qa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text")
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text")
     */
    private $answer;

    /**
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\Faqs", inversedBy="questionAnswer")
     * @ORM\JoinColumn(name="faq_id", referencedColumnName="id")
     */
    private $faq;


    /**
     * Get Id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set Question
     *
     * @param string $question
     * @return Faqs
     */
    public function setQuestion($question){

        $this->question = $question;

        return $this;
    }

    /**
     * Get Question
     *
     * @return string
     */
    public function getQuestion(){
        return $this->question;
    }

    /**
     * Set Answer
     *
     * @param string $answer
     * @return Faqs
     */
    public function setAnswer($answer){

        $this->answer = $answer;
        return $this;
    }

    /**
     * Get Answer
     *
     * @return string
     */
    public function getAnswer(){
        return $this->answer;
    }

    /**
     * Set faq
     *
     * @param \OurguestBundle\Entity\Faqs $faq
     *
     * @return Qa
     */
    public function setFaq(\OurguestBundle\Entity\Faqs $faq)
    {
        $this->faq = $faq;

        return $this;
    }

    /**
     * Get faq
     *
     * @return \OurguestBundle\Entity\Faqs
     */
    public function getFaq()
    {
        return $this->faq;
    }
}
