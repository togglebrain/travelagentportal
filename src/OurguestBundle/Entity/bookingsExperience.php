<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class bookingsExperience
 * @ORM\Table(name="bookings_experience")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\bookingsExperienceRepository")
 * @ORM\HasLifecycleCallbacks
 */

class bookingsExperience{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="customer_name", type="string", length=255)
     */
    private $customerName;

    /**
     * @var int
     * @ORM\Column(name="guest_quantity", type="integer")
     */
    private $guestQuantity;

    /**
     * @var string
     * @ORM\Column(name="month", type="string")
     */
    private $month;

    /**
     * @var string
     * @ORM\Column(name="email", type="string")
     * @Assert\Regex(pattern="/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/", message="Enter a valid Email ID!")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=10)
     * @Assert\Type(type="digit", message="Mobile Number has to be numeric!")
     * @Assert\Regex(pattern="/^[0-9]{10}$/", message="Mobile Number has to be 10 digits.")
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="message", type="text", length=255)
     */
    private $message;

    /**
     * @var array
     *
     * @ORM\Column(name="address", type="json_array")
     */
    private $experiences;

    /**
     * Get Homestay Booking ID
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set Customer Name
     * @param string $customerName
     * @return bookingsExperience
     */
    public function setName($customerName){
        $this->customerName = $customerName;
        return $this;
    }

    /**
     * Get Customer Name
     * @return string
     */
    public function getName(){
        return $this->customerName;
    }

    /**
     * Set Number of Guests
     * @param integer $guestQuantity
     * @return bookingsExperience
     */
    public function setGuestQuantity($guestQuantity){
        $this->guestQuantity = $guestQuantity;
        return $this;
    }

    /**
     * Get Number of Guests
     * @return integer
     */
    public function getGuestQuantity(){
        return $this->guestQuantity;
    }

    /**
     * Set Month of Choice
     * @param string $month
     * @return bookingsExperience
     */
    public function setMonth($month){
        $this->month = $month;
        return $this;
    }

    /**
     * Get Month of Choice
     * @return string
     */
    public function getMonth(){
        return $this->month;
    }

    /**
     * Set Customer Phone Number
     * @param string $phone
     * @return bookingsExperience
     */
    public function setPhone($phone){
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get Customer Phone Number
     * @return string
     */
    public function getPhone(){
        return $this->phone;
    }

    /**
     * Set Customer Email
     * @param string $email
     * @return bookingsExperience
     */
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    /**
     * Get Customer Email
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Get Message
     * @return string
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * Set Message
     * @param string $message
     * @return bookingsExperience
     */
    public function setMessage($message){

        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param string $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return array
     */
    public function getExperiences()
    {
        return $this->experiences;
    }

    /**
     * @param array $experiences
     */
    public function setExperiences($experiences)
    {
        $this->experiences = $experiences;
    }




    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    protected $created;


    /**
     * Set Created
     * @param \DateTime $created
     * @return bookingsExperience
     */
    public function setCreated($created){
        $this->created = $created;

        return $this;
    }

    /**
     * Get Created
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }

    /**
     * Triggered on Insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->created = new \DateTime("now");
    }
}
