<?php

namespace OurguestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Itinerary
 * @package OurguestBundle\Entity
 * @ORM\Table(name="itinerary")
 * @ORM\Entity(repositoryClass="OurguestBundle\Repository\ItineraryRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Itinerary
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="day", type="integer")
     */
    private $day;


    /**
     * @var string
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;



    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="alt_text", type="string", length=125, nullable=true)
     */
    private $altText;

    /**
     * @Assert\NotBlank(message="Image is missing!")
     * @Assert\File(
     *     mimeTypes = {"images/*"},
     *     mimeTypesMessage = "Please upload a valid image.",
     *     maxSize="1024k",
     *     maxSizeMessage="File size too large."
     * )
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="OurguestBundle\Entity\Tours", inversedBy="itinerary")
     * @ORM\JoinColumn(name="tour_id", referencedColumnName="id")
     */
    private $tour;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Itinerary
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }



    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'images/uploads/itinerary';
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getFile();
    }

    /**
     * Set day
     * @param int $day
     * @return Itinerary
     */
    public function setDay($day){
        $this->day = $day;
        return $this;
    }

    /**
     * Get day
     * @return int
     */
    public function getDay(){
        return $this->day;
    }

    /**
     * Set Description
     * @param string $description
     * @return Itinerary
     */
    public function setDescription($description){

        $this->description = $description;

        return $this;
    }

    /**
     * Get Description
     * @return string
     */
    public function getDescription(){

        return $this->description;
    }

    /**
     * Set Tour
     * @param \OurguestBundle\Entity\Tours $tour
     * @return Itinerary
     */
    public function setTour(\OurguestBundle\Entity\Tours $tour){

        $this->tour = $tour;
        return $this;
    }

    /**
     * Get Tour
     *
     * @return \OurguestBundle\Entity\Tours
     */
    public function getTour(){
        return $this->tour;
    }

    /**
     * Set Alt Text
     *
     * @param string $altText
     * @return Itinerary
     */
    public function setAltText($altText){
        $this->altText = $altText;
        return $this;
    }

    /**
     * Get Alt Text
     *
     * @return string
     */
    public function getAltText(){
        return $this->altText;
    }
}
