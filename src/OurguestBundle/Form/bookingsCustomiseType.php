<?php

namespace OurguestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class bookingsCustomiseType extends AbstractType{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array(
                    'placeholder' => '*Name',
                    'class' => 'form-control',
                ),
                'required' => true))
            ->add('tourDestination', ChoiceType::class, array(
                'label' => '*Select your Choice of Destination',
                'expanded' => true,
                'multiple' => true,
                'choices' => array(
                    'Sikkim' => 'Sikkim',
                    'Darjeeling/Kalimpong' => 'Darjeeling/Kalimpong',
                    'Meghalaya' => 'Meghalaya',
                    'Nagaland' => 'Nagaland',
                    'Assam' => 'Assam',
                ),
                'required' => true,
            ))
            ->add('tourDuration', ChoiceType::class, array(
                'placeholder' => '*I want to travel for...',
                'choices' => array(
                    '< 1 week' => '< 1 week',
                    '1 - 2 weeks' => '1 - 2 weeks',
                    '2 - 3 weeks' => '2 - 3 weeks',
                    '3 - 4 weeks' => '3 - 4 weeks',
                    '> 1 month' => '> 1 month',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'choice_attr' => array(
                    'placeholder' => array(
                        'attr' => 'disabled selected',
                    )
                )
            ))
            ->add('startDate', DateTimeType::class, array(
                'widget' => 'single_text',
                'attr' => array(
                    'placeholder' => '*I want to start on...',
                    'class' => 'form-control datepicker',
                    'format' => 'yyyy-MM-dd'
                ),
                'required' => true))
            ->add('adultQuantity', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => '*Guests above 6 yrs.'
                ),
                'required' => true,
            ))
            ->add('childQuantity', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Guests below 6 yrs.'
                ),
                'required' => false
            ))
            ->add('phone', TextType::class, array(
                'attr' => array(
                    'maxlength' => '10',
                    'minlength' => '10',
                    'placeholder' => '*10 digit Phone Number',
                    'class' => 'form-control',
                    'pattern' => '\d*',
                ),
                'required' => true))
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'placeholder' => '*Email',
                    'class' => 'form-control',
                ),
                'required' => true))
            ->add('message', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Special Requirements',
                    'class' => 'form-control',
                ),
                'required' => false))

            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary ml-auto mr-auto mt-4 mt-md-auto mb-md-auto'
                ),
                'label' => 'Send Request',
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OurguestBundle\Entity\bookingsCustomise',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ourguestbundle_bookings_tour';
    }
}