<?php

namespace OurguestBundle\Form;

use AppBundle\Entity\Property;
use AppBundle\Form\ImagesType;
use OurguestBundle\Entity\Experiences;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class ToursType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Name',
                'required' => true,
            ))
            ->add('noOfDays', ChoiceType::class, array(
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                    '7' => '7',
                    '8' => '8',
                    '9' => '9',
                    '10' => '10',
                    '11' => '11',
                    '12' => '12',
                    '13' => '13',
                    '14' => '14',
                    '15' => '15',
                    '16' => '16',
                    '17' => '17',
                    '18' => '18',
                    '19' => '19',
                    '20' => '20',
                ),
                'label' => 'Tour Duration',
                'required' => true,
            ))
            ->add('startDate', DateType::class, array(
                'label' => 'Start Date',
                'required' => false
            ))
            ->add('endDate', DateType::class, array(
                'label' => 'End Date',
                'required' => false,
            ))
            ->add('itinerary', CollectionType::class, array(
                'entry_type' => ItineraryType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
                'required' => false
            ))
            ->add('tourOverview')
            ->add('featImage',FileType::class,array(
                'label'=>'Featured Image',
                'required'=>false
            ))
            ->add('altText', TextType::class, array(
                'required' => false,
                'attr' => array(
                    'placeholder' => 'Alt Text'
                )
            ))
            ->add('videoUrl', UrlType::class, array(
                'label' => 'Video URL',
                'required' => false,
            ))
            ->add('tourType', ChoiceType::class, array(
                'choices' => array(
                    'Customizable' => '0',
                    'Fixed Departure' => '1',
                ),
                'required' => 'true',
            ))
            ->add('rate', NumberType::class, array(
                'label' => 'Rate',
                'required' => true,
            ))
            ->add('isFeatured', CheckboxType::class, array(
                'label' => 'Featured',
                'required' => false,
            ))
            ->add('state')
            ->add('image', CollectionType::class, array(
                'entry_type' => ImagesType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
                'required' => false
            ))
            ->add('experiences',EntityType::class,array(
                'label' => 'Tour Experiences',
                'class' => Experiences::class,
                'multiple' => true,
                'by_reference' => false,
                'expanded' => true
            ))
            ->add('property', EntityType::class,array(
                'label' => 'Select Properties',
                'class' => Property::class,
                'multiple' => true,
                'by_reference' => false,
                'expanded' => true,
            ))
            ->add('keywords', TextType::class, array(
                'label' => 'Keywords',
                'required' => true
            ));
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OurguestBundle\Entity\Tours'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ourguestbundle_tours';
    }
}
