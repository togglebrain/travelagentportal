<?php

namespace OurguestBundle\Form;

use AppBundle\Entity\Rooms;
use AppBundle\Repository\RoomsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class bookingsHomestayType extends AbstractType{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $homestayId = $options['homestayId'];

        $builder
            ->add('name', TextType::class, array(
                'attr' => array(
                    'placeholder' => '*Name',
                    'class' => 'form-control',
                ),
                'required' => true))
            ->add('homestayName', TextType::class, array(
                'attr' => array(
                    'hidden'=>true
                ),
                'required' => true))
            ->add('checkIn', DateTimeType::class, array(
                'widget'=>'single_text',
                'attr' => array(
                    'placeholder' => '*Check In',
                    'class' => 'form-control datepicker',
                ),
                'required' => true))
            ->add('checkOut',DateTimeType::class, array(
                'widget'=>'single_text',
                'attr' => array(
                    'placeholder' => '*Check Out',
                    'class' => 'form-control datepicker',
                ),
                'required' => true))
            ->add('room', EntityType::class, array(
                'placeholder' => 'Room Type',
                'class' => 'AppBundle\Entity\Rooms',
                'query_builder' => function(RoomsRepository $repository) use ($homestayId){
                    return $repository->filterRoomsForForm($homestayId);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'multiple' => false,
                'by_reference' => false,
                'expanded' => false,
                'required' => false
            ))
            ->add('adultQuantity', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => '*Guests above 6 yrs.',
                ),
                'required' => true,
            ))
            ->add('childQuantity', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Guests below 6 yrs.'
                ),
                'required' => false,
            ))
            ->add('phone', TextType::class, array(
                'attr' => array(
                    'maxlength' => '10',
                    'minlength' => '10',
                    'placeholder' => '*Phone Number',
                    'class' => 'form-control',
                    'pattern' => '\d*',
                ),
                'required' => true))
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'placeholder' => '*Email',
                    'class' => 'form-control',
                ),
                'required' => true))
            ->add('message', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Special Requirements',
                    'class' => 'form-control',
                ),
                'required' => false))
            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary mt-2',
                    'style' => 'width: 100%',
                ),
                'label' => "Book Now!",
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OurguestBundle\Entity\bookingsHomestay',
            'homestayId' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ourguestbundle_bookings_homestay';
    }
}
