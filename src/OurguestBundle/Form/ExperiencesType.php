<?php

namespace OurguestBundle\Form;

use AppBundle\Form\ImagesType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ExperiencesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Name',
                'required' => true,
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Description',
                'required' => true,
            ))
            ->add('featImage',FileType::class,array(
                'label'=>'Featured Image',
                'required'=>false))
            ->add('altText', TextType::class, array('attr'=>array('placeholder'=>'Alt Text'),'required'=>false))
            ->add('videoUrl', UrlType::class, array(
                'label' => 'Video Url',
                'required' => false,
            ))
            ->add('location')
            ->add('image', CollectionType::class, array(
                'entry_type' => ImagesType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
                'required' => false
            ))
            ->add('isFeatured', CheckboxType::class, array(
                'label' => 'Featured',
                'required' => false,
            ))
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OurguestBundle\Entity\Experiences'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ourguestbundle_experiences';
    }


}
