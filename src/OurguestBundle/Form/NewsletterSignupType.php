<?php

namespace OurguestBundle\Form;

use function Sodium\add;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class NewsletterSignupType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Email',
                    'style' => 'background-color: #fefefe'
                ),
                'required' => true
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Send',
                'attr' => array(
                    'class' => 'btn btn-info'
                )
            ))
        ;
    }
}