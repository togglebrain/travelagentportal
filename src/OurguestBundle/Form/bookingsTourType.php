<?php

namespace OurguestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class bookingsTourType extends AbstractType{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tourDuration = $options['tourDuration'];

        $builder
            ->add('name', TextType::class, array(
                'attr' => array(
                    'placeholder' => '*Name',
                    'class' => 'form-control',
                ),
                'required' => true))
            ->add('tourName', TextType::class, array(
                'attr' => array(
                    'hidden'=>true
                ),
                'required' => true))
            ->add('tourDuration', ChoiceType::class, array(
                'placeholder' => '*Tour Duration',
                'choices' => array(
                    '1 day' => '1',
                    '2 days' => '2',
                    '3 days' => '3',
                    '4 days' => '4',
                    '5 days' => '5',
                    '6 days' => '6',
                    '7 days' => '7',
                    '8 days' => '8',
                    '9 days' => '9',
                    '10 days' => '10',
                    '11 days' => '11',
                    '12 days' => '12',
                    '13 days' => '13',
                    '14 days' => '14',
                    '15 days' => '15',
                    '16 days' => '16',
                    '17 days' => '17',
                    '18 days' => '18',
                    '19 days' => '19',
                    '20 days' => '20',
                    '21 days' => '21',
                    '22 days' => '22',
                    '23 days' => '23',
                    '24 days' => '24',
                    '25 days' => '25',
                    '26 days' => '26',
                    '27 days' => '27',
                    '28 days' => '28',
                    '29 days' => '29',
                    '30 days' => '30',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'data' => $tourDuration,
                'required' => false,
                'choice_attr' => array(
                    'placeholder' => array(
                        'attr' => 'disabled selected',
                    )
                )))
            ->add('startDate', DateTimeType::class, array(
                'widget'=>'single_text',
                'attr' => array(
                    'placeholder' => '*Pick your Start Date',
                    'class' => 'form-control datepicker',
                    'format' => 'yyyy-MM-dd HH:mm'
                ),
                'required' => true))
            ->add('adultQuantity', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => '*Guests above 6 yrs.',
                ),
                'required' => true,
            ))
            ->add('childQuantity', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Guests below 6 yrs.'
                ),
                'required' => false
            ))
            ->add('phone', TextType::class, array(
                'attr' => array(
                    'maxlength' => '10',
                    'minlength' => '10',
                    'placeholder' => '*10 digit Phone Number',
                    'class' => 'form-control',
                    'pattern' => '\d*',
                ),
                'required' => true))
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'placeholder' => 'Email',
                    'class' => 'form-control',
                ),
                'required' => false))
            ->add('message', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Special Requirements',
                    'class' => 'form-control',
                ),
                'required' => false))

            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary'
                ),
                'label' => 'Book Now!',
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OurguestBundle\Entity\bookingsTour',
            'tourDuration' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ourguestbundle_bookings_tour';
    }
}