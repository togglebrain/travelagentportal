<?php

namespace OurguestBundle\Form;

use blackknight467\StarRatingBundle\Form\RatingType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ReviewsType extends AbstractType{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('comment', TextareaType::class, array(
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('email', EmailType::class, array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                ),
            ))
            ->add('rating', RatingType::class, array(
                'label' => 'Rating',
                'required' => true
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary'
                ),
                'label' => 'SUBMIT',
            ));
    }
}