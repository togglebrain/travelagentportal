<?php

namespace OurguestBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array(
                    'placeholder' => '*Name',
                    'class' => 'form-control',
                ),
                'required' => true
            ))
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'placeholder' => '*Email',
                    'class' => 'form-control',
                ),
                'required' => true,
            ))
            ->add('phone', TextType::class, array(
                'attr' => array(
                    'minlength' => 10,
                    'maxlength' => 10,
                    'placeholder' => '*10 digit Phone Number',
                    'class' => 'form-control',
                    'pattern' => '\d*'
                ),
                'required' => true
            ))
            ->add('subject', ChoiceType::class, array(
                'placeholder' => 'I am curious about...',
                'choices' => array(
                    'Tours' => 'Tours',
                    'Homestays' => 'Homestays',
                    'Experiences' => 'Experiences',
                    'Permits' => 'Permits',
                    'Others (Please specify)' => 'Others',
                ),
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choice_attr' => array(
                    'placeholder' => array(
                        'attr' => 'disabled selected'
                    )
                ),
                'required' => false,
            ))
            ->add('message', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Message',
                    'class' => 'form-control',
                )
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary mt-2 mb-3',
                    'style' => 'width: 100%',
                ),
                'label' => 'Send'
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OurguestBundle\Entity\Contact',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ourguestbundle_contact';
    }
}