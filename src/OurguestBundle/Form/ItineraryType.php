<?php

namespace OurguestBundle\Form;

use AppBundle\Form\ImagesType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ItineraryType extends AbstractType{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('day')
            ->add('file',FileType::class, array('label' => '.','required' => false))
            ->add('description')
            ->add('alt_text', TextType::class, array('attr' => array(
                'placeholder' => 'Alt Text',
            ), 'required' => false));
//            ->add('image', CollectionType::class, array(
//                'entry_type' => ImagesType::class,
//                'allow_add' => true,
//                'allow_delete' => true,
//                'prototype' => true,
//                'prototype_name' => '__image__',
//                'by_reference' => false,
//                'label' => false,
//                'required' => true
//            ))
//            ->add('description', CollectionType::class, array(
//                'entry_type' => TextareaType::class,
//                'allow_add' => true,
//                'allow_delete' => true,
//                'prototype' => true,
//                'prototype_name' => '__description__',
//                'by_reference' => false,
//                'label' => false,
//                'required' => true,
//            ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OurguestBundle\Entity\Itinerary'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ourguestbundle_itinerary';
    }
}