<?php

namespace OurguestBundle\Form;

use OurguestBundle\Entity\Experiences;
use OurguestBundle\Repository\ExperiencesRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class bookingsExperienceType extends AbstractType{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', TextType::class, array(
                'attr' => array(
                    'placeholder' => '*Name',
                    'class' => 'form-control',
                ),
                'required' => true))
            ->add('email', EmailType::class, array(
                'attr' => array(
                    'placeholder' => '*Email',
                    'class' => 'form-control',
                ),
                'required' => true))
            ->add('phone', TextType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'maxlength' => '10',
                    'minlength' => '10',
                    'placeholder' => '*10 digit Phone Number',
                    'pattern' => '\d*'
                ),
                'required' => true))
            ->add('guestQuantity', IntegerType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => '*Number of Guests'
                ),
                'required' => true,
            ))
            ->add('month', ChoiceType::class, array(
                'placeholder' => '*Select Month of Travel',
                'choices' => array(
                    'January' => 'January',
                    'February' => 'February',
                    'March' => 'March',
                    'April' => 'April',
                    'May' => 'May',
                    'June' => 'June',
                    'July' => 'July',
                    'August' => 'August',
                    'September' => 'September',
                    'October' => 'October',
                    'November' => 'November',
                    'December' => 'December',
                ),
                'attr' => array(
                    'class' => 'form-control',
                ),
                'required' => true,
                'choice_attr' => array(
                    'placeholder' => array(
                        'attr' => 'disabled selected',
                    )
                )
            ))
            ->add('message', TextareaType::class, array(
                'required' => false,
                'attr' => array(
                    'placeholder' => '*Message',
                    'class' => 'form-control'
                ),
                ))
            ->add('experiences', TextType::class, array(
                'attr' => array(
                    'hidden' => true
                )
                )
            )

            /*->add('experiences', EntityType::class, array(
                'class' => Experiences::class,
                'query_builder' => function(ExperiencesRepository $repository) use ($)))*/
            ->add('submit', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary',
                ),
                'label' => "Send Request!",
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OurguestBundle\Entity\bookingsExperience',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ourguestbundle_bookings_experience';
    }
}