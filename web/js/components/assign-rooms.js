var ROOM_COSTS;
async function getRoomsCost() {
    let propertyId = document.querySelector('.property-intro .item').getAttribute('data-property-id');
    let url = Routing.generate("get_room_costs", {propertyId: propertyId});
    let options = {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
    };

    //ajax call using fetch
    await fetch(url, options)
    .then(response => response.ok ? response.json() : alert('there was a server error'))
    .then(data => {
        ROOM_COSTS = data;
        initializeRoomsCombo();
    })
    .catch(error => alert(error));
};

function toggleRoomsList(event) {
    event.currentTarget.parentElement.classList.toggle('show');
}

function setRoom(event) {
    let roomIndex = event.currentTarget.getAttribute('data-room-index');
    let roomTypeId = event.currentTarget.closest('ul').getAttribute('data-list-index'); //the list index is initialized with the room id
    let isGuestOverLimit = event.currentTarget.classList.contains('guest-limit-crossed'); //class 'guest-limit-crossed' is set if the no. of guests is greater than max. occupancy
    if (isGuestOverLimit) {

    } else {
        let listIndex = event.currentTarget.closest('.assign-room-options ul').getAttribute('data-list-index');

        event.currentTarget.classList.add('selected'); 
        deselectRoomFromOtherLists(roomIndex - 1, listIndex); 

        cartAction('add', roomTypeId, roomIndex - 1);
    }
}

function deselectRoomFromOtherLists(roomIndex, listIndex) {
    let roomsLists = document.querySelectorAll('.assign-room-options ul');
    roomsLists.forEach((roomsList, index) => {
        if (roomsList.getAttribute('data-list-index') !== listIndex)
            roomsList.querySelectorAll('li').forEach((room, index) => {
                index == roomIndex ? room.classList.remove('selected') : null;
            });
    })
}

class Room {
    constructor(props) {
        this.cost = this.getRoomCost(props);
        this.detail = this.getRoomDetail(props.room);
        this.index = props.index;
        this.costAfterDiscount = !isNaN(this.cost) ? (this.cost - (this.cost * (props.discountRate / 100))) : "Max " + props.roomType.max_person + " guests";
    }

    getRoomCost({room, roomType, roomPlan}) {
        let infantCount = room.ages.filter(childAge => childAge <= roomType.max_infant_age).length;
        let childCount = room.childCount - infantCount; //number of children above the minimum infant age
        let guestCount = room.adultCount + childCount;

        let extraAdultCount = room.adultCount - roomType.default_quantity;
        let extraCount = guestCount - roomType.default_quantity;
        let roomCost;

        if(guestCount > roomType.max_person)
            return "guest_overflow";
        
        if (extraAdultCount > 0) 
            roomCost = +roomType['double_' + roomPlan + '_cost'] + (extraAdultCount * roomType['adult_' + roomPlan + '_cost']) + (childCount * roomType['child_with_bed_' + roomPlan + '_cost']) + (infantCount * roomType['child_without_bed_' + roomPlan + '_cost']);
        else if (extraAdultCount <= 0 && room.adultCount > 1)
            roomCost = +roomType['double_' + roomPlan + '_cost'] + (-extraAdultCount * roomType['child_without_bed_' + roomPlan + '_cost']);
        else 
            roomCost = +roomType['single_' + roomPlan + '_cost'] + (childCount * roomType['child_with_bed_' + roomPlan + '_cost']) + (infantCount * roomType['child_without_bed_' + roomPlan + '_cost']);
        
        return roomCost;
    }

    getRoomDetail(room) {
        let adultText = room.adultCount + (room.adultCount > 1 ? " Adults" : " Adult");
        let childText = room.childCount > 0 ? (room.childCount + " Children") : "";
    
        return room.childCount > 0 ? adultText + ", " + childText : adultText;
    }

    updateRoomCost

    render() {
        return ("<li data-room-index=" + this.index + " onclick='setRoom(event)' class=" + (isNaN(this.costAfterDiscount) ? 'guest-limit-crossed' : '') + ">" +
            "<span class='room-title'>" +
            "Room " + this.index +
            "<span class='room-price'>" + (isNaN(this.costAfterDiscount) ? this.costAfterDiscount : "<i class='fa fa-inr'></i>" + this.costAfterDiscount) + "</span>" +
            "</span>" +
            "<span class='room-details'>" +
            this.detail +
            "<span onclick='initializeRoomComboEdit(event)'>edit</span>" +
            "</span>" +
            "<span class='tooltip'>Click edit to adjust guests in room</span>" +
            "</li>");
    }
}


const listRoomsCombo = function(roomId, roomPlan, discountRate) {
    let rooms = JSON.parse(window.sessionStorage.getItem('rooms'));
    let roomType = ROOM_COSTS.filter(room => room.id == roomId)[0];
    return (
        "<ul data-list-index=" + roomId + ">" + 
            rooms.map((room, index) => {
                index++;
                let listItem = new Room({room, roomType, index, roomPlan, discountRate});
                return listItem.render();
            }).join('') +
        "</ul>"
    );
}


function initializeRoomsCombo() {
    let discountRate = document.querySelector('.discount').getAttribute('data-discount');
    let roomOptionsWrapper = document.querySelectorAll('.assign-room-options');
    let roomPlan = document.querySelector('.property-intro .item').getAttribute('data-meal-plan');

    roomOptionsWrapper.forEach((wrapper, index) => {
        let roomId = wrapper.closest('.listing').getAttribute('data-room-id');
        wrapper.querySelector('ul') ? wrapper.querySelector('ul').remove() : null;
        wrapper.insertAdjacentHTML('beforeend', listRoomsCombo(roomId, roomPlan, discountRate));
    });
}