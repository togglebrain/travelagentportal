var roomsData = [];

function openRoomGuestContainer(event) {
    let element = event.currentTarget;
    element.querySelector('.room-guest-quantity__container').classList.add('show');
}

function closeRoomGuestContainer(event) {
    let element = event.currentTarget;
    element.closest('.room-guest-quantity__container').classList.remove('show');
    event.stopImmediatePropagation();
}

//open room-guest in form for editing room-combo
function editRoomCombo() {

}

function addRoom(event) {
    let roomGuestQuantityContainer = event.target.closest('.room-guest-quantity__container');
    let roomGuestQuantityWrapper = roomGuestQuantityContainer.querySelector('.room-guest-quantity__wrapper');
    let lastRoom = roomGuestQuantityContainer.querySelector('.room-combo:last-child');

    if (isChildAgeSet(lastRoom)) {
        let newRoom = lastRoom.cloneNode(true);

        //resetting values of cloned room
        let quantityDetails = newRoom.querySelector('.quantity-details');
        let quantityOptions = newRoom.querySelectorAll('.quantity-options');
        quantityDetails.querySelector('.adult-count').innerText = 2;
        quantityDetails.querySelector('.child-count').innerText = 0;
        quantityOptions.forEach((options) => {
            let input = options.parentElement.querySelector('input');
            options.querySelector('.active').classList.remove('active');
            if(options.getAttribute('data-for') == 'adult') {
                options.querySelector('span:nth-child(2)').classList.add('active')
                input.value = 2;
                input.setAttribute('value', 2);
            }
            else {
                options.querySelector('span').classList.add('active')
                input.value = 0;
                input.setAttribute('value', 0);
            }

        });

        //adding class 'added' for top border
        newRoom.classList.add('added');

        //resetting children 
        let staleAges = newRoom.querySelectorAll('.age-dropdown-wrapper:not(#dummy-age)');
        let childAgeWrapper = newRoom.querySelector('.child-age-wrapper');
        staleAges.forEach(staleAge => childAgeWrapper.removeChild(staleAge));

        //appending cloned room to list
        roomGuestQuantityWrapper.appendChild(newRoom);
        setRoomsIndexAndRemoveButtons(roomGuestQuantityContainer);
    }
}

function editRoom(event) {
    let roomGuestQuantityWrapper = event.target.closest('.room-guest-quantity__wrapper');
    let activeRoom = roomGuestQuantityWrapper.querySelector('.room-combo:not(.collapsed)');
    activeRoom.classList.add('collapsed'); //adding collapsed class to hide room combo details
    event.target.closest('.room-combo').classList.remove('collapsed'); //removing collapsed class to show room combo details
}

function removeRoom(event) {
    let roomGuestQuantityContainer = event.currentTarget.closest('.room-guest-quantity__container');
    let room = event.currentTarget.closest('.room-combo');
    room.parentElement.removeChild(room);
    setRoomsIndexAndRemoveButtons(roomGuestQuantityContainer);
}

function setRoomsIndexAndRemoveButtons(roomsContainer) {
    let rooms = roomsContainer.querySelectorAll('.room-combo');
    rooms.forEach((room, index, array) => {
        index++;
        room.querySelector('.room-title').innerText = 'Room' + ' ' + index;
        index !== array.length ? room.classList.add('collapsed') : room.classList.remove('collapsed');

        if(array.length > 1) {
            room.querySelector('.room__remove').classList.add('visible');
        }
        else {
            room.querySelector('.room__remove').classList.remove('visible'); //removing 'remove' button if only one room selected
            room.classList.remove('added'); //removing top border from the last room
        }
    });
}

function setQuantity(event) {
   if (event.target.tagName == 'SPAN') {
       let selectedValue = event.target.getAttribute('value');
       //setting quantity value. This is displayed when the room-combo is collapsed 
       let quantityOptions = event.target.closest('.quantity-options');
       let inputTag = quantityOptions.parentElement.querySelector('input');

       let count = quantityOptions.closest('.room-combo').querySelector('.quantity-details .quantity .' + quantityOptions.getAttribute('data-for') + '-count');

       inputTag.value = selectedValue;
       inputTag.setAttribute('value', selectedValue);
       count.innerText = selectedValue;
       let currentActiveOption = quantityOptions.querySelector('.active');
       currentActiveOption.classList.remove('active');
       event.target.classList.add('active');
   }
}

function setChildQuantity(event) {
    let childSelection = event.target;
    let childQuantity = childSelection.getAttribute('value');
    let wrapper = childSelection.closest('.child-quantity-wrapper');
    let quantityOptions = childSelection.closest('.quantity-options');
    let currentChildQuantity = quantityOptions.querySelector('.active').getAttribute('value');
    let childAgeWrapper = wrapper.nextElementSibling;
    
    let ageDropdown = childAgeWrapper.querySelector('#dummy-age');

    if (childQuantity > currentChildQuantity) {
        let childAgeIndex = currentChildQuantity;

        while (childQuantity > currentChildQuantity) {
            let newAgeDropdown = ageDropdown.cloneNode(true);
            childAgeIndex++;
            newAgeDropdown.id = "child-age__" + childAgeIndex;
            newAgeDropdown.querySelector('.age-title span').innerText = 'Child' + ' ' + childAgeIndex;
            childAgeWrapper.appendChild(newAgeDropdown);
            childQuantity--;
        }
    } else {
        while(currentChildQuantity > childQuantity) {
            childAgeWrapper.removeChild(childAgeWrapper.querySelector('.age-dropdown-wrapper:last-child'));
            currentChildQuantity--;
        }
    }
    setQuantity(event);
}

function toggleChildAgeDropdown(event) {
    let dropdown = event.target.closest('.age-dropdown');
    dropdown.querySelector('.age-options').classList.toggle('collapsed');
}

function setChildAge(event) {
    let age = event.target.getAttribute('value');
    let ageInput = event.target.closest('.age-dropdown').querySelector('input');
    if (age) {
        event.target.closest('.age-dropdown-wrapper').classList.remove('error');
        ageInput.value = age;
        ageInput.setAttribute('value', age);
    }
    toggleChildAgeDropdown(event);
}

function isChildAgeSet(lastRoom) {
    let lastRoomChildAges = lastRoom.querySelectorAll('.age-dropdown-wrapper:not(#dummy-age)');

    let childAgesNotSet = [];

    lastRoomChildAges.forEach(child => {
        !child.querySelector('.age-dropdown input').value ? childAgesNotSet.push(child) : null;
    });
    
    if(childAgesNotSet.length > 0) {
        childAgesNotSet.map(age => age.classList.add('error'));
        return false;
    }
    return true;
}

function confirmRooms(event) {
    roomsData = []; //clearing rooms array
    let roomsContainer = event.target.parentElement.parentElement;
    let displayInputField = roomsContainer.previousElementSibling;
    let adultTotal, roomTotal, childTotal;
    
    roomsContainer.querySelectorAll('.room-combo').forEach(room => {
        let adultCount = parseInt(room.querySelector('.adult-quantity-wrapper input').value);
        let childCount = parseInt(room.querySelector('.child-quantity-wrapper input').value);
        let ages = [];
        room.querySelectorAll('.child-age-wrapper .age-dropdown-wrapper:not(#dummy-age) .age-value').forEach(age => ages.push(parseInt(age.value)));
        roomsData.push({adultCount, childCount, ages});
    });
    adultTotal = roomsData.reduce(function(total, currentRoom) {
        return total + currentRoom.adultCount;
    }, 0);
    childTotal = roomsData.reduce(function(total, currentRoom) {
        return total + currentRoom.childCount;
    }, 0);
    roomTotal = roomsData.length;

    //updating input field values which are sent to server
    roomsContainer.querySelector('input[name="adult-quantity"]').value = adultTotal;
    roomsContainer.querySelector('input[name="child-quantity"]').value = childTotal;
    roomsContainer.querySelector('input[name="rooms-quantity"]').value = roomTotal;
    roomsContainer.querySelector('input[name="adult-quantity"]').setAttribute('value', adultTotal);
    roomsContainer.querySelector('input[name="child-quantity"]').setAttribute('value', childTotal);
    roomsContainer.querySelector('input[name="rooms-quantity"]').setAttribute('value', roomTotal);

    //updating display field values
    displayInputField.querySelector('.rooms-count').innerText = roomsData.length;
    displayInputField.querySelector('.guest-count').innerText = roomsData.reduce(function (total, currentRoom) {
        return total + currentRoom.adultCount + currentRoom.childCount;
    }, 0);

    //checking if child age in last room set
    let lastRoom = roomsContainer.querySelector('.room-combo:last-child');
    isChildAgeSet(lastRoom) ? closeRoomGuestContainer(event) : null;

    window.sessionStorage.setItem('rooms', JSON.stringify(roomsData));

    typeof(initializeRoomsCombo) == "function" ? initializeRoomsCombo() : null;
}

(function initializeForm() {
    let rooms = JSON.parse(window.sessionStorage.getItem('rooms'));

    if (rooms) {
        
    }
})

