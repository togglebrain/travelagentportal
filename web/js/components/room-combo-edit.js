function initializeRoomComboEdit(event) {
    let roomCombo = event.currentTarget.closest('li');
    let roomIndex = roomCombo.getAttribute('data-room-index');
    let roomGuestContainer = document.querySelector('.room-guest-quantity');
    let editRoomCombo = roomGuestContainer.querySelectorAll('.room-combo')[roomIndex - 1].querySelector('.room-combo__wrapper .quantity-details .room__edit');
    
    let roomGuestFieldClick = new MouseEvent('click');
    let editRoomComboClick = new MouseEvent('click');
    roomGuestContainer.dispatchEvent(roomGuestFieldClick);
    editRoomCombo.dispatchEvent(editRoomComboClick);

    event.stopPropagation();
};

function updatePlanAndRoomCosts(event) {
    document.querySelector('.property-intro .item').setAttribute('data-meal-plan', event.target.value.toLowerCase());
    initializeRoomsCombo();
}