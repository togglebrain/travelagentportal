//ourguest.html.twig
$(function () {

    var topbarHeight = $('.top-bar').height();
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if(scroll > topbarHeight){
            $('.top-bar').addClass('shadow');
            $('.contact-info-side').addClass('show__phone-icon');
        }
        else{
            $('.top-bar').removeClass('shadow');
            $('.contact-info-side').removeClass('show__phone-icon');
            $('.contact-info-side').removeClass('show__all');
            $('.phone-icon i').removeClass('fa-arrow-up rotate').addClass('fa-phone');
        }
    });

    $(".fa-phone").on('click', function () {
        $(this).toggleClass('fa-phone fa-arrow-up rotate');
        $(".contact-info-side").toggleClass('show__all');
    });
});

//adds active class to first carousel in main-carousel
$(function () {
    $('#products-carousel .carousel-inner > div:first-child').addClass('active');
});

//closing popup-navbar(navigation tabs in homestay/tour Modal popups) after selection
$(function () {
    if($(window).width() < 576){
        $(".popup-navbar .navbar-collapse a").click(function(){
            $(".popup-navbar .navbar-collapse").collapse('hide');
        });
    }
});

//inserting show class into tab-content
$(function () {
    var firstLocation = $("#state-navbar li:first-child a");
    var firstTab = $("#primary-tab-content > div:first-child");
    firstLocation.addClass('active selected');
    firstTab.addClass('active show');
});

$("#navbarMenu button").on("click", function () {
    $("#navbarMenu button i").toggleClass('fa-bars fa-times');
});

$.fn.datepicker.defaults.format = "dd-mm-yyyy";
$(".datepicker").datepicker({
    autoclose: true,
    todayHighlight: true,
});

$(".input-daterange").datepicker();

$(function () {
    $(".form-group > select option:first-child").attr("disabled",true);
});

$("#form-newsletter").submit(function (e) {
    e.preventDefault();
    $("#loader-newsletter").show();
    $.ajax({
        data: $(this).serialize(),
        url: Routing.generate('newsletter-signup'),
        method: 'POST',
        success: function(response) { // on success..
            $('#loader-newsletter').hide();
            $('#form-resp').html('<p class="text-success">Welcome to the OG Fam!</p>').hide();
            $('#form-resp').fadeIn('slow').delay(5000).fadeOut('slow');
            $("#form-tour")[0].reset();
        },
        error: function() {
            $('#loader-newsletter').hide();
            $('#form-resp').html('<p class="text-danger">Please try again</p>');
        }
    });
    return false; // cancel original event to prevent form submitting
});


//location.html.twig
$(".btn__load-more.location").on("click", function () {
    var stateId = $(".state-id").attr('id');
    var appendHere = $(this).parent().siblings('.content-section');
    var itemCount = appendHere.find('.content-holder').length;

    var loader = $('<div id="loader1"><img src="/images/loader-dot.gif" width="50"></div>');
    $(this).before(loader);

    loadMore($(this), stateId, itemCount, appendHere);
});

function loadMore(button, stateId, itemCount, appendHere) {
    var url = "";

    switch (button.attr('id')) {
        case "tours":

            url = Routing.generate('load_more_customisable_tours');

            break;

        case "experiences":

            url = Routing.generate('load_more_experiences');

            break;

        case "homestays":

            url = Routing.generate('load_more_homestays');

            break;

        default:
            alert("error");
            break;
    }

    $.ajax({
        url: url,
        data: {
            id: stateId,
            itemCount: itemCount
        },
        type: "POST",
        success: function (data) {
            console.log(data);
            setTimeout(function () {
                $("#loader1").remove();
                if (!$.trim(data))
                {
                    button.parent().append('<div class="msg col-12 text-center">That\'s it for now! More on the way</div>').delay(5000).fadeOut('slow');
                }
                else {
                    appendHere.append(data);
                }
            }, 1000);
        },
        error: function (e) {
            $("#loader1").remove();
            alert('Error Loading Data');
        }
    });

}

//individual row checkboxes on customise tour form
$(function () {
    $('.individual-row-checkboxes label').after('<br>');
});

//sending customise tour email action
$('#customize-form').submit(function(e) { // catch the form's submit event
    e.preventDefault();
    $('#loader').show();
    $.ajax({
        data: $(this).serialize(),
        url: Routing.generate('customize_tour_form'),
        method: 'POST',
        success: function(response) { // on success..
            $('#loader').hide();
            $('#form-resp').html('<p class="text-success">Email sent successfully!!</p>').hide();
            $('#form-resp').fadeIn('slow').delay(5000).fadeOut('slow');
            $("#form-tour")[0].reset();
        },
        error: function() {
            $('#loader').hide();
            $('#form-resp').html('<p class="text-danger">Email failed!! Please try again</p>');
        }
    });
    return false; // cancel original event to prevent form submitting
});

//updating location list in filter and updating the tab content
$(function () {
    $("#state-navbar a").on('click', function () {
        var stateId = $(this).attr('data-id');
        //var url1 = Routing.generate('state_properties', {id: stateId});
        var url2 = Routing.generate('location_listing', {id: stateId});

        //$.when(
        /*$.ajax({
            url: url1,
            data: {
                id: stateId
            },
            type: "POST"
        }),*/

        $.ajax({
            url: url2,
            data: {
                id: stateId
            },
            type: "POST",
            success: function (data) {
                $("#location-list").html(data);
            },
            error: function (e) {
                alert('Error Loading Data');
            }
        });

    });/*.done(function (data1, data2) {
                    $("#primary-tab-content #state_" + stateId + " div").html(data1);
                    $("#location-list").html(data2);
                });*/
});

//updating the tab content on changing location
$(function () {
    $("#location-list").on('change', function () {
        var stateId = $("#location-list option:first-child").val();
        $("#more-content").html("");
        var locationId = $(this).val();
        console.log(locationId);
        if ($.isNumeric(locationId)){
            var url2 = Routing.generate('location_properties', {id: locationId});

            $.ajax({
                url: url2,
                data: {
                    id: locationId
                },
                type: "POST",
                success: function (data) {
                    console.log(data);
                    $("#primary-tab-content #" + stateId + " div").html(data);
                },
                error: function (e) {
                    alert('Error loading data');
                }
            });
        }
        else {
            var state_Id = locationId.slice(6);
            console.log(state_Id);

            var url1 = Routing.generate('state_properties', {id: state_Id});

            $.ajax({
                url: url1,
                data: {
                    id: state_Id
                },
                type: "POST",
                success: function (data) {
                    console.log(data);
                    $("#primary-tab-content #state_" + state_Id + " div").html(data);
                },
                error: function (e) {
                    alert('Error Loading Data');
                }
            });
        }
    });
});

//load more homestays
$(".btn__load-more.homestays").on("click", function () {
    loadMoreProperties($(this));
});

function loadMoreProperties(button) {
    var id = $("#location-list option:selected").val();
    var stateId = $("#location-list option:first-child").val();
    var appendHere = $("#primary-tab-content #" + stateId + ">div");
    var propCount = $("#primary-tab-content #" + stateId + ">div .content-holder").length;

    var url = Routing.generate('load_more_homestays');
    $("#loader1").show();
    $.ajax({
        url: url,
        data: {
            id: id,
            stateId: stateId,
            itemCount: propCount
        },
        type: "POST",
        success: function (data) {
            setTimeout(function () {
                $("#loader1").hide();
                if (!$.trim(data))
                {
                    button.append('<div class="msg col-12 text-center">That\'s it for now! More on the way</div>').find('.msg').delay(5000).fadeOut('slow');
                }
                else {
                    appendHere.append(data);
                }
            }, 1000);
        },
        error: function(e){
            $("#loader1").hide();
            alert('Error Loading Data');
        }
    });
}

//load more experiences
$(".btn__load-more.experiences").on('click', function () {
    loadMoreExperiences($(this));
});

function loadMoreExperiences(button) {
    var activeTab = $("#primary-tab-content div.active");
    var state_id = activeTab.attr('id');
    var appendHere = $("#primary-tab-content div.active>div");
    var expCount = activeTab.find('>div div.content-holder').length;
    var url = Routing.generate('load_more_experiences');

    $("#loader1").show();
    $.ajax({
        url: url,
        data:{
            id: state_id,
            itemCount: expCount
        },
        method: 'POST',
        success: function (data) {
            setTimeout(function () {
                $("#loader1").hide();
                if (!$.trim(data))
                {
                    button.append('<div class="msg col-12 text-center">That\'s it for now! More on the way</div>').find('.msg').delay(5000).fadeOut('slow');
                }
                else {
                    appendHere.append(data);
                }
            }, 1000);
        },
        error: function (e) {
            $("#loader1").hide();
            alert('Error Loading Data');
        }

    });
}

//accordion opening and closing function
$('.accordion__single-page .collapse').on('show.bs.collapse', function(e) {
    var $card = $(this).closest('.card');
    var $open = $($(this).data('parent')).find('.collapse.show');

    var additionalOffset = 0;
    var topBarOffset = $(".top-bar").height();
    topBarOffset = topBarOffset + 20;
    if($card.prevAll().filter($open.closest('.card')).length !== 0)
    {
        additionalOffset =  $open.height() + topBarOffset;
    }
    $('html,body').animate({
        scrollTop: $card.offset().top - additionalOffset
    }, 1000);
});

//multi-item carousel function
$(function () {
    $(".carousel-inner > div:first-child").addClass('active');

    // for every slide in carousel, copy the next slide's item in the slide.
    // Do the same for the next, next item.
    $('.multi-item-carousel .carousel-inner .carousel-item').each(function(){
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length>0) {
            next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
});

$(function () {
    $(".carousel-inner > div:first-child").addClass('active');

    // for every slide in carousel, copy the next slide's item in the slide.
    // Do the same for the next, next item.
    $('.multi-item-carousel .carousel-inner .carousel-item').each(function(){
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length>0) {
            next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
});

//homestay-form submission
$('#form-homestay').submit(function(e) { // catch the form's submit event
    e.preventDefault();
    $('#loader').show();
    var slug = $(".header-section__title").attr('data-slug');
    $.ajax({
        data: $(this).serialize(),
        url: Routing.generate('homestay_template', {slug: slug}),
        method: 'POST',
        success: function(response) { // on success..
            $('#loader').hide();
            $('#form-resp').html('<p class="text-success">Thank you for the enquiry. We will get back to you soon.</p>').hide();
            $('#form-resp').fadeIn('slow').delay(5000).fadeOut('slow');
            e.stopPropagation();
            $("#form-homestay")[0].reset();
        },
        error: function() {
            $('#loader').hide();
            $('#form-resp').html('<p class="text-danger">Email failed!! Please try again</p>');
        }
    });
    return false; // cancel original event to prevent form submitting
});

//tour-form submission
$('#form-tour').submit(function(e) { // catch the form's submit event
    e.preventDefault();
    $('#loader').show();
    var slug = $(".header-section__title").attr('data-slug');
    $.ajax({
        data: $(this).serialize(),
        url: Routing.generate('tour_template', {slug: slug}),
        method: 'POST',
        success: function(response) { // on success..
            $('#loader').hide();
            $('#form-resp').html('<p class="text-success">Thank you for the enquiry. We will get back to you soon.</p>').hide();
            $('#form-resp').fadeIn('slow').delay(5000).fadeOut('slow');
            e.stopPropagation();
            $("#form-tour")[0].reset();
        },
        error: function() {
            $('#loader').hide();
            $('#form-resp').html('<p class="text-danger">Email failed!! Please try again</p>');
        }
    });
    return false; // cancel original event to prevent form submitting
});

//experience-form submission
$('#form-experience').submit(function(e) { // catch the form's submit event
    e.preventDefault();
    $('#loader').show();
    $.ajax({
        data: $(this).serialize(),
        url: Routing.generate('experiences_booking'),
        method: 'POST',
        success: function(response) { // on success..
            $('#loader').hide();
            $('#form-resp').html('<p class="text-success">Thank you for the enquiry. We will get back to you soon.</p>').hide();
            $('#form-resp').fadeIn('slow').delay(5000).fadeOut('slow');
            e.stopPropagation();
            $("#form-homestay")[0].reset();
        },
        error: function() {
            $('#loader').hide();
            $('#form-resp').html('<p class="text-danger">Email failed!! Please try again</p>');
        }
    });
    return false; // cancel original event to prevent form submitting
});

//experience-popup open and close
$(function () {
    $(".experience-popup").on("click", function () {
        var id = this.id;
        var url = Routing.generate("experience_popup", {id: id});

        var name = $(".modal-header .experience-name");
        var location = $(".modal-header .experience-location");
        var description = $(".modal-body .experience-description p");
        var carousel = $("#modal-body-experience .carousel-inner");

        name.html('');
        location.html('');
        description.html('');
        carousel.html('');

        //AJAX Request
        $.ajax({
            url: url,
            type: "GET",
            datatype: "JSON",
            data: {
                id: id
            },
            success: function (response) {
                console.log(response);

                name.text(response[0].name);
                location.text(response[0].location);
                description.text(response[0].description);
                carousel.append('<div class="carousel-item active"><div class="inner-box"><div class="featured-img"><img src="/images/uploads/' + response[0].featimg + '"></div></div></div>');
                $.each(response[0].images, function (key, image) {
                    carousel.append('<div class="carousel-item"><div class="inner-box"><div class="featured-img"><img src="/images/uploads/experience/'+ image.path + '"></div></div></div>')
                });

                //Display Modal
                $("#experienceModal").modal("show");
            },
            error: function (err) {
                alert('Error loading data');
            }
        });
    });
});

//cart functions
var count = 0,
    cartList = $(".cart-body ul"),
    cart = $(".cart"),
    cartBody = $(".cart-body");

//toggle cart on clicking icon
cart.on('click','.cart-trigger', function (event) {
    event.preventDefault();
    toggleCart();
});

cartBody.on('click', '.cart__delete-item', function () {
    var expInCart = $(this).parents('.cart__item');
    var expId = expInCart.attr('id');
    var expOnTab = $('#primary-tab-content').find('label input#' + expId);

    removeFromCart(expInCart, expOnTab, count, 0);
    animateCheckbox(expOnTab);
});

$(document).on("click",'.experience-tab-wrapper input[type="checkbox"]', function () {
    var exp = $(this);
    var expName = exp.val();
    var expId = exp.attr('id');

    if (exp.is(":checked")){
        addToCart(expName, expId, count);
    }
    else if (exp.is(':not(:checked)')){
        var expInCart = cartBody.find('#' + expId);
        if (count > 1){
            removeFromCart(expInCart, exp, count);
        }
        else{
            removeFromCart(expInCart, exp, count);
            /*$("#experience-cart").parent().addClass("cart-empty");
            $(".cart-content").addClass('cart-empty');*/
        }
    }
    animateCheckbox(exp);
});

//cart visibility control
function cartVisibility(status, source){
    if (status < 1) {
        $("#experience-cart").parent().addClass('cart-empty');
        if (source === 0)
            animateCart();
        if ($("body div").hasClass('cart-open__backdrop'))
            blurBackground();
    }
    else {
        $("#experience-cart").parent().removeClass('cart-empty');
    }
}

//adding experience to cart
function addToCart(expName, expId, countUp) {
    var itemAdded = '<li class="cart__item" id="' + expId + '"><div class="cart__item-details d-flex justify-content-between"><span>' + expName + '</span><a href="#0" class="cart__delete-item pt-2">Remove</a href="#0"></div></li>';
    cartList.append(itemAdded);
    count = countUp + 1;
    updateCartCount(count);
    cartVisibility(count);
}

//removing experience from cart
function removeFromCart(expInCart, expOnTab, countDown, source){
    expInCart.addClass('cart__item--deleted').detach();
    count = countDown - 1;

    //setting status of checkbox on tab to false
    expOnTab.prop('checked', false);

    updateCartCount(count);
    cartVisibility(count, source);
}

//update cart count
function updateCartCount(count){
    $(".cart-count").text(count);
}

//checkbox animation
function animateCheckbox(exp) {
    exp.siblings('span').find('i').toggleClass('fa-plus-square fa-check-circle');
}

function toggleCart() {
    animateCart();
    blurBackground();
    if (!$(".experience-form").hasClass('not-confirmed')){
        $(".experience-form").addClass('not-confirmed');
    }
}

function animateCart() {
    $("#experience-cart i").toggleClass('fa-shopping-cart fa-close rotate');
    $(".cart-count").toggleClass('cart-open');

    if ($(".cart-count").hasClass('cart-open'))
        $(".cart-content").removeClass('cart-empty');
    else
        $(".cart-content").addClass('cart-empty');
}

function blurBackground() {
    if ($("body div").hasClass('cart-open__backdrop')){
        $("body div.cart-open__backdrop").remove();
        $("body").removeClass('cart-open');
    }
    else
        $("body").addClass('cart-open').append('<div class="cart-open__backdrop"></div>');
}

//slide in form on confirm
function showForm() {
    $(".experience-form").removeClass('not-confirmed');
    $(".cart-content").toggleClass('cart-empty');

    var experiencesList = [];
    $(".cart-body ul li.cart__item span").each(function () {
        experiencesList.push($(this).text());
    });

    $("#form-experience .experiences").val(experiencesList);
}


